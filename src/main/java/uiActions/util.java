 /* 
 */
package uiActions;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
//import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.Assert;

//import com.hybridFramework.helper.Javascript.JavaScriptHelper;
//import com.hybridFramework.helper.Logger.LoggerHelper;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import frameworkdata.ExtentManager;
import testbase.base;

/**
 * @author Niharika
 *
 * 
 */
@Test
public class util extends base{

	private static final char A = 0;
	public static WebDriver driver;
	public static Properties prop;
	public static Properties OR;
	public File f1;
	public File f2;
	public FileInputStream file1;
	public FileInputStream file2;
	public ExtentReports rep = ExtentManager.getInstance();
	public static ExtentTest test;
	//private Logger Log = LoggerHelper.getLogger(util.class);
	 private static Logger log = LogManager.getLogger(util.class.getName());
	 
	 
	 
	 
	 /***************** BrightFiled Login ************************/
		
	 public void LoginToApp(String username,String password)
	 {
		 type("login_username_xpath",username);
		 
		 //click("login_nextbutton_id");
		 
		 type("login_password_xpath",password);
		 
		 click("login_button_xpath");
	 }
	 
	//========================= PDF Reader ==========================================
	 
	 public String ReadDataFromPDF(String FileName) throws IOException
	 {
    	//String PDFText = null;
    	String pdfFileInText = null;
    	//String FileName = "srinivas";
		 try (PDDocument document = PDDocument.load(new File(System.getProperty("user.dir")+"\\PDFs\\"+FileName+".pdf"))) {
				 
	            document.getClass();

	            if (!document.isEncrypted()) {
				
	                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
	                stripper.setSortByPosition(true);

	                PDFTextStripper tStripper = new PDFTextStripper();

	                pdfFileInText = tStripper.getText(document);
	                //System.out.println("Text:" + st);
					// split by whitespace
	                //String lines[] = pdfFileInText.split("\\r?\\n");
	                 
	            }

	        }
		return pdfFileInText;

	    } 
		 
//===================================== Delete Docs ==========================================================	 
	     public void DeleteDocs() 
	     { 
	         File file = new File(System.getProperty("user.dir")+"\\src\\main\\java\\PDFs\\.pdf"); 
	           
	         if(file.delete()) 
	         { 
	             System.out.println("File deleted successfully"); 
	         } 
	         else
	         { 
	             System.out.println("Failed to delete the file"); 
	         } 
	     } 
	 
	 
	 
	 
	//close browser

	public void closeBrowser(){
		driver.close();
		driver = null;
	}
	
	
	
	
	
	 	/********************Explicit Wait/Expected Conditions*******************************/
	
	public void waitForElementPresence(WebDriver driver,By element,int time)
	{
	    WebDriverWait wait = new WebDriverWait(driver,time);
	    try{
	    wait.until(ExpectedConditions.presenceOfElementLocated(element));
	    } catch(Exception e){
	    	reportFailure(e.getMessage());
			e.printStackTrace();
	    
			Assert.assertFalse(false, "Failed the test - "+e.getMessage());
	    }
	}
	
	
	public void waitForElementVisibility(WebDriver driver,WebElement element,int time)
	{
	    WebDriverWait wait = new WebDriverWait(driver,time);
	    try{
	    wait.until(ExpectedConditions.visibilityOf(element));
	   
	    } catch(Exception e){
	    	reportFailure(e.getMessage());
			e.printStackTrace();
	    
			Assert.assertFalse(false, "Failed the test - "+e.getMessage());
	    }
	}
	
	public void waitForlocatorVisibility(WebDriver driver,By locator,int time)
	{
	    WebDriverWait wait = new WebDriverWait(driver,time);
	    try{
	    
	    wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	    } catch(Exception e){
	    	reportFailure(e.getMessage());
			e.printStackTrace();
	    
			Assert.assertFalse(false, "Failed the test - "+e.getMessage());
	    }
	}
	

	
	public void islocatorInvisible(WebDriver driver,By locator,int time)
	{
	    WebDriverWait wait = new WebDriverWait(driver,time);
	    try{
	    
	    wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	    } catch(Exception e){
	    	reportFailure(e.getMessage());
			e.printStackTrace();
	    
			Assert.assertFalse(false, "Failed the test - "+e.getMessage());
	    }
	}
	
	
	public void waitForElementClickable(WebDriver driver,WebElement element, int time)
	{
	    WebDriverWait wait = new WebDriverWait(driver,time);
	    try{
	    
	    wait.until(ExpectedConditions.elementToBeClickable(element));
	    } catch(Exception e){
	    	reportFailure(e.getMessage());
			e.printStackTrace();
	    
			Assert.assertFalse(false, "Failed the test - "+e.getMessage());
	    }
	}
	
	

	
	
	
	/****************click,wait,type **********************/
	
	       //wait
			public void waitToLoad() throws InterruptedException{
				Thread.sleep(5000);
			}

			
			public static void waitForPageLoad(WebDriver driver,int i) {

				driver.manage().timeouts().pageLoadTimeout(i, TimeUnit.SECONDS);

				}
			
	//click
	public  void click(String locatorKey){
		try{
			
		getElement(locatorKey).click();
		
		}catch(Exception e){
			reportFailure(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	//clear
			public void clear(String locatorKey){
				try{
					
					getElement(locatorKey).clear();
				}catch(Exception e){
					reportFailure(e.getMessage());
					e.printStackTrace();
				}
			}
	
//type
	public void type(String locatorKey,String data){
		try{
		getElement(locatorKey).sendKeys(data);
		}catch(Exception e){
			reportFailure(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	
	//enter action
			public void pressEnter(String locatorKey){
				// a = new Actions(driver);
			    // a.sendKeys(Keys.ENTER);
				try{
					getElement(locatorKey).sendKeys(Keys.ENTER);
				}catch(Exception e){
					reportFailure(e.getMessage());
					e.printStackTrace();
				}
			}
			

	/********************Browser Navigation***********************/
	public static void navigate_forward(WebDriver driver) {
		driver.navigate().forward();
		}

		public static void navigate_back(WebDriver driver) {
		driver.navigate().back();
		}

		public static void refresh(WebDriver driver) {
		driver.navigate().refresh();
		}
	
	/***********************Validations***************************/

	public boolean verifyTextEquals(WebElement element,String expectedText){
		try {
	Assert.assertTrue(element.getText().matches(expectedText));
		}catch (Error e) {
			reportFailure(e.toString());
			Assert.assertFalse(false, "Text didn't match");
		}
	return false;
	}
	
	//wait for element
	
	public WebElement waitForElement(WebDriver driver,long time, final WebElement element){
		/*WebDriverWait wait = new WebDriverWait(driver,time);
		//
		wait.pollingEvery(2, TimeUnit.SECONDS);
		wait.ignoring(NoSuchElementException.class);
		//return wait.until(ExpectedConditions.elementToBeClickable(element));
		return element;*/
	
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(Duration.ofSeconds(time))			
			.pollingEvery(Duration.ofSeconds(5))	
			.ignoring(NoSuchElementException.class);
	 WebElement elem=wait.until(new Function<WebDriver, WebElement>(){
		
		public WebElement apply(WebDriver driver ) {
			return element;
		}
	});
	return elem;
	
	
	}
		
	
		

/*****************************Alerts***********************************/
	
		
		//  check for browser alert
	    public static boolean isDialogPresent(WebDriver driver) {
	        try {
	            driver.getTitle();
	            return false;
	        } catch (UnhandledAlertException e) {
	            // Modal dialog showed
	            return true;
	        }
	    }

	    // accept browser alert
	    public static void acceptBrowserAlert(WebDriver driver) {
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
	    }

	    // decline browser alert
	    public static void declineBrowserAlert(WebDriver driver) {
	        Alert alert = driver.switchTo().alert();
	        alert.dismiss();
	    }

	    // get alert text
	    public static String getBrowserAlertText(WebDriver driver){
	        try {
	            Alert alert = driver.switchTo().alert();
	            String alertText = alert.getText();
	            return alertText;
	        } catch (Exception e) {
	            System.out.println("no browser alert showing");
	        }
	        return null;
	    }

	    /******************* window handling ********************/
	
		public static void switchToNewWindow() {
			Set s = driver.getWindowHandles();
			Iterator itr = s.iterator();
			String w1 = (String) itr.next();
			String w2 = (String) itr.next();
			driver.switchTo().window(w2);
			}

			public static void switchToOldWindow() {
			Set s = driver.getWindowHandles();
			Iterator itr = s.iterator();
			String w1 = (String) itr.next();
			String w2 = (String) itr.next();
			driver.switchTo().window(w1);
			}

			public static void switchToParentWindow() {
			driver.switchTo().defaultContent();
			}

		
		
		/*****************Verify element is displayed ***************/
	
		
		
			public static ExpectedCondition<Boolean> isElementDisplayed(final WebElement element){
				return new ExpectedCondition<Boolean> (){
					public Boolean apply(WebDriver driver){
						try{
							return element.isDisplayed();
						}catch(NoSuchElementException e){
							return false;
						}catch(StaleElementReferenceException e1){
							return false;
						}
					}
				};
			}
			
			
			
			//checkbox
			
			public boolean isChecboxSelected(WebElement element){
				try {
			Assert.assertTrue(element.isSelected());
				}catch (Error e) {
					reportFailure(e.toString());
					Assert.assertFalse(false, "checkbox not selected");
				}
			return false;
			}
			

	/*************************Dropdown*********************************/
		
	   public String getSelectedValue(WebElement element) {
			String value = new Select(element).getFirstSelectedOption().getText();
		 	return value;
		}
		
		public void SelectUsingIndex(WebElement element,int index) {
			Select select = new Select(element);
			select.selectByIndex(index);
			
		}
		
		public void SelectUsingVisibleText(WebElement element,String text) {
			Select select = new Select(element);
			select.selectByVisibleText(text);
			}
		
		public void SelectByVisibleValue(WebElement element,String text) {
			Select select = new Select(element);
			select.selectByValue(text);
		}
		
		
		public List<String> getAllDropDownValues(WebElement locator) {
			Select select = new Select(locator);
			List<WebElement> elementList = select.getOptions();
			List<String> valueList = new LinkedList<String>();
			
			for (WebElement element : elementList) {
				test.log(LogStatus.PASS,element.getText());
				valueList.add(element.getText());
			}
			return valueList;
		}
		

	/************************scroll*******************************/
		//scroll to bottom
		public static void scrollToBottom(WebDriver driver) {
	        ((JavascriptExecutor) driver)
	                .executeScript("window.scrollTo(0, document.body.scrollHeight)");
	    }
		
		//scroll to element
		public static void scrollTo(WebDriver driver, WebElement element) {
	        ((JavascriptExecutor) driver).executeScript(
	                "arguments[0].scrollIntoView();", element);
	    }
		
		//scroll by coordinates
		public void scrollingByCoordinatesofAPage(WebDriver driver) {
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,500)");
		}

		
		/**********************Dynamic string@return ************************/
		public static String dynamicString() {
			//java.util.Date date=new java.util.Date();
			String currenttime = new SimpleDateFormat("mmss").format(Calendar.getInstance().getTime());
			return currenttime;
		}
		
		
		
		
		
		}




