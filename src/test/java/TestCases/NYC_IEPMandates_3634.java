package TestCases;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import org.testng.asserts.SoftAssert;

import java.awt.dnd.peer.DragSourceContextPeer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Driver;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import PAGES.ActiveStaffPAGE;
import PAGES.StudentCriteriaPAGE;
import PAGES.HomePAGE;
import PAGES.IEPMandatesPAGE;
import PAGES.LoginPAGE;
import PAGES.SelectAStudentPAGE;
import PAGES.SelectAUserWizardsPAGE;
import PAGES.SelectAWizardPAGE;
import PAGES.StudentCenterPAGE;
import PAGES.AddStudentPAGE;

import org.openqa.selenium.support.PageFactory;

import testbase.base;
import uiActions.util;

public class NYC_IEPMandates_3634 extends base{
	SoftAssert softAssert = new SoftAssert();
	private static Logger log = LogManager.getLogger(NYC_IEPMandates_3634.class.getName());
	util u = new util();
	testbase.Config config = new testbase.Config(prop);
	
	LoginPAGE LoginPage= new LoginPAGE();
	HomePAGE HomePage = new HomePAGE();
	StudentCriteriaPAGE StudentCriteriaPage = new StudentCriteriaPAGE();
	AddStudentPAGE AddStudentPage = new AddStudentPAGE();
	SelectAStudentPAGE SelectAStudentPage = new SelectAStudentPAGE();
	SelectAWizardPAGE SelectAWizardPage = new SelectAWizardPAGE();
	SelectAUserWizardsPAGE SelectAUserWizardsPage = new SelectAUserWizardsPAGE();
	ActiveStaffPAGE ActiveStaffPage = new ActiveStaffPAGE();
	StudentCenterPAGE StudentCenterPage = new StudentCenterPAGE();
	IEPMandatesPAGE IEPMandatesPage = new IEPMandatesPAGE();
	
	@BeforeTest()
	public void driverinitialize() throws IOException, InterruptedException{
		test = rep.startTest("NYC_IEPMandates_3634");
		test.log(LogStatus.INFO, "Starting the test NYC_IEPMandates_3634 = Verify navigation path from right side window.");
		initializeDriver();
		
		//========================================= DRIVER REFERENCES =======================================================
		driver.navigate().to(prop.getProperty("url"));
		test.log(LogStatus.PASS,  prop.getProperty("url") );
		
		LoginPage.LoginDriverRef(driver);
		HomePage.HomePageDriverRef(driver);
		StudentCriteriaPage.StudentCriteriaPageDriverRef(driver);
		SelectAStudentPage.SelectStudentPageDriverRef(driver);
		SelectAWizardPage.SelectAWizardPageDriverRef(driver);
		SelectAUserWizardsPage.SelectAUserWizardsPageDriverRef(driver);
		ActiveStaffPage.ActiveStaffPageDriverRef(driver);
		IEPMandatesPage.IEPMandatesPageDriverRef(driver);
		StudentCenterPage.StudentCenterDriverRef(driver);
		
		
		
	}
		@Test(priority=1)
		public void AddStudentsInfo() throws Exception
		{
			LoginPage.VerifyEdPlandefaultpage();
			test.log(LogStatus.PASS,  "EdPlan default page is displayed" );
			
			LoginPage.loginToPCG(prop.getProperty("username"),prop.getProperty("password"));
			test.log(LogStatus.PASS, "User logging in to the application" );
			
			HomePage.VerifyEdPlanhomepage();
			test.log(LogStatus.PASS, "User lands on home page of the application" );
			
			HomePage.ClickStudentMenu();
			test.log(LogStatus.PASS, "User Clicking home page student menu" );
			
			HomePage.ClickActiveStudentsMenu();
			test.log(LogStatus.PASS, "User Clicking home page Active student menu" );
			
			getElement("last_name_xpath").sendKeys(prop.getProperty("LastName"));
			test.log(LogStatus.PASS, "Entering criteria Student Last Name Test" );
			
			StudentCriteriaPage.ClickViewStudentsButton();
			test.log(LogStatus.PASS, "Clicking View Students submit button" );
			
			SelectAStudentPage.VerifySelectaStudentHeading();
			test.log(LogStatus.PASS, "Select Student page is displayed" );
			
			SelectAStudentPage.ClickAutomationTest();
			test.log(LogStatus.PASS, "Click on student Automation Test" );
			
			StudentCenterPage.VerifyStudentCenterHeader();
			test.log(LogStatus.PASS, "\"New Student Center\" main page should open " );
			
			StudentCenterPage.ClickIEPMandatesTile();
			test.log(LogStatus.PASS, "Click on IEP Mandates tile" );
			
			IEPMandatesPage.IEPMandatesHeader();
			test.log(LogStatus.PASS, "Control is navigated to IEP Mandates page is displayed" );
			
			IEPMandatesPage.Entries();
			
			IEPMandatesPage.ClickChevronleftArrow();
			test.log(LogStatus.PASS, "Clicking Arrow at extreme right of the IEP Mandates page" );
			
			IEPMandatesPage.VerifyStudentCenterDropDown();
			test.log(LogStatus.PASS, "New Student Center menu's drop down window is displayed." );
			
			u.waitToLoad();
			u.waitToLoad();
			IEPMandatesPage.ClickParentConsent();
			test.log(LogStatus.PASS, "click on menu Parent Consent student center drop down menu" );
			
			IEPMandatesPage.CreateParentConsentDocument();
			test.log(LogStatus.PASS, "Parent Consent header is displayed" );
			
			IEPMandatesPage.ClickChevronleftArrow();
			test.log(LogStatus.PASS, "Clicking Arrow at extreme right of the IEP Mandates page" );
			
			IEPMandatesPage.IEPMandates();
			test.log(LogStatus.PASS, "clicking IEP Mandates from student center drop down menu" );
			
			IEPMandatesPage.IEPMandatesHeader();
			test.log(LogStatus.PASS, "IEP Mandates section is displayed." );
			
			try {
				IEPMandatesPage.ClickChevronleftArrow();
			} catch (Exception e) {
				// TODO: handle exception
			}
			test.log(LogStatus.PASS, "Clicking Arrow at extreme right of the IEP Mandates page" );
			
			IEPMandatesPage.ClickOrderforService();
			test.log(LogStatus.PASS, "click on menu Order of Service from drop down menu" );
			
			IEPMandatesPage.CreateOrderforService();
			test.log(LogStatus.PASS, "Order of Service section is displayed" );
			
			IEPMandatesPage.ClickChevronleftArrow();
			test.log(LogStatus.PASS, "Clicking Arrow at extreme right of the IEP Mandates page" );
			
			IEPMandatesPage.IEPMandates();
			test.log(LogStatus.PASS, "clicking IEP Mandates from student center drop down menu" );
			
			IEPMandatesPage.IEPMandatesHeader();
			test.log(LogStatus.PASS, "IEP Mandates section is displayed." );
			
			try {
				IEPMandatesPage.ClickChevronleftArrow();
			} catch (Exception e) {
				// TODO: handle exception
			}
			test.log(LogStatus.PASS, "Clicking Arrow at extreme right of the IEP Mandates page" );
			
			IEPMandatesPage.ClickSpeechEReferral();
			test.log(LogStatus.PASS, "clicking on Speech E-Referral from drop down menu" );
			
			IEPMandatesPage.ClickSpeechTherapyEReferral();
			test.log(LogStatus.PASS, "Speech E-Referral section is displayed" );
			
			IEPMandatesPage.ClickChevronleftArrow();
			test.log(LogStatus.PASS, "Clicking Arrow at extreme right of the IEP Mandates page" );
			
			IEPMandatesPage.IEPMandates();
			test.log(LogStatus.PASS, "clicking IEP Mandates from student center drop down menu" );
			
			IEPMandatesPage.IEPMandatesHeader();
			test.log(LogStatus.PASS, "IEP Mandates section is displayed." );
			
			try {
				IEPMandatesPage.ClickChevronleftArrow();
			} catch (Exception e) {
				// TODO: handle exception
			}
			test.log(LogStatus.PASS, "Clicking Arrow at extreme right of the IEP Mandates page" );
			
			IEPMandatesPage.ClickOrderReferralHistory();
			test.log(LogStatus.PASS, "click on Order Referral History from drop down menu" );
			
			IEPMandatesPage.OrdersReferralHistoryHeader();
			test.log(LogStatus.PASS, "Order Referral History section is displayed" );
			
			IEPMandatesPage.ClickChevronleftArrow();
			test.log(LogStatus.PASS, "Clicking Arrow at extreme right of the IEP Mandates page" );
			
			IEPMandatesPage.IEPMandates();
			test.log(LogStatus.PASS, "clicking IEP Mandates from student center drop down menu" );
			
			IEPMandatesPage.IEPMandatesHeader();
			test.log(LogStatus.PASS, "IEP Mandates section is displayed." );
			
			try {
				IEPMandatesPage.ClickChevronleftArrow();
			} catch (Exception e) {
				// TODO: handle exception
			}
			test.log(LogStatus.PASS, "Clicking Arrow at extreme right of the IEP Mandates page" );
			
			IEPMandatesPage.ClickUDOAcknowledgementForm();
			test.log(LogStatus.PASS, "click on UDO Acknowledgement Form drop down menu" );
			
			IEPMandatesPage.AcknowledgementofIEPResponsibilities();
			test.log(LogStatus.PASS, "UDO Acknowledgement Form section is displayed" );
			
			IEPMandatesPage.ClickChevronleftArrow();
			test.log(LogStatus.PASS, "Clicking Arrow at extreme right of the IEP Mandates page" );
			
			IEPMandatesPage.IEPMandates();
			test.log(LogStatus.PASS, "clicking IEP Mandates from student center drop down menu" );
			
			IEPMandatesPage.IEPMandatesHeader();
			test.log(LogStatus.PASS, "IEP Mandates section is displayed." );
			
			
			
	}
	
		@AfterTest
		public void closeBrowser(){
		driver.close();
		driver = null; 
		rep.endTest(test);
		rep.flush();
	}		
}