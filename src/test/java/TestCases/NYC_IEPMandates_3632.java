
  package TestCases;
  
  import java.io.IOException; import java.util.concurrent.TimeUnit;
  
  import org.apache.logging.log4j.LogManager; 
  import org.apache.logging.log4j.Logger; 
  import org.openqa.selenium.By; 
  import org.openqa.selenium.WebDriver; 
  import org.testng.Assert; 
  import org.testng.annotations.AfterTest; 
  import org.testng.annotations.BeforeTest;
  import org.testng.annotations.Test; 
  import org.testng.asserts.SoftAssert;
  
  import com.relevantcodes.extentreports.LogStatus;
  
  
  import PAGES.AddStudentPAGE;
  import PAGES.HomePAGE; 
  import PAGES.IEPMandatesPAGE; 
  import PAGES.LoginPAGE;
  import PAGES.SelectAStudentPAGE; 
  import PAGES.SelectAUserWizardsPAGE; 
  import PAGES.SelectAWizardPAGE; 
  import PAGES.StudentCenterPAGE;
  import PAGES.StudentCriteriaPAGE;
  import testbase.base; 
  import uiActions.util;
  
  public class NYC_IEPMandates_3632 extends base{
  
  SoftAssert softAssert = new SoftAssert();
  
  LoginPAGE LoginPage= new LoginPAGE(); 
  HomePAGE HomePage = new HomePAGE();
  StudentCriteriaPAGE StudentCriteriaPage = new StudentCriteriaPAGE(); 
  AddStudentPAGE AddStudentPage = new AddStudentPAGE(); 
  SelectAStudentPAGE SelectAStudentPage = new SelectAStudentPAGE();
  SelectAWizardPAGE SelectAWizardPage = new SelectAWizardPAGE(); 
  SelectAUserWizardsPAGE SelectAUserWizardsPage = new SelectAUserWizardsPAGE(); 
  StudentCenterPAGE StudentCenterPage = new StudentCenterPAGE(); 
  IEPMandatesPAGE IEPMandatesPage=new IEPMandatesPAGE();
  
  private static Logger log = LogManager.getLogger(NYC_IEPMandates_3632.class.getName()); 
  util u = new util();
  testbase.Config config = new testbase.Config(prop);
  
  @BeforeTest() 
  public void driverinitialize() throws IOException, InterruptedException{
  test = rep.startTest("NYC_IEPMandates_3632");
  test.log(LogStatus.INFO, "Starting the test NYC_IEPMandates_3632 :  Verify Search functionality.");
  initializeDriver(); 
  
  driver.navigate().to(prop.getProperty("url"));
  
  
  LoginPage.LoginDriverRef(driver);
  HomePage.HomePageDriverRef(driver); 
  SelectAStudentPage.SelectStudentPageDriverRef(driver);
  SelectAWizardPage.SelectAWizardPageDriverRef(driver);
  SelectAUserWizardsPage.SelectAUserWizardsPageDriverRef(driver);
  StudentCriteriaPage.StudentCriteriaPageDriverRef(driver);
  StudentCenterPage.StudentCenterDriverRef(driver);
  IEPMandatesPage.IEPMandatesPageDriverRef(driver);
  
  
  }
  
  @Test public void TestExecute_NYC3_3622() throws IOException,
  InterruptedException {
  
  
  //=====================================STUDENTINFORMATION===========================================================
  
  
	    LoginPage.VerifyEdPlandefaultpage();
		test.log(LogStatus.PASS,  "EdPlan default page is displayed" );
		
		LoginPage.loginToPCG(prop.getProperty("username"),prop.getProperty("password"));
		test.log(LogStatus.PASS, "User logging in to the application" );
		
		HomePage.VerifyEdPlanhomepage();
		test.log(LogStatus.PASS, "User lands on home page of the application" );
		
		HomePage.ClickStudentMenu();
		test.log(LogStatus.PASS, "User Clicking home page student menu" );
		
		HomePage.VerifyActiveAndInactiveStudentsMenu();
		test.log(LogStatus.PASS, "Menu student open up with Active Students / Inactive Students options" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "User Clicking home page Active student menu" );
		
		StudentCriteriaPage.EnterLastName(prop.getProperty("LastName"));
		test.log(LogStatus.PASS, "Enter criteria Student Last Name: Test" );
		  
		StudentCriteriaPage.ClickViewStudentsButton();
		test.log(LogStatus.PASS, "Click View Students" );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "Select Student page is displayed" );
		  
		SelectAStudentPage.ClickAutomationTest();
		test.log(LogStatus.PASS, "Enter criteria Student Last Name: Test" );
		
		StudentCenterPage.VerifyStudentCenterHeader();
		test.log(LogStatus.PASS, "\"New Student Center\" main page should open " );
		  
		StudentCenterPage.ClickIEPMandatesTile();
		test.log(LogStatus.PASS, "Click \"IEP Mandates\" section" );
		
		String TableData = IEPMandatesPage.IEPMandatesRelativeServicesTabularForm();
		test.log(LogStatus.PASS, " \"IEP Mandates\" section should open and display Related Services in tabular format."+TableData+"" );
		  
		IEPMandatesPage.EnterIEPSearchBox(prop.getProperty("IEPSearchItem1"));
		test.log(LogStatus.PASS, "Click in Search and enter \"Occupational Therapy\" in search criteria" );
		  
		String OccupationalTheraphy = IEPMandatesPage.SearchCriteraData();
		test.log(LogStatus.PASS, "Data should be entered and displayed as per search criteria i.e all Occupational Therapy services are displayed."+OccupationalTheraphy+"" );
		
		IEPMandatesPage.EnterIEPSearchBox(prop.getProperty("IEPSearchItem2"));
		test.log(LogStatus.PASS, "Click in Search and enter �Speech Therapy� in search criteria" );
		
		String SpeechTheraphy = IEPMandatesPage.SearchCriteraData();
		test.log(LogStatus.PASS, "Data should be entered and displayed as per search criteria i.e all Speech Therapy services are displayed."+SpeechTheraphy+"" ); 
		  
  }
  
  
  @AfterTest
	public void closeBrowser(){
	driver.close();
	driver = null; 
	rep.endTest(test);
	rep.flush();
}		
  
  }
 