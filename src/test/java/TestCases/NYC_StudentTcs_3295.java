
package TestCases;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import org.testng.asserts.SoftAssert;

import java.awt.dnd.peer.DragSourceContextPeer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Driver;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import PAGES.ActiveStaffPAGE;
import PAGES.StudentCriteriaPAGE;
import PAGES.StudentHistoryPAGE;
import PAGES.UserSearchCriteriaPAGE;
import PAGES.HomePAGE;
import PAGES.IEPMandatesPAGE;
import PAGES.InactiveStudentsPAGE;
import PAGES.LoginPAGE;
import PAGES.OrderForServicePAGE;
import PAGES.ParentConsentPAGE;
import PAGES.SelectAStudentPAGE;
import PAGES.SelectAUserWizardsPAGE;
import PAGES.SelectAWizardPAGE;
import PAGES.SelectUserPAGE;
import PAGES.StudentCenterPAGE;
import PAGES.AddStudentPAGE;
import PAGES.EventsDetailsPAGE;

import org.openqa.selenium.support.PageFactory;

import testbase.base;
import uiActions.util;

public class NYC_StudentTcs_3295 extends base{
	SoftAssert softAssert = new SoftAssert();
	private static Logger log = LogManager.getLogger(NYC_StudentTcs_3295.class.getName());
	util u = new util();
	testbase.Config config = new testbase.Config(prop);
	int i = 1;
	
	//======================================= PAGE REFERENCES ===================================================================
	LoginPAGE LoginPage= new LoginPAGE();
	HomePAGE HomePage = new HomePAGE();
	StudentCriteriaPAGE StudentCriteriaPage = new StudentCriteriaPAGE();
	AddStudentPAGE AddStudentPage = new AddStudentPAGE();
	SelectAStudentPAGE SelectAStudentPage = new SelectAStudentPAGE();
	SelectAWizardPAGE SelectAWizardPage = new SelectAWizardPAGE();
	SelectAUserWizardsPAGE SelectAUserWizardsPage = new SelectAUserWizardsPAGE();
	ActiveStaffPAGE ActiveStaffPage = new ActiveStaffPAGE();
	StudentCenterPAGE StudentCenterPage = new StudentCenterPAGE();
	InactiveStudentsPAGE InactiveStudentsPage = new InactiveStudentsPAGE();
	IEPMandatesPAGE IEPMandatesPage = new IEPMandatesPAGE();
	UserSearchCriteriaPAGE UserSearchCriteriaPage = new UserSearchCriteriaPAGE();
	SelectUserPAGE SelectUserPage = new SelectUserPAGE();
	StudentHistoryPAGE StudentHistoryPage = new StudentHistoryPAGE();
	EventsDetailsPAGE EventsDetailsPage = new EventsDetailsPAGE();
	ParentConsentPAGE ParentConsentPage = new ParentConsentPAGE();
	OrderForServicePAGE OrderForServicePage = new OrderForServicePAGE();
	
	@BeforeTest()
	public void driverinitialize() throws IOException, InterruptedException{
		test = rep.startTest("NYC_StudentTcs_3295");
		test.log(LogStatus.INFO, "Starting the test NYC_StudentTcs_3295 = NYC - Delete Student. ");
		initializeDriver();
		
		driver.navigate().to(prop.getProperty("url"));
		test.log(LogStatus.PASS,  prop.getProperty("url") );
		
		//================================= DRIVER REFERNCES ======================================================================== 
		LoginPage.LoginDriverRef(driver);
		HomePage.HomePageDriverRef(driver);
		StudentCriteriaPage.StudentCriteriaPageDriverRef(driver);
		SelectAStudentPage.SelectStudentPageDriverRef(driver);
		SelectAWizardPage.SelectAWizardPageDriverRef(driver);
		SelectAUserWizardsPage.SelectAUserWizardsPageDriverRef(driver);
		ActiveStaffPage.ActiveStaffPageDriverRef(driver);
		InactiveStudentsPage.InactiveStudentsPageDriverRef(driver);
		AddStudentPage.AddStudentriverRef(driver);
		StudentCenterPage.StudentCenterDriverRef(driver);
		IEPMandatesPage.IEPMandatesPageDriverRef(driver);
		UserSearchCriteriaPage.UserSearchCriteriaDriverRef(driver);
		SelectUserPage.SelectUserPageDriverRef(driver);
		StudentHistoryPage.StudentHistoryPageDriverRef(driver);
		EventsDetailsPage.EventsDetailsPageDriverRef(driver);
		ParentConsentPage.ParentConsentPageDriverRef(driver);
		OrderForServicePage.OrderForServicePageDriverRef(driver);
		
		
	}
	
	@Test(dataProvider="excel")
	public void AddStudentsInfo(Map<Object, Object> map) throws Exception
	{
		test.log(LogStatus.INFO, "*********************Executing Test Row : "+i+" & Sheet Row Number is :["+map.get("RowNo").toString()+"]******************");
		i++;
		//=====================================STUDENT INFORMATION===========================================================
		LoginPage.VerifyEdPlandefaultpage();
		test.log(LogStatus.PASS,  "EdPlan default page is displayed" );
		
		LoginPage.loginToPCG(prop.getProperty("username"),prop.getProperty("password"));
		test.log(LogStatus.PASS, "Enter valid username and password " );
		
		HomePage.VerifyEdPlanhomepage();
		test.log(LogStatus.PASS, "Edplan home page is displayed" );
		
		HomePage.ClickStudentMenu();
		test.log(LogStatus.PASS, "Click on Students menu item " );
		
		HomePage.VerifyActiveAndInactiveStudentsMenu();
		test.log(LogStatus.PASS, "Menu student open up with Active Students / Inactive Students options" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Click Active Student menu item " );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria is displayed" );
		
		u.waitForElementVisibility(driver, getElement("last_name_xpath"), 40);
		StudentCriteriaPage.EnterLastName( map.get("LastName").toString());
		test.log(LogStatus.PASS, "Entering last name from excel data" );
		
		StudentCriteriaPage.ClickViewStudentsButton();
		test.log(LogStatus.PASS, "Clicking View Students " );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "Select a student page is displayed " );
		
		SelectAStudentPage.ClickStudentDelete(map.get("LastName").toString());
		test.log(LogStatus.PASS, "Clicking Student delete check box " );
		
		SelectAStudentPage.ClickDeleteButton();
		test.log(LogStatus.PASS, "Clicking  delete button for a selected stdent " );
		
		SelectAStudentPage.ClickCancelButton();
		test.log(LogStatus.PASS, "Clicking  Cancel button for not to delete all students " );
		
		HomePage.ClickStudentMenu();
		test.log(LogStatus.PASS, "User Clicking home page student menu" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "User Clicking home page Active student menu" );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria is displayed" );
		
		u.waitForElementVisibility(driver, getElement("last_name_xpath"), 40);
		StudentCriteriaPage.EnterLastName( map.get("LastName").toString());
		test.log(LogStatus.PASS, "Entering last name from excel data" );
		
		StudentCriteriaPage.ClickViewStudentsButton();
		test.log(LogStatus.PASS, "Clicking View Students " );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "Select a student page is displayed " );
		
		SelectAStudentPage.ClickStudentDelete(map.get("LastName").toString());
		test.log(LogStatus.PASS, "Clicking Student delete check box " );
		
		SelectAStudentPage.ClickDeleteButton();
		test.log(LogStatus.PASS, "Clicking  delete button for a selected stdent " );
		
		SelectAStudentPage.ClickPermanentlyDeleteButton();
		test.log(LogStatus.PASS, "Clicking Permanently delete button for a selected stdent " );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " User is returned to the Student CriteriaPage " );
		
		HomePage.ClickStudentMenu();
		test.log(LogStatus.PASS, "User Clicking home page student menu" );
		
		HomePage.ClickInactiveStudents();
		test.log(LogStatus.PASS, "User Clicking Inactive Student menu item" );
		
		StudentCriteriaPage.StudentCriteriaPageFrame(driver);
		
		u.scrollToBottom(driver);
		StudentCriteriaPage.EnterStudentLastname(map.get("LastName").toString());
		test.log(LogStatus.PASS, "Entering last name from excel data" );
		
		StudentCriteriaPage.ClickViewInactiveStudentsButton();
		test.log(LogStatus.PASS, "Clicking View Students " );
		
		SelectAStudentPage.VerifyInactiveStudentsList();
		test.log(LogStatus.PASS, "List of Inactive students displayed after deleting students" );
		
		SelectAStudentPage.SwitchFrameToDefault(driver);
		
		HomePage.ClickSignOut();
	    test.log(LogStatus.PASS, "Clicking signout, user signs out of the application" );
}
	
	        //======================================Data Provider==========================================================================
			//=============================================================================================================================
		  	@DataProvider(name="excel")
		  	  public Object[][] dataSupplier() throws IOException {

		  	    File file = new File(System.getProperty("user.dir")+"\\src\\main\\java\\data\\TestDataNYC4.xlsx");
		  	    FileInputStream fis = new FileInputStream(file);

		  	    XSSFWorkbook wb = new XSSFWorkbook(fis);
		  	    XSSFSheet sheet = wb.getSheet(this.getClass().getSimpleName());
		  	    //XSSFSheet sheet = wb.getSheet("tmv_query1");
		  	    wb.close();
		  	    int lastRowNum = getRowCount(sheet);
		  	    int lastCellNum = sheet.getRow(0).getLastCellNum();
		  	    Object[][] obj = new Object[lastRowNum][1];
		  	    //System.out.println(this.getClass().getSimpleName());

		  	    for (int i = 0,k=0; i < sheet.getLastRowNum(); i++,k++) {
		  	      Map<Object, Object> datamap = new HashMap<Object, Object>();
		  	      datamap.put("RowNo",(i+2));
		  	      for (int j = 0; j < lastCellNum; j++) {
		  	        datamap.put(sheet.getRow(0).getCell(j).toString(), sheet.getRow(i+1).getCell(j).toString());
		  	      }
		  	      if(datamap.containsValue("*")) {
		  	    	  k--;
		  	      }else{
		  	    	  obj[k][0] = datamap;
		  	    	  //System.out.println("Datamap is "+datamap);
		  	      }

		  	    }
		  	    return  obj;
		  	  }
		      
		      public int getRowCount(XSSFSheet sheet) {
		      	int cnt = 0;
		      	for(int i = 0; i <= sheet.getLastRowNum(); i++) {
		      		if(!sheet.getRow(i).getCell(0).toString().equalsIgnoreCase("*")) {
		      			cnt++;
		      		}
		      	}
		        	return cnt-1;
		      }

		   @AfterTest
			  	public void closeBrowser(){
			  	driver.close();
			  	driver = null; 
			  	rep.endTest(test);
			  	rep.flush();
			  	
			  }	  
		    
	}
