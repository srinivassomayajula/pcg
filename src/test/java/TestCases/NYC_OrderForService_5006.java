package TestCases;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import org.testng.asserts.SoftAssert;

import static org.testng.Assert.assertEquals;

import java.awt.dnd.peer.DragSourceContextPeer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Driver;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import PAGES.StudentCriteriaPAGE;
import PAGES.StudentHistoryPAGE;
import PAGES.ActiveStaffPAGE;
import PAGES.EventsDetailsPAGE;
import PAGES.HomePAGE;
import PAGES.IEPMandatesPAGE;
import PAGES.InactiveStudentsPAGE;
import PAGES.LoginPAGE;
import PAGES.OrderForServicePAGE;
import PAGES.ParentConsentPAGE;
import PAGES.SelectAStudentPAGE;
import PAGES.SelectAUserWizardsPAGE;
import PAGES.SelectAWizardPAGE;
import PAGES.SelectUserPAGE;
import PAGES.StudentCenterPAGE;
import PAGES.AddStudentPAGE;
import PAGES.UserSearchCriteriaPAGE;

import org.openqa.selenium.support.PageFactory;

import testbase.base;
import uiActions.util;

public class NYC_OrderForService_5006 extends base{
	SoftAssert softAssert = new SoftAssert();
	private static Logger log = LogManager.getLogger(NYC_OrderForService_5006.class.getName());
	util u = new util();
	testbase.Config config = new testbase.Config(prop);
	int i =1;
	//=================================== PAGE REFERENCES ===================================================================
	LoginPAGE LoginPage= new LoginPAGE();
	HomePAGE HomePage = new HomePAGE();
	StudentCriteriaPAGE StudentCriteriaPage = new StudentCriteriaPAGE();
	AddStudentPAGE AddStudentPage = new AddStudentPAGE();
	SelectAStudentPAGE SelectAStudentPage = new SelectAStudentPAGE();
	SelectAWizardPAGE SelectAWizardPage = new SelectAWizardPAGE();
	SelectAUserWizardsPAGE SelectAUserWizardsPage = new SelectAUserWizardsPAGE();
	ActiveStaffPAGE ActiveStaffPage = new ActiveStaffPAGE();
	StudentCenterPAGE StudentCenterPage = new StudentCenterPAGE();
	InactiveStudentsPAGE InactiveStudentsPage = new InactiveStudentsPAGE();
	IEPMandatesPAGE IEPMandatesPage = new IEPMandatesPAGE();
	UserSearchCriteriaPAGE UserSearchCriteriaPage = new UserSearchCriteriaPAGE();
	SelectUserPAGE SelectUserPage = new SelectUserPAGE();
	StudentHistoryPAGE StudentHistoryPage = new StudentHistoryPAGE();
	EventsDetailsPAGE EventsDetailsPage = new EventsDetailsPAGE();
	ParentConsentPAGE ParentConsentPage = new ParentConsentPAGE();
	OrderForServicePAGE OrderForServicePage = new OrderForServicePAGE();
	
	
	@BeforeTest()
	public void driverinitialize() throws IOException {
		test = rep.startTest("NYC_OrderForService_5006");
		test.log(LogStatus.INFO, "Starting the test NYC_OrderForService_5006 = New Student Center - Order for Service - Enter service order data and verify event in student history");
		initializeDriver();
		
		driver.navigate().to(prop.getProperty("url"));
		test.log(LogStatus.PASS,  prop.getProperty("url") );
		
		//================================= DRIVER REFERENCES ======================================================================== 
		LoginPage.LoginDriverRef(driver);
		HomePage.HomePageDriverRef(driver);
		StudentCriteriaPage.StudentCriteriaPageDriverRef(driver);
		SelectAStudentPage.SelectStudentPageDriverRef(driver);
		SelectAWizardPage.SelectAWizardPageDriverRef(driver);
		SelectAUserWizardsPage.SelectAUserWizardsPageDriverRef(driver);
		ActiveStaffPage.ActiveStaffPageDriverRef(driver);
		InactiveStudentsPage.InactiveStudentsPageDriverRef(driver);
		AddStudentPage.AddStudentriverRef(driver);
		StudentCenterPage.StudentCenterDriverRef(driver);
		IEPMandatesPage.IEPMandatesPageDriverRef(driver);
		UserSearchCriteriaPage.UserSearchCriteriaDriverRef(driver);
		SelectUserPage.SelectUserPageDriverRef(driver);
		StudentHistoryPage.StudentHistoryPageDriverRef(driver);
		EventsDetailsPage.EventsDetailsPageDriverRef(driver);
		ParentConsentPage.ParentConsentPageDriverRef(driver);
		OrderForServicePage.OrderForServicePageDriverRef(driver);
	}
	
		@Test()
		public void AddStudentsInfo() throws Exception
		{
			LoginPage.VerifyEdPlandefaultpage();
			test.log(LogStatus.PASS,  "EdPlan default page should be displayed " );
			
			LoginPage.loginToPCG(prop.getProperty("username"),prop.getProperty("password"));
			test.log(LogStatus.PASS, "Enter valid username and password logging" );
			
			HomePage.VerifyEdPlanhomepage();
			test.log(LogStatus.PASS, "EdPlan home page should be displayed" );
			
			HomePage.ClickStaffMenu();
			test.log(LogStatus.PASS, "Access Staff" );
			
			HomePage.VerifyActiveAndInactiveStaffStaffMenu();
			test.log(LogStatus.PASS, "Menu should open up with Active Staff /Inactive Staff options" );
			
			HomePage.ClickActiveStaffMenu();
			test.log(LogStatus.PASS, "Click Active Staff" );
			
			UserSearchCriteriaPage.UserSearchCriteriaPageFrame(driver);
			UserSearchCriteriaPage.ClickClinicalSupervisorInstitution();
			UserSearchCriteriaPage.ClickViewUsers();
			test.log(LogStatus.PASS, "Check criteria User Role: Clinical Supervisor - Institution Click View Users" );
			
			SelectUserPage.ClickLogonBehalf();
			test.log(LogStatus.PASS, "For user Name: Automation Supervisee, click the Log on Behalf icon " );
			
			UserSearchCriteriaPage.SwitchFrameToDefault(driver);
			HomePage.VerifyEdPlanhomepage();
			HomePage.VerifyAutomationSuperviseeHeader();
			test.log(LogStatus.PASS, " Return to Main page.Test user is being impersonated as indicated by * Automation Supervisee in the upper right header. " );
	
			HomePage.ClickStudentMenu();
			test.log(LogStatus.PASS, "Click on Students menu item " );
			
			HomePage.VerifyActiveAndInactiveStudentsMenu();
			test.log(LogStatus.PASS, "Menu student should open up with Active Students / Inactive Students options  " );
			
			HomePage.ClickActiveStudentsMenu();
			test.log(LogStatus.PASS, "Click Active Student menu item  " );
			
			StudentCriteriaPage.VerifyStudentCriteria();
			test.log(LogStatus.PASS, "Student Criteria should be displayed " );
			
			u.waitToLoad();
			StudentCriteriaPage.EnterLastName(prop.getProperty("LastName"));
			test.log(LogStatus.PASS, "Enter criteria Student Last Name 'Test'" );
			
			StudentCriteriaPage.ClickViewStudentsButton();
			test.log(LogStatus.PASS, "Click View Students" );
			
			SelectAStudentPage.VerifySelectaStudentHeading();
			test.log(LogStatus.PASS, "Select Student page should be displayed (results limited to criteria)" );
			
			SelectAStudentPage.ClickAutomationTest();
			test.log(LogStatus.PASS, "Click on student Automation Test" );
			
			StudentCenterPage.VerifyStudentCenterHeader();
			test.log(LogStatus.PASS, "Control should be navigated to New Student Center Page." );
			
			StudentCenterPage.ClickOrderForServiceTile();
			test.log(LogStatus.PASS, "Click on Order of Service tile" );
			
			OrderForServicePage.VerifyCreateOrderforServiceHeader();
			test.log(LogStatus.PASS, "Control should be navigated to Create Order of Service section." );
			
			OrderForServicePage.ClickStep2EnterOrderforServiceData();
			test.log(LogStatus.PASS, "Click on Step2: Enter order for Service Data" );
			
			OrderForServicePage.VerifyEnterOrderforServiceDataHeader();
			test.log(LogStatus.PASS, "Control should be navigated to \"Enter order for Service Data\" section." );
			
			OrderForServicePage.EnterOrderforServiceData();
			test.log(LogStatus.PASS, "Enter data for fields under \"Enter Order for Service Data\" section" );
			
			OrderForServicePage.ClickAddNewServiceOrderSubmitButton();
			test.log(LogStatus.PASS, "Click on Add New Service Order" );
			
			Map<Object, Object> lastEnteredOrder = OrderForServicePage.LastEnteredOrderTable();
			test.log(LogStatus.PASS, "This should create New Service Order and the data saved is displayed in Last Entered Order table. "+lastEnteredOrder+"" );
			
			HomePage.ClickStudentCenterMenu();
			test.log(LogStatus.PASS, "Click New Student Center menu" );
			
			StudentCenterPage.VerifyStudentCenterHeader();
			StudentCenterPage.VerifyStudentHistoryTile();
			test.log(LogStatus.PASS, "Control should be navigated to New Student Center Page AND User can see Student History tile on Student Center page." );
			
			StudentCenterPage.ClickStudentHistoryTile();
			test.log(LogStatus.PASS, "Click on \"Student History\" tile " );
			
			StudentHistoryPage.StudentHistoryPageFrame(driver);
			StudentHistoryPage.VeifyStudentHistorypage();
			test.log(LogStatus.PASS, "User is redirected to Student History standard page without problems where Events will appear in the tabular format." );
			
			StudentHistoryPage.ClickEventID();
			StudentHistoryPage.ClickEventID();
			test.log(LogStatus.PASS, "Click on 'Event Id' column's two times to see the latest Order of service event on top" );
			
			String LastorderOfService = StudentHistoryPage.ReadEventIDLatestData();
			test.log(LogStatus.PASS, "Latest Order of service event will appear as the first event with appropriate data for headers such as:\r\n" + 
					"Event ID, Event Date, Event Type, Begin Date, End Date, User, Document, Date Created. ["+LastorderOfService+"]" );
			
			StudentHistoryPage.ClickDetails();
			test.log(LogStatus.PASS, "Click on 'Details' button for recent Order of Service event" );
			
			EventsDetailsPage.SwitchFrameToDefault(driver);
			HomePage.ClickStudentCenterMenu();
			String SiteStudentCenterPage = StudentCenterPage.SiteStudentCenterPage();
			StudentCenterPage.ClickParentConsentTile();
			ParentConsentPage.ClickStep2EnterConsentDataTab();
			//String DateParentConsentpage = ParentConsentPage.ConsentDateParentConsentpage();
			//String ConsentTypeParentConsentpage =  ParentConsentPage.ConsentTypeParentConsentpage();
			HomePage.ClickStudentCenterMenu();
			u.waitToLoad();
			StudentCenterPage.ClickStudentHistoryTile();
			StudentHistoryPage.StudentHistoryPageFrame(driver);
			StudentHistoryPage.ClickEventID();
			StudentHistoryPage.ClickEventID();
			String EventTypeEventStudentHistorypage = StudentHistoryPage.CurrentEventTypeStudentHistory();
			String DateCreatedStudentHistory = StudentHistoryPage.CurrentDateCreatedStudentHistorypage();
			StudentHistoryPage.ClickDetails();
			
			String EventType = EventsDetailsPage.VerifyEventType(EventTypeEventStudentHistorypage);
			String Date = EventsDetailsPage.VerifyDateEventDetailspage(DateCreatedStudentHistory);
			//String ConsentType = EventsDetailsPage.VerifyConsentEventDetailspage(ConsentTypeParentConsentpage);
			String Site = EventsDetailsPage.VerifySiteEventDetailspage(SiteStudentCenterPage);
			String CreatedBy = EventsDetailsPage.VerifyCreatedByEventDetailsPage();
			test.log(LogStatus.PASS, "1. The 'Event Type' type on  EventsDetails.htm page, should be pulled from 'Event Type' in Student History table.["+EventType+"]"+ 
					                 "2. Date will be pulled from �Date Created� in Student history table.["+Date+"]" + 
					                 "3. Creator name will print here as Creator Name. In this scenario, Automation Supervisee name will appear.["+CreatedBy+"]" + 
					                 "4. The data for 'Site' is pulled from 'Site Name' (Student  > New Student Center  > CMR Demographic Snapshot)["+Site+"]" + 
					                 "5. The Start Date is  pulled from �Start Date� in Last Entered Order table (Students  > New Studentcenter  > Order for Service  > "
					                 + "Step2: Enter Order for service data tab  >Last entered order table --["+lastEnteredOrder+"]"
					                 + "Physician, Address,Phone Number, NPI Number, License Number, Medicaid ID Number, ICD Codes,Related Services is pulled from Authorizing Physician�,�� Address, ��PhoneNumber,� NPI Number�, �License Number�, �Medicaid ID Number�, �ICD Codes�,�Related Services� in Last Entered Order table"); 
			
			EventsDetailsPage.SwitchFrameToDefault(driver);	
			
			
			

		}
		
		
		@AfterTest
		public void closeBrowser(){
		driver.close();
		driver = null; 
		rep.endTest(test);
		rep.flush();
	}			
		
}
		