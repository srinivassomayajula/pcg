package TestCases;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import org.testng.asserts.SoftAssert;

import java.awt.dnd.peer.DragSourceContextPeer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Driver;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import PAGES.StudentCriteriaPAGE;
import PAGES.StudentHistoryPAGE;
import PAGES.ActiveStaffPAGE;
import PAGES.EventsDetailsPAGE;
import PAGES.HomePAGE;
import PAGES.IEPMandatesPAGE;
import PAGES.InactiveStudentsPAGE;
import PAGES.LoginPAGE;
import PAGES.OrderForServicePAGE;
import PAGES.ParentConsentPAGE;
import PAGES.SelectAStudentPAGE;
import PAGES.SelectAUserWizardsPAGE;
import PAGES.SelectAWizardPAGE;
import PAGES.SelectUserPAGE;
import PAGES.StudentCenterPAGE;
import PAGES.AddStudentPAGE;
import PAGES.UserSearchCriteriaPAGE;

import org.openqa.selenium.support.PageFactory;

import testbase.base;
import uiActions.util;

public class NYC_OrderForService_5010 extends base{
	SoftAssert softAssert = new SoftAssert();
	private static Logger log = LogManager.getLogger(NYC_OrderForService_5010.class.getName());
	util u = new util();
	testbase.Config config = new testbase.Config(prop);
	int i =1;
	//=================================== PAGE REFERENCES ===================================================================
	LoginPAGE LoginPage= new LoginPAGE();
	HomePAGE HomePage = new HomePAGE();
	StudentCriteriaPAGE StudentCriteriaPage = new StudentCriteriaPAGE();
	AddStudentPAGE AddStudentPage = new AddStudentPAGE();
	SelectAStudentPAGE SelectAStudentPage = new SelectAStudentPAGE();
	SelectAWizardPAGE SelectAWizardPage = new SelectAWizardPAGE();
	SelectAUserWizardsPAGE SelectAUserWizardsPage = new SelectAUserWizardsPAGE();
	ActiveStaffPAGE ActiveStaffPage = new ActiveStaffPAGE();
	StudentCenterPAGE StudentCenterPage = new StudentCenterPAGE();
	InactiveStudentsPAGE InactiveStudentsPage = new InactiveStudentsPAGE();
	IEPMandatesPAGE IEPMandatesPage = new IEPMandatesPAGE();
	UserSearchCriteriaPAGE UserSearchCriteriaPage = new UserSearchCriteriaPAGE();
	SelectUserPAGE SelectUserPage = new SelectUserPAGE();
	StudentHistoryPAGE StudentHistoryPage = new StudentHistoryPAGE();
	EventsDetailsPAGE EventsDetailsPage = new EventsDetailsPAGE();
	ParentConsentPAGE ParentConsentPage = new ParentConsentPAGE();
	OrderForServicePAGE OrderForServicePage = new OrderForServicePAGE();
	
	
	@BeforeTest()
	public void driverinitialize() throws IOException {
		test = rep.startTest("NYC_OrderForService_5010");
		test.log(LogStatus.INFO, "Starting the test NYC_OrderForService_5010 = New Student Center - Order for Service - Upload file with cover page");
		initializeDriver();
		
		driver.navigate().to(prop.getProperty("url"));
		test.log(LogStatus.PASS,  prop.getProperty("url") );
		
		//================================= DRIVER REFERENCES ======================================================================== 
		LoginPage.LoginDriverRef(driver);
		HomePage.HomePageDriverRef(driver);
		StudentCriteriaPage.StudentCriteriaPageDriverRef(driver);
		SelectAStudentPage.SelectStudentPageDriverRef(driver);
		SelectAWizardPage.SelectAWizardPageDriverRef(driver);
		SelectAUserWizardsPage.SelectAUserWizardsPageDriverRef(driver);
		ActiveStaffPage.ActiveStaffPageDriverRef(driver);
		InactiveStudentsPage.InactiveStudentsPageDriverRef(driver);
		AddStudentPage.AddStudentriverRef(driver);
		StudentCenterPage.StudentCenterDriverRef(driver);
		IEPMandatesPage.IEPMandatesPageDriverRef(driver);
		UserSearchCriteriaPage.UserSearchCriteriaDriverRef(driver);
		SelectUserPage.SelectUserPageDriverRef(driver);
		StudentHistoryPage.StudentHistoryPageDriverRef(driver);
		EventsDetailsPage.EventsDetailsPageDriverRef(driver);
		ParentConsentPage.ParentConsentPageDriverRef(driver);
		OrderForServicePage.OrderForServicePageDriverRef(driver);
	}
	
		@Test()
		public void AddStudentsInfo() throws Exception
		{
			LoginPage.VerifyEdPlandefaultpage();
			test.log(LogStatus.PASS,  "EdPlan default page should be displayed " );
			
			LoginPage.loginToPCG(prop.getProperty("username"),prop.getProperty("password"));
			test.log(LogStatus.PASS, "Enter valid username and password logging" );
			
			HomePage.VerifyEdPlanhomepage();
			test.log(LogStatus.PASS, "EdPlan home page should be displayed" );
			
			HomePage.ClickStaffMenu();
			test.log(LogStatus.PASS, "Access Staff" );
			
			HomePage.VerifyActiveAndInactiveStaffStaffMenu();
			test.log(LogStatus.PASS, "Menu should open up with Active Staff /Inactive Staff options" );
			
			HomePage.ClickActiveStaffMenu();
			test.log(LogStatus.PASS, "Click Active Staff" );
			
			UserSearchCriteriaPage.UserSearchCriteriaPageFrame(driver);
			UserSearchCriteriaPage.ClickClinicalSupervisorInstitution();
			UserSearchCriteriaPage.ClickViewUsers();
			test.log(LogStatus.PASS, "Check criteria User Role: Clinical Supervisor - Institution Click View Users" );
			
			SelectUserPage.ClickLogonBehalf();
			test.log(LogStatus.PASS, "For user Name: Automation Supervisee, click the Log on Behalf icon " );
			
			UserSearchCriteriaPage.SwitchFrameToDefault(driver);
			HomePage.VerifyEdPlanhomepage();
			HomePage.VerifyAutomationSuperviseeHeader();
			test.log(LogStatus.PASS, " Return to Main page.Test user is being impersonated as indicated by * Automation Supervisee in the upper right header. " );
	
			HomePage.ClickStudentMenu();
			test.log(LogStatus.PASS, "Click on Students menu item " );
			
			HomePage.VerifyActiveAndInactiveStudentsMenu();
			test.log(LogStatus.PASS, "Menu student should open up with Active Students / Inactive Students options  " );
			
			HomePage.ClickActiveStudentsMenu();
			test.log(LogStatus.PASS, "Click Active Student menu item  " );
			
			StudentCriteriaPage.VerifyStudentCriteria();
			test.log(LogStatus.PASS, "Student Criteria should be displayed " );
			
			u.waitToLoad();
			StudentCriteriaPage.EnterLastName(prop.getProperty("LastName"));
			test.log(LogStatus.PASS, "Enter criteria Student Last Name 'Test'" );
			
			StudentCriteriaPage.ClickViewStudentsButton();
			test.log(LogStatus.PASS, "Click View Students" );
			
			SelectAStudentPage.VerifySelectaStudentHeading();
			test.log(LogStatus.PASS, "Select Student page should be displayed (results limited to criteria)" );
			
			SelectAStudentPage.ClickAutomationTest();
			test.log(LogStatus.PASS, "Click on student Automation Test" );
			
			StudentCenterPage.VerifyStudentCenterHeader();
			test.log(LogStatus.PASS, "Control should be navigated to New Student Center Page." );
			
			StudentCenterPage.ClickOrderForServiceTile();
			test.log(LogStatus.PASS, "Click on Order of Service tile" );
			
			OrderForServicePage.VerifyCreateOrderforServiceHeader();
			test.log(LogStatus.PASS, "Control should be navigated to Create Order of Service section." );
			
			OrderForServicePage.ClickStep3UploadPaperOrderForm();
			test.log(LogStatus.PASS, "Verify click on Step3: Upload Paper Order Form" );
			
			OrderForServicePage.VerifyStep3UploadPaperOrderFormHeader();
			test.log(LogStatus.PASS, "test.log(LogStatus.PASS, Verify click on Step3: Upload Paper Order Form" );
			
			OrderForServicePage.ClickCreateCoverPageButton();
			test.log(LogStatus.PASS, "Verify click on 'Create Cover Page'" );
			
			OrderForServicePage.verifyHomepageTemplateDocumentHeader();
			test.log(LogStatus.PASS, "Control is navigated to 'Homepage Template Document' window." );
			
			OrderForServicePage.EnterCommentsOrderForService(prop.getProperty("Comments"));
			test.log(LogStatus.PASS, "Enter comment in 'Comments' section" );
			
			u.takeScreenShot();
			test.log(LogStatus.PASS, "This should allow comments entry in 'Comments' section." );
			
			OrderForServicePage.ClickCreateCoverPageButtonOnWindow();
			test.log(LogStatus.PASS, "Click on 'Create Cover page' button." );
			
			OrderForServicePage.DownloadAndSavePDF("NYC_OrderForService_5010", System.getProperty("user.dir")+"\\SnipsSikuli\\SmartScan.png");
			test.log(LogStatus.PASS, "Download Order for Service Cover Page" );
				
			OrderForServicePage.ClickPDFWindowClose();
			OrderForServicePage.ClickFileUploadIcon();
			OrderForServicePage.UploadPdfFile("NYC_OrderForService_5010");
			test.log(LogStatus.PASS, "Attach this cover page and any other downloaded document by clicking the Upload (up arrow) icon in Upload Paper Order Form section And Click Upload File button and browse to select the combined documents" );
			
			OrderForServicePage.ClickFileUploadIcon();
			OrderForServicePage.UploadPdfFile("NYC_OrderForService_5010");
			test.log(LogStatus.PASS, "User should able to attach multiple downloaded documents at a time. " );
			
			OrderForServicePage.ClickUploadFileButton();
			HomePage.ClickStudentCenterMenu();
			StudentCenterPage.VerifyStudentCenterHeader();
			test.log(LogStatus.PASS, "Navigate to \"New Student Center\" main page and verify downloaded documents in Documents table" );
			
			StudentCenterPage.VerifyDownloadedDocumentsTable();
			test.log(LogStatus.PASS, "Downloaded documents will appear in the Documents table " );
			
		}
		@AfterTest
		public void closeBrowser(){
		driver.close();
		driver = null; 
		rep.endTest(test);
		rep.flush();
	}		
		
		
		
}