package TestCases;


import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import org.testng.asserts.SoftAssert;

import java.awt.dnd.peer.DragSourceContextPeer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Driver;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import PAGES.ActiveStaffPAGE;
import PAGES.StudentCriteriaPAGE;
import PAGES.StudentHistoryPAGE;
import PAGES.UserSearchCriteriaPAGE;
import PAGES.HomePAGE;
import PAGES.IEPMandatesPAGE;
import PAGES.InactiveStudentsPAGE;
import PAGES.LoginPAGE;
import PAGES.OrderForServicePAGE;
import PAGES.ParentConsentPAGE;
import PAGES.SelectAStudentPAGE;
import PAGES.SelectAUserWizardsPAGE;
import PAGES.SelectAWizardPAGE;
import PAGES.SelectUserPAGE;
import PAGES.StudentCenterPAGE;
import PAGES.AddStudentPAGE;
import PAGES.EventsDetailsPAGE;

import org.openqa.selenium.support.PageFactory;

import testbase.base;
import uiActions.util;

public class NYC_StudentTcs_3242 extends base{
	SoftAssert softAssert = new SoftAssert();
	private static Logger log = LogManager.getLogger(NYC_StudentTcs_3242.class.getName());
	util u = new util();
	testbase.Config config = new testbase.Config(prop);
	int i = 1;
	
	//======================================= PAGE REFERENCES ===================================================================
	LoginPAGE LoginPage= new LoginPAGE();
	HomePAGE HomePage = new HomePAGE();
	StudentCriteriaPAGE StudentCriteriaPage = new StudentCriteriaPAGE();
	AddStudentPAGE AddStudentPage = new AddStudentPAGE();
	SelectAStudentPAGE SelectAStudentPage = new SelectAStudentPAGE();
	SelectAWizardPAGE SelectAWizardPage = new SelectAWizardPAGE();
	SelectAUserWizardsPAGE SelectAUserWizardsPage = new SelectAUserWizardsPAGE();
	ActiveStaffPAGE ActiveStaffPage = new ActiveStaffPAGE();
	StudentCenterPAGE StudentCenterPage = new StudentCenterPAGE();
	InactiveStudentsPAGE InactiveStudentsPage = new InactiveStudentsPAGE();
	IEPMandatesPAGE IEPMandatesPage = new IEPMandatesPAGE();
	UserSearchCriteriaPAGE UserSearchCriteriaPage = new UserSearchCriteriaPAGE();
	SelectUserPAGE SelectUserPage = new SelectUserPAGE();
	StudentHistoryPAGE StudentHistoryPage = new StudentHistoryPAGE();
	EventsDetailsPAGE EventsDetailsPage = new EventsDetailsPAGE();
	ParentConsentPAGE ParentConsentPage = new ParentConsentPAGE();
	OrderForServicePAGE OrderForServicePage = new OrderForServicePAGE();
	
	@BeforeTest()
	public void driverinitialize() throws IOException, InterruptedException{
		test = rep.startTest("NYC_StudentTcs_3242");
		test.log(LogStatus.INFO, "Starting the test NYC_StudentTcs_3242 = NYC - Advanced Student Search Criteria // User will impersonate team member User Role: Clinical Supervisor - Institution. ");
		initializeDriver();
		
		driver.navigate().to(prop.getProperty("url"));
		test.log(LogStatus.PASS,  prop.getProperty("url") );
		
		//================================= DRVER REFERENCES ======================================================================== 
		LoginPage.LoginDriverRef(driver);
		HomePage.HomePageDriverRef(driver);
		StudentCriteriaPage.StudentCriteriaPageDriverRef(driver);
		SelectAStudentPage.SelectStudentPageDriverRef(driver);
		SelectAWizardPage.SelectAWizardPageDriverRef(driver);
		SelectAUserWizardsPage.SelectAUserWizardsPageDriverRef(driver);
		ActiveStaffPage.ActiveStaffPageDriverRef(driver);
		InactiveStudentsPage.InactiveStudentsPageDriverRef(driver);
		AddStudentPage.AddStudentriverRef(driver);
		StudentCenterPage.StudentCenterDriverRef(driver);
		IEPMandatesPage.IEPMandatesPageDriverRef(driver);
		UserSearchCriteriaPage.UserSearchCriteriaDriverRef(driver);
		SelectUserPage.SelectUserPageDriverRef(driver);
		StudentHistoryPage.StudentHistoryPageDriverRef(driver);
		EventsDetailsPage.EventsDetailsPageDriverRef(driver);
		ParentConsentPage.ParentConsentPageDriverRef(driver);
		OrderForServicePage.OrderForServicePageDriverRef(driver);
		
	}
	
	@Test(dataProvider="excel")
	public void AddStudentsInfo(Map<Object, Object> map) throws Exception
	{
		test.log(LogStatus.INFO, "*********************Executing Test Row : "+i+" & Sheet Row Number is :["+map.get("RowNo").toString()+"]******************");
		i++;
		
		LoginPage.VerifyEdPlandefaultpage();
		test.log(LogStatus.PASS,  "EdPlan default page is displayed" );
		
		LoginPage.loginToPCG(prop.getProperty("username"),prop.getProperty("password"));
		test.log(LogStatus.PASS, "User logging in to the application" );
		
		HomePage.VerifyEdPlanhomepage();
		test.log(LogStatus.PASS, "User lands on home page of the application" );
		
		HomePage.ClickStaffMenu();
		test.log(LogStatus.PASS, "User Clicking home page Staff menu" );
		
		HomePage.VerifyActiveAndInactiveStaffStaffMenu();
		test.log(LogStatus.PASS, "VerifyActiveAndInactiveStaffStaffMenu" );
		
		HomePage.ClickActiveStaffMenu();
		test.log(LogStatus.PASS, "Click Active Staff" );
		
		UserSearchCriteriaPage.UserSearchCriteriaPageFrame(driver);
		
		UserSearchCriteriaPage.ClickClinicalSupervisorInstitution(); 
		UserSearchCriteriaPage.ClickViewUsers();
		test.log(LogStatus.PASS, " Check criteria User Role: Clinical Supervisor - Institution  Click View Users" );
		
		UserSearchCriteriaPage.SwitchFrameToDefault(driver);
		
		SelectUserPage.VerifyAutomationUser();
		test.log(LogStatus.PASS, "Select User page is displayed (results limited to criteria)" );
		
		SelectUserPage.SelectUserPageFrame(driver);
		
		SelectUserPage.ClickLogonBehalf();
		test.log(LogStatus.PASS, "For user Name: Automation Supervisee, click the \"Log on Behalf\" icon" );
		
		SelectUserPage.SwitchFrameToDefault(driver);
		
		HomePage.VerifyAutomationSuperviseeHeader();
		HomePage.ClickStudentMenu();
		HomePage.VerifyActiveAndInactiveStudentsMenu();
		test.log(LogStatus.PASS, "Return to Main page.\r\n" + 
				"Test user is being impersonated as indicated by * Automation Supervisee in the upper right header.\r\n" + 
				"Click on Students menu item =>Menu student open up with Active Students / Inactive Students options" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Click Active Student menu item " );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria is displayed" );
		
		StudentCriteriaPage.ClickAdvancedSearch();
		test.log(LogStatus.PASS, "Click on Advanced Search " );
		
		u.waitToLoad();
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, "Advanced Student Criteria is displayed" );
		
		u.SelectUsingVisibleText(getElement("Site_Active_students_xpath"), prop.getProperty("Site"));
		test.log(LogStatus.PASS, "Entering criteria School is Bayside High School" );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.ClickViewStudentsButton();
		test.log(LogStatus.PASS, "Click View Students to search" );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "verifying Select a student page is displayed " );
		
		SelectAStudentPage.ClickAutomationTest();
		test.log(LogStatus.PASS, "Clicking Click on Student : Automation Test" );
		
		StudentCenterPage.VerifyStudentCenterHeader();
		test.log(LogStatus.PASS, "Student Center page is displayed" );
		
		HomePage.ClickStudentMenu();
		test.log(LogStatus.PASS, " Clicking on Menu student open up with Active Students / Inactive Students options" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Active Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Inactive Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Clicking on Active Student menu item" );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria is displayed" );
		
		StudentCriteriaPage.ClickAdvancedSearch();
		test.log(LogStatus.PASS, " Clicking on Advanced Search " );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria is displayed" );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.EnterLastName( map.get("LastName").toString());
		test.log(LogStatus.PASS, "Entering last name from excel data" );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.ClickViewStudentsButton();
		test.log(LogStatus.PASS, "Clicking View Students " );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "Select Student page is displayed (results limited to criteria)" );
		
		SelectAStudentPage.ClickAutomationTest();
		test.log(LogStatus.PASS, "Clicking Student name with Automation Test " );
		
		StudentCenterPage.VerifyStudentCenterHeader();
		test.log(LogStatus.PASS, "Student Center page is displayed" );
	
		HomePage.ClickStudentMenu();
		test.log(LogStatus.PASS, " Clicking on Menu student open up with Active Students / Inactive Students options" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Active Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Inactive Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Clicking on Active Student menu item" );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria is displayed" );
		
		StudentCriteriaPage.ClickAdvancedSearch();
		test.log(LogStatus.PASS, " Clicking on Advanced Search " );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria is displayed" );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.EnterFirstName(map.get("FirstName").toString());
		test.log(LogStatus.PASS, "Entering first name from excel data" );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.ClickViewStudentsButton();
		test.log(LogStatus.PASS, "Clicking View Students " );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "Select Student page is displayed (results limited to criteria)" );
		
		SelectAStudentPage.ClickAutomationTest();
		test.log(LogStatus.PASS, "Clicking Student name with Automation Test " );
		
		StudentCenterPage.VerifyStudentCenterHeader();
		test.log(LogStatus.PASS, "Student Center page is displayed" );
		
		HomePage.ClickStudentMenu();
		test.log(LogStatus.PASS, " Clicking on Menu student open up with Active Students / Inactive Students options" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Active Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Inactive Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Clicking on Active Student menu item" );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria page is displayed" );
		
		StudentCriteriaPage.ClickAdvancedSearch();
		test.log(LogStatus.PASS, " Clicking on Advanced Search " );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria is displayed" );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.EnterMiddleName(map.get("MiddleName").toString());
		test.log(LogStatus.PASS, "Entering Middle name from excel data" );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.ClickViewStudentsButton();
		test.log(LogStatus.PASS, "Clicking View Students " );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "Select Student page is displayed (results limited to criteria)" );
		
		SelectAStudentPage.ClickAutoStudent1Test();
		test.log(LogStatus.PASS, "Clicking Student name with Regression QA Test " );

		StudentCenterPage.VerifyStudentCenterHeader();
		test.log(LogStatus.PASS, "Student Center page is displayed" );
		
		HomePage.ClickStudentMenu();
		test.log(LogStatus.PASS, " Clicking on Menu student open up with Active Students / Inactive Students options" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Active Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Inactive Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Clicking on Active Student menu item" );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria page is displayed" );
		
		StudentCriteriaPage.ClickAdvancedSearch();
		test.log(LogStatus.PASS, " Clicking on Advanced Search " );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria is displayed" );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		u.scrollTo(driver, getElement("NYCID_xpath"));
		StudentCriteriaPage.EnterNYCID(map.get("NycID").toString());
		test.log(LogStatus.PASS, "Entering NYCID from excel data" );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.ClickViewStudentsButton();
		test.log(LogStatus.PASS, "Clicking View Students " );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "Select Student page is displayed (results limited to criteria)" );
		
		SelectAStudentPage.ClickAutomationTest();
		test.log(LogStatus.PASS, "Clicking Student name with Automation Test " );
		
		StudentCenterPage.VerifyStudentCenterHeader();
		test.log(LogStatus.PASS, "Student Center page is displayed" );
		
		HomePage.ClickStudentMenu();
		test.log(LogStatus.PASS, " Clicking on Menu student open up with Active Students / Inactive Students options" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Active Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Inactive Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Clicking on Active Student menu item" );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria page is displayed" );
		
		StudentCriteriaPage.ClickAdvancedSearch();
		test.log(LogStatus.PASS, " Clicking on Advanced Search " );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria is displayed" );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.EnterInstitutionID(map.get("InstitutionID").toString());
		test.log(LogStatus.PASS, "Entering InstitutionID from excel data" );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.ClickViewStudentsButton();
		test.log(LogStatus.PASS, "Clicking View Students " );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "Select Student page is displayed (results limited to criteria)" );
		
		SelectAStudentPage.ClickAutoStudent1Test();
		test.log(LogStatus.PASS, "Clicking Student name with Auto Student1 Test " );
		
		StudentCenterPage.VerifyStudentCenterHeader();
		test.log(LogStatus.PASS, "Student Center page is displayed" );
		
		HomePage.ClickStudentMenu();
		test.log(LogStatus.PASS, " Clicking on Menu student open up with Active Students / Inactive Students options" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Active Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Inactive Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Clicking on Active Student menu item" );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria page is displayed" );
		
		StudentCriteriaPage.ClickAdvancedSearch();
		test.log(LogStatus.PASS, " Clicking on Advanced Search " );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria Page is displayed " );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		u.scrollTo(driver, getElement("Related_Services_xpath"));
		StudentCriteriaPage.ClickOccupationalTherapy();
		test.log(LogStatus.PASS, " Clicking on Occupational Therapy Check box " );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.ClickViewStudentsButton();
		test.log(LogStatus.PASS, "Clicking View Students " );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "Select Student page is displayed (results limited to criteria)" );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "verifying Select a student page is displayed " );
		
		SelectAStudentPage.ClickExportResults();
		test.log(LogStatus.PASS, "Clicking Export Results Button " );
		
		HomePage.ClickStudentMenu();
		test.log(LogStatus.PASS, " Clicking on Menu student open up with Active Students / Inactive Students options" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Active Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Inactive Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Clicking on Active Student menu item" );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria page is displayed" );
		
		StudentCriteriaPage.ClickAdvancedSearch();
		test.log(LogStatus.PASS, " Clicking on Advanced Search " );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria Page is displayed " );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		u.scrollTo(driver, getElement("Related_Services_xpath"));
		StudentCriteriaPage.ClickPhysicalTherapy();
		test.log(LogStatus.PASS, " Clicking on Physical Therapy Check box " );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.ClickViewStudentsButton();
		test.log(LogStatus.PASS, "Clicking View Students " );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "verifying Select a student page is displayed " );
		
		SelectAStudentPage.ClickPrintResults();
		test.log(LogStatus.PASS, "Clicking Print Results button " );
		
		SelectAStudentPage.ClickPrintResultsClose();
		test.log(LogStatus.PASS, "Clicking Print Results Close. " );
		
		HomePage.ClickStudentMenu();
		test.log(LogStatus.PASS, " Clicking on Menu student open up with Active Students / Inactive Students options" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Active Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Verifying Inactive Student menu option is displayed" );
		
		HomePage.ClickActiveStudentsMenu();
		test.log(LogStatus.PASS, "Clicking on Active Student menu item" );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria page is displayed" );
		
		StudentCriteriaPage.ClickAdvancedSearch();
		test.log(LogStatus.PASS, " Clicking on Advanced Search " );
		
		StudentCriteriaPage.VerifyStudentCriteria();
		test.log(LogStatus.PASS, " Student Criteria Page is displayed " );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		u.scrollTo(driver, getElement("Related_Services_xpath"));
		StudentCriteriaPage.ClickSpeechTherapy();
		test.log(LogStatus.PASS, " Clicking on Speech Therapy Check box " );
		
		u.scrollTo(driver, getElement("View_Students_xpath"));
		StudentCriteriaPage.ClickViewStudentsButton();
		test.log(LogStatus.PASS, "Clicking View Students " );
		
		SelectAStudentPage.VerifySelectaStudentHeading();
		test.log(LogStatus.PASS, "verifying Select a student page is displayed " );
		
		SelectAStudentPage.ClickPrintResults();
		test.log(LogStatus.PASS, "Clicking Print Results button " );
		
		SelectAStudentPage.ClickPrintResultsClose();
		test.log(LogStatus.PASS, "Clicking Print Results Close. " );
		
		HomePage.ClickSignOut();
		test.log(LogStatus.PASS, "Clicking signout, user signs out of the application" );
		
	}
	
	@DataProvider(name="excel")
	  public Object[][] dataSupplier() throws IOException {

	    File file = new File(System.getProperty("user.dir")+"\\src\\main\\java\\data\\TestDataNYC3.xlsx");
	    FileInputStream fis = new FileInputStream(file);

	    XSSFWorkbook wb = new XSSFWorkbook(fis);
	    XSSFSheet sheet = wb.getSheet(this.getClass().getSimpleName());
	    //XSSFSheet sheet = wb.getSheet("tmv_query1");
	    wb.close();
	    int lastRowNum = getRowCount(sheet);
	    int lastCellNum = sheet.getRow(0).getLastCellNum();
	    Object[][] obj = new Object[lastRowNum][1];
	    //System.out.println(this.getClass().getSimpleName());

	    for (int i = 0,k=0; i < sheet.getLastRowNum(); i++,k++) {
	      Map<Object, Object> datamap = new HashMap<Object, Object>();
	      datamap.put("RowNo",(i+2));
	      for (int j = 0; j < lastCellNum; j++) {
	        datamap.put(sheet.getRow(0).getCell(j).toString(), sheet.getRow(i+1).getCell(j).toString());
	      }
	      if(datamap.containsValue("*")) {
	    	  k--;
	      }else{
	    	  obj[k][0] = datamap;
	    	  //System.out.println("Datamap is "+datamap);
	      }

	    }
	    return  obj;
	  }
  
  public int getRowCount(XSSFSheet sheet) {
  	int cnt = 0;
  	for(int i = 0; i <= sheet.getLastRowNum(); i++) {
  		if(!sheet.getRow(i).getCell(0).toString().equalsIgnoreCase("*")) {
  			cnt++;
  		}
  	}
    	return cnt-1;
  }

  @AfterTest
	public void closeBrowser(){
  
	driver.close();
	driver = null; 
	rep.endTest(test);
	rep.flush();
  }
  
  

}