package TestCases;

import java.awt.AWTException;
import java.io.IOException;
import java.util.HashMap;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import PAGES.HomePAGE;
import PAGES.LoginPAGE;
import PAGES.SelectAStudentPAGE;
import PAGES.SelectUserPAGE;
import PAGES.SpeechEReferralPAGE;
import PAGES.StudentCenterPAGE;
import PAGES.StudentCriteriaPAGE;
import PAGES.UserSearchCriteriaPAGE;
import testbase.base;
import uiActions.util;

public class NYC_SpeechEReferral_5014 extends base
{
     util u = new util();
   //=================================== PAGE REFERENCES ===================================================================

        LoginPAGE LoginPage= new LoginPAGE();
		HomePAGE HomePage = new HomePAGE();
		SelectAStudentPAGE selectStudentPage = new SelectAStudentPAGE();
		SelectUserPAGE SelectUserpage = new SelectUserPAGE();
		StudentCenterPAGE studentCenterPage =new StudentCenterPAGE();
		StudentCriteriaPAGE studentCriteriapage = new StudentCriteriaPAGE();
		UserSearchCriteriaPAGE userSearchCriteriapage = new UserSearchCriteriaPAGE();
		SpeechEReferralPAGE SpeechEReferralpage = new SpeechEReferralPAGE();
	
	
	@BeforeTest
	public void driverinitialize() throws IOException
        {
		test = rep.startTest("NYC_SpeechEReferral_5014");
		test.log(LogStatus.INFO, "NYC_SpeechEReferral_5014 - Starting the test  Student Center - Speech E-Referral - Create E-Referral document ");
		initializeDriver();
		driver.navigate().to(prop.getProperty("url"));
		test.log(LogStatus.PASS, "Open Url:" +prop.getProperty("url") );
		
		//================================= DRIVER REFERENCES ======================================================================== 
		  HomePage.HomePageDriverRef(driver);	     
		  studentCenterPage.StudentCenterDriverRef(driver);
		  SelectUserpage.SelectUserPageDriverRef(driver);
		  selectStudentPage.SelectStudentPageDriverRef(driver);
		  studentCriteriapage.StudentCriteriaPageDriverRef(driver);
		  userSearchCriteriapage.UserSearchCriteriaDriverRef(driver);
		  SpeechEReferralpage.SpeechEReferralDriverRef(driver);
		  LoginPage.LoginDriverRef(driver);
		
		}
	
	
	@Test(priority=0)
	public void CreateEReferraldocument() throws InterruptedException, FindFailed, AWTException, IOException
	{
		
	     LoginPage.VerifyEdPlandefaultpage(); 
	     test.log(LogStatus.PASS,"EdPlan default page is displayed" );
	     
	     LoginPage.loginToPCG(prop.getProperty("username"),prop.getProperty("password"));
	     test.log(LogStatus.PASS, " EdPlan home page is displayed." );
	       
		  
		  HomePage.ClickStaffMenu();
		  test.log(LogStatus.PASS, "clicking on Staff Menu" );
		  
		  HomePage.VerifyActiveAndInactiveStaffStaffMenu();
		  test.log(LogStatus.PASS, "Verifying Menu should open up with Active Staff /Inactive Staff options." );
		  
		  HomePage.ClickActiveStaffMenu();
		  test.log(LogStatus.PASS, "clicking on ActiveStaff Menu" );
		  
		  userSearchCriteriapage.UserSearchCriteriaPageFrame(driver);
		  //userSearchCriteriapage.VerifyUserSearchCriteriaPage();
		  test.log(LogStatus.PASS, "Verifying User Search criteria page should be displayed" );
		  
		  //userSearchCriteriapage.UserSearchCriteriaPageFrame(driver);
		  userSearchCriteriapage.ClickClinicalSupervisorInstitution();
		  userSearchCriteriapage.ClickViewUsers();
		  test.log(LogStatus.PASS, "Check criteria User Role: Clinical Supervisor - Institution" );
		  
		  
		  
          SelectUserpage.ClickLogonBehalf();
		  test.log(LogStatus.PASS, "For user Name: Automation Supervisee, click the \"Log on Behalf\" icon" );
		  
		  userSearchCriteriapage.SwitchFrameToDefault(driver);
		  HomePage.VerifyEdPlanhomepage();
		  HomePage.VerifyAutomationSuperviseeHeader();
		  test.log(LogStatus.PASS, "Verifying Return to Main page. Test user is being impersonated as indicated by * Automation Supervisee in the upper right header. " );
		  
		  
		  HomePage.ClickStudentMenu();
		  test.log(LogStatus.PASS, "Click on Students menu item" );
		 
		  HomePage.VerifyActiveAndInactiveStudentsMenu();
		  test.log(LogStatus.PASS, "Verifying Menu student should open up with Active Students / Inactive Students options  " );
		  
		  HomePage.ClickActiveStudentsMenu();
		  test.log(LogStatus.PASS, "Click Active Student menu item " );
		  
		  studentCriteriapage.VerifyStudentCriteria();
		  test.log(LogStatus.PASS, "Verifying Student Criteria should be displayed " );
			
		  u.waitForElementVisibility(driver, getElement("last_name_xpath"), 30);
		  test.log(LogStatus.PASS, "Enter criteria Student Last Name" );
		 
		  studentCriteriapage.ClickViewStudentsButton();
		  test.log(LogStatus.PASS, "Click View Student" );
		  
		  selectStudentPage.ClickAutomationTest();
		  HashMap<String, String> map = new HashMap<String, String>();
		  studentCenterPage.studentProfileData(map);
		  System.out.println(map);
		  
		 
		  
		  studentCenterPage.ClickSpeechEReferralTile1();
		  test.log(LogStatus.PASS, "Click on Speech E-Referral tile " );
		 
		  
		  SpeechEReferralpage.StudentDataPulledFromStudentProfile(map);
		  SpeechEReferralpage.DataDisplayedUnderSpeechTherapy();
		  test.log(LogStatus.PASS, " Verifying Control should be navigated to \"Speech Therapy E-Referral\" section wherein student data displayed is being "
			  		+ "pulled from Student profile and also last entered data under "
			  		+ "Speech Therapy section is displayed for Speech Therapy services selected " );
		  
		  SpeechEReferralpage.ClickOnServicISMedicallyNecessary_RadioBtn();
		  test.log(LogStatus.PASS, "Click on RadioBtn corresponding to Servic IS Medically Necessary   " );
		  
		  SpeechEReferralpage.ClickCertificationcriteriaCheckbox();
		  test.log(LogStatus.PASS, "Click Selection of Certification criteria checkbox " );
		  
		  SpeechEReferralpage.VerifydisplayofCreateEReferralbutton();
		  test.log(LogStatus.PASS, "Verifying Selection of Certification criteria should allow display of 'Create E-Referral' button." );
		  
		  SpeechEReferralpage.ClickCreateEReferralbutton();
		  test.log(LogStatus.PASS, "Click on 'Create E-Referral' button" );
		  
		  String Student_Name=SpeechEReferralpage.StudentName();
		  String Student_DateOfBirth=SpeechEReferralpage.StudentDateofBirth();
		  String Student_NYCID=SpeechEReferralpage.StudentNYCID1();
		  
		  SpeechEReferralpage.DownloadAndSavePDF("NYC_SpeechEReferral_5014" , System.getProperty("user.dir")+"\\SnipsSikuli\\ReferalForSpeechLanguage.png");
		 
		  String PDF = SpeechEReferralpage.VerifyPdfData("NYC_OrderForService_5009");
		  PDF.contains(Student_Name);
		  PDF.contains(Student_DateOfBirth);
		  PDF.contains(Student_NYCID);
		  
	    	

		  
		  
	}
	
	@AfterTest
	public void closeBrowser(){
		driver.close();
		driver = null; 
		rep.endTest(test);
		rep.flush();
	}
	
}
