package TestCases;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import org.testng.asserts.SoftAssert;

import java.awt.dnd.peer.DragSourceContextPeer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Driver;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import PAGES.StudentCriteriaPAGE;
import PAGES.StudentHistoryPAGE;
import PAGES.UserSearchCriteriaPAGE;
import PAGES.ActiveStaffPAGE;
import PAGES.HomePAGE;
import PAGES.IEPMandatesPAGE;
import PAGES.InactiveStudentsPAGE;
import PAGES.LoginPAGE;
import PAGES.OrderForServicePAGE;
import PAGES.ParentConsentPAGE;
import PAGES.SelectAStudentPAGE;
import PAGES.SelectAUserWizardsPAGE;
import PAGES.SelectAWizardPAGE;
import PAGES.SelectUserPAGE;
import PAGES.StudentCenterPAGE;
import PAGES.AddStudentPAGE;
import PAGES.EventsDetailsPAGE;

import org.openqa.selenium.support.PageFactory;

import testbase.base;
import uiActions.util;

public class NYC_StudentTcs_3288 extends base{
	SoftAssert softAssert = new SoftAssert();
	private static Logger log = LogManager.getLogger(NYC_StudentTcs_3288.class.getName());
	util u = new util();
	testbase.Config config = new testbase.Config(prop);
	int i =1;
	//=================================== PAGE REFERENCES ===================================================================
	LoginPAGE LoginPage= new LoginPAGE();
	HomePAGE HomePage = new HomePAGE();
	StudentCriteriaPAGE StudentCriteriaPage = new StudentCriteriaPAGE();
	AddStudentPAGE AddStudentPage = new AddStudentPAGE();
	SelectAStudentPAGE SelectAStudentPage = new SelectAStudentPAGE();
	SelectAWizardPAGE SelectAWizardPage = new SelectAWizardPAGE();
	SelectAUserWizardsPAGE SelectAUserWizardsPage = new SelectAUserWizardsPAGE();
	ActiveStaffPAGE ActiveStaffPage = new ActiveStaffPAGE();
	StudentCenterPAGE StudentCenterPage = new StudentCenterPAGE();
	InactiveStudentsPAGE InactiveStudentsPage = new InactiveStudentsPAGE();
	IEPMandatesPAGE IEPMandatesPage = new IEPMandatesPAGE();
	UserSearchCriteriaPAGE UserSearchCriteriaPage = new UserSearchCriteriaPAGE();
	SelectUserPAGE SelectUserPage = new SelectUserPAGE();
	StudentHistoryPAGE StudentHistoryPage = new StudentHistoryPAGE();
	EventsDetailsPAGE EventsDetailsPage = new EventsDetailsPAGE();
	ParentConsentPAGE ParentConsentPage = new ParentConsentPAGE();
	OrderForServicePAGE OrderForServicePage = new OrderForServicePAGE();
	
	@BeforeTest()
	public void driverinitialize() throws IOException{
		test = rep.startTest("NYC_StudentTcs_3288");
		test.log(LogStatus.INFO, "Starting the test NYC_StudentTcs_3288 = Add Student and Assign to Caseload");
		initializeDriver();
		
		driver.navigate().to(prop.getProperty("url"));
		test.log(LogStatus.PASS,  prop.getProperty("url") );
		
		//================================= DRIVER REFERENCES ======================================================================== 
		LoginPage.LoginDriverRef(driver);
		HomePage.HomePageDriverRef(driver);
		StudentCriteriaPage.StudentCriteriaPageDriverRef(driver);
		SelectAStudentPage.SelectStudentPageDriverRef(driver);
		SelectAWizardPage.SelectAWizardPageDriverRef(driver);
		SelectAUserWizardsPage.SelectAUserWizardsPageDriverRef(driver);
		ActiveStaffPage.ActiveStaffPageDriverRef(driver);
		InactiveStudentsPage.InactiveStudentsPageDriverRef(driver);
		AddStudentPage.AddStudentriverRef(driver);
		StudentCenterPage.StudentCenterDriverRef(driver);
		IEPMandatesPage.IEPMandatesPageDriverRef(driver);
		UserSearchCriteriaPage.UserSearchCriteriaDriverRef(driver);
		SelectUserPage.SelectUserPageDriverRef(driver);
		StudentHistoryPage.StudentHistoryPageDriverRef(driver);
		EventsDetailsPage.EventsDetailsPageDriverRef(driver);
		ParentConsentPage.ParentConsentPageDriverRef(driver);
		OrderForServicePage.OrderForServicePageDriverRef(driver);
	}
	
		@Test(dataProvider="excel")
		public void AddStudentsInfo(Map<Object, Object> map) throws Exception
		{
			test.log(LogStatus.INFO, "*********************Executing Test Row : "+i+" & Sheet Row Number is :["+map.get("RowNo").toString()+"]******************");
			i++;
			//=====================================STUDENT INFORMATION===========================================================
			LoginPage.VerifyEdPlandefaultpage();
			test.log(LogStatus.PASS,  "EdPlan default page should be displayed" );
			
			LoginPage.loginToPCG(prop.getProperty("username"),prop.getProperty("password"));
			test.log(LogStatus.PASS, "Enter valid username and password logging" );
			
			HomePage.VerifyEdPlanhomepage();
			test.log(LogStatus.PASS, "EdPlan home page should be displayed" );
			
			HomePage.ClickStudentMenu();
			test.log(LogStatus.PASS, "Click on Students menu item " );
			
			HomePage.VerifyActiveAndInactiveStudentsMenu();
			test.log(LogStatus.PASS, "Menu student open up with Active Students / Inactive Students options" );
			
			HomePage.ClickActiveStudentsMenu();
			test.log(LogStatus.PASS, "Click Active Student menu item " );
			
			StudentCriteriaPage.VerifyStudentCriteria();
			test.log(LogStatus.PASS, "Student Criteria is displayed" );
			
			StudentCriteriaPage.ClickAddStudentButton();
			test.log(LogStatus.PASS, "Click Add Student " );
			
			//=================================	STUDENT INFORMATION =====================================================	
			AddStudentPage.AddStudentPageFrame(driver);
			AddStudentPage.EnterFirstName(map.get("FirstName").toString());
			AddStudentPage.EnterMiddleName(map.get("MiddleName").toString());
			AddStudentPage.EnterLastName( map.get("LastName").toString());
			AddStudentPage.EnterSuffix(map.get("Suffix").toString());
			String NYCID =  map.get("NycID") + u.dynamicString();
			AddStudentPage.EnterNYCID(NYCID);
			String InstitutionID = NYCID+map.get("InstitutionID");
			AddStudentPage.EnterInstitutionID(InstitutionID);
			AddStudentPage.EnterDateOfBirth(map.get("DateOfBirth").toString());
			AddStudentPage.EnterPlaceOfBirth("PlaceOfBirth");
			u.SelectUsingVisibleText(getElement("Gender_dropdown_xpath"), prop.getProperty("Gender"));
			u.SelectUsingVisibleText(getElement("Primary_language_dropdown_xpath"), prop.getProperty("Primarylanguage"));
			u.SelectByVisibleValue(getElement("Grade_xpath"), "2");
			u.SelectUsingVisibleText(getElement("Language_Of_Instruction_xpath"), prop.getProperty("Languageofinstruction"));
			u.SelectUsingVisibleText(getElement("Site_xpath"), prop.getProperty("Site"));	
			AddStudentPage.ClickingRace();
			AddStudentPage.EnterNotesStudent(map.get("Notes").toString());
			test.log(LogStatus.PASS, "Enter Name: First Middle Last Suffix - Auto Student1 Test, Jr." + 
					"Enter NYC ID: - AUT0001" + 
					"Enter Institution ID: - AUT0001A" + 
					"Pick a Date of Birth: - 06/24/2016" + 
					"Enter Place of Birth: - Somewhere, NY" + 
					"Select Gender: - Female" + 
					"Select Prim. Language - English" + 
					"Select Grade: - Kindergarten" + 
					"Select Language of Instruction: - English" + 
					"Select Site: - Demonstration 2" + 
					"Select Enrollment Type: - 853" + 
					"Select Race: - Not Indicated" + 
					"Add Note: - test text" );
            //=================================	PARENTAL INFORMATION =====================================================		
			//============================================================================================================
			AddStudentPage.EnterParentName(map.get("Parents").toString());
			String Relationship = map.get("Relationship").toString();
			AddStudentPage.EnterRelationship(Relationship);
			AddStudentPage.EnterAddress(map.get("Address").toString());
			AddStudentPage.EnterCity(map.get("City").toString());
			AddStudentPage.EnterState(map.get("State").toString());
			AddStudentPage.EnterZipCode(map.get("ZipCode").toString());
			AddStudentPage.EnterEmail(map.get("Email").toString());
			AddStudentPage.EnterHomePhone(map.get("HomePhone").toString());
			AddStudentPage.EnterWorkPhone(map.get("WorkPhone").toString());
			AddStudentPage.EnterCommentsParent(map.get("Comments").toString());
			test.log(LogStatus.PASS, "Enter Parents: - Father Test" + 
					"Select Relationship: - Parent" + 
					"Enter Address: - 123 Test St." + 
					"Enter City, State, Zip Code: - Somewhere, NY 98765" + 
					"Leave Student Lives Here checked - checked" + 
					"Enter E-Mail: - Father@pcgus.com" + 
					"Enter Home Phone: - 555-555-5555" + 
					"Select Voice and Text - Voice and Text" + 
					"Enter Work Phone: - 555-555-5556" + 
					"Select Voice and Text - Voice and Text" + 
					"Leave Include on IEP Team checked - checked" + 
					"Enter Comments: - Test text." );
			
			AddStudentPage.ClickAddStudenttoDatabase();
			test.log(LogStatus.PASS, "Click on Add Student to Database " );
			
			AddStudentPage.ClickConfirmedAddStudent();
			test.log(LogStatus.PASS, "Popup displays that student was added. Update Enrollment page displayed" );
			AddStudentPage.SwitchFrameToDefault(driver);
			
			//=================================	BACK TO STUDENT CRITERIA PAGE FORM ====================================================		
			//=========================================================================================================================
			StudentCriteriaPage.VerifyStudentCriteria();
			test.log(LogStatus.PASS, "Returned to Student Criteria page." );
			
			StudentCriteriaPage.EnterNYCID(NYCID);
			test.log(LogStatus.PASS, "Enter Student ID NYC ID given to new student" );
			
			StudentCriteriaPage.ClickViewStudentsButton();
			SelectAStudentPage.VerifyNYCIDSearhResults(NYCID);
			test.log(LogStatus.PASS, "Search Results return the student" );
			
			SelectAStudentPage.ClickNewStudent();
			test.log(LogStatus.PASS, "Click on Student " );
			
			StudentCenterPage.VerifyStudentCenterHeader();
			test.log(LogStatus.PASS, "Student Center page is displayed" );
			
			StudentCenterPage.VerifyCMRDemographicSnapshot();
			StudentCenterPage.ScrollCreateDocumentBatchScroll();
			String FName = map.get("FirstName").toString();
			String MName = map.get("MiddleName").toString();
			String LName = map.get("LastName").toString();
			String Suffix = map.get("Suffix").toString();
			String LogStudentName = StudentCenterPage.VerifyCMRName(FName + MName + LName + Suffix);
			test.log(LogStatus.PASS,  "CMR Demographic Snapshot matches data entered for new student[" + LogStudentName + "]");
			System.out.println(LogStudentName);
			
			String StudentNameInFull = StudentCenterPage.ForCheckbox();
			test.log(LogStatus.PASS,  "Getting Student name for Crteria verification [" + StudentNameInFull + "]");
			
			String LogNYCID = StudentCenterPage.VerifyCMRNYCID(NYCID);;
			test.log(LogStatus.PASS, "Verifying Student NYCID from CMR Demographic Snapshot with test Data [" + LogNYCID + "]");
			
			String DateofBirth = map.get("DateOfBirth").toString();
			String LogDateofBirth = StudentCenterPage.VerifyDateofBirth(DateofBirth);
			test.log(LogStatus.PASS, "Verifying Student Date of birth from CMR Demographic Snapshot with test Data [" + LogDateofBirth + "]" );
			
			String LogSite = StudentCenterPage.VerifySite(prop.getProperty("Site")).toString();
			test.log(LogStatus.PASS, "Verifying Site from CMR Demographic Snapshot with  [" + prop.getProperty("Site") + "] test Data [" + LogSite + "]" );
			
			String LogPrimaryLanguage = StudentCenterPage.VerifyHomeLanguage(prop.getProperty("Primarylanguage")).toString();
			test.log(LogStatus.PASS, "Verifying Primary language from CMR Demographic Snapshot with [" + prop.getProperty("Primarylanguage") + "] test Data [" + LogPrimaryLanguage + "]" );
			
			String Languageofinstruction = StudentCenterPage.VerifyLanguageofInstruction(prop.getProperty("Languageofinstruction")).toString();
			test.log(LogStatus.PASS, "Verifying language of Instruction from CMR Demographic Snapshot with [" + prop.getProperty("Languageofinstruction") + "] test Data [" + Languageofinstruction + "]" );
			
			String LogGender = StudentCenterPage.VerifyGender(prop.getProperty("Gender")).replaceAll("[,^emale ]*", "").toString();
			test.log(LogStatus.PASS, "Verifying Gender from CMR Demographic Snapshot with [" + prop.getProperty("Gender") + "] test Data [" + LogGender + "]" );
			//========================================================================================================================
			
			HomePage.ClickWizardsMenu();
			test.log(LogStatus.PASS, "On Main Menu, click Wizards" );
			
			SelectAWizardPage.SelectAWizardPageFrame(driver);
			
			SelectAWizardPage.SelectaWizardModal();
			test.log(LogStatus.PASS, "Display of Select a Wizard modal." );
			
			SelectAWizardPage.ClickCaseloadAdministration();
			test.log(LogStatus.PASS, "Click Caseload Administration Wizard" );
			
			SelectAUserWizardsPage.Verifycaseloadsearchcriteriapage();
			test.log(LogStatus.PASS, "User search criteria page." );
			
			String lncriteria = map.get("lncriteria").toString();
			StudentCriteriaPage.EnterCriteriaLastName(lncriteria);
			StudentCriteriaPage.ClickViewUsersSubmit();
			test.log(LogStatus.PASS, "Enter criteria for Last Name:Supervisee. View User(s) [" + lncriteria +"]" );
			
			SelectAUserWizardsPage.VerifyAutomationSupervisee(map.get("lncriteria").toString());
			test.log(LogStatus.PASS, "Search results return users meeting the search criteria, including: Automation Supervisee." );
			
			//SelectAUserWizardsPage.verifyAutomationSupervisee();
			//test.log(LogStatus.PASS, "User caseload search criteria page is displayed" );
			
			SelectAUserWizardsPage.ClickAutomationSupervisee();
			test.log(LogStatus.PASS, "Click the user's name: Automation Supervisee" );
			
			SelectAUserWizardsPage.UserCaseLoadPage();
			test.log(LogStatus.PASS, "Display of the user's caseload page." );
			
			SelectAUserWizardsPage.ClickAddMoreStudentstoCaseload();
			test.log(LogStatus.PASS, "Click button - Add More Students to Caseload" );
			
			//SelectAUserWizardsPage.Verifycaseloadsearchcriteriapage();
			//test.log(LogStatus.PASS, "User caseload search criteria page is displayed" );
			
			String Criterialn = map.get("Criterialn").toString();
			StudentCriteriaPage.EnterStudentLastname(Criterialn);
			StudentCriteriaPage.ClickViewUsersSubmit();
			test.log(LogStatus.PASS, "Enter criteria for Last Name: test View Student(s)[" + Criterialn +"] from excel data" );
			SelectAUserWizardsPage.VerifyTestNewstudent(map.get("Criterialn").toString());
			test.log(LogStatus.PASS, "Search results return students meeting the search criteria, including the newly added student verified" );
			
			//========================= BY DEFAULT ITS CHECKED ===========================================================================
			
			try {
				StudentCriteriaPage.ClickCheckBoxNewstudent(StudentNameInFull);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			test.log(LogStatus.PASS, "Checking the Team Member checkbox for the new student" );
			
			SelectAUserWizardsPage.ClickAddStudentstoCaseload();
			test.log(LogStatus.PASS, "Clicking Add Students to Caseload  submit button,which  Returns to display of the user's caseload page including the new student with Team Member checked." );
			u.takeScreenShot();
			
			SelectAUserWizardsPage.VerifyTestNewstudent(map.get("Criterialn").toString());
			test.log(LogStatus.PASS, "Return to display of the user's caseload page including the new student is verified" );
			
			SelectAUserWizardsPage.VerifyTestNewstudentChecked();
			test.log(LogStatus.PASS, "Verifying new student with Team Member checked" );
			
			SelectAUserWizardsPage.SwitchFrameToDefault(driver);
			
			HomePage.ClickSignOut();
		    test.log(LogStatus.PASS, "Clicking signout, user signs out of the application" );
		    
		    LoginPage.VerifyEdPlandefaultpage();
			test.log(LogStatus.PASS,  "EdPlan default page is displayed" );
			
			}
		
		//======================================Data Provider==========================================================================
		//=============================================================================================================================
	  	@DataProvider(name="excel")
	  	  public Object[][] dataSupplier() throws IOException {

	  	    File file = new File(System.getProperty("user.dir")+"\\src\\main\\java\\data\\TestDataNYC1.xlsx");
	  	    FileInputStream fis = new FileInputStream(file);

	  	    XSSFWorkbook wb = new XSSFWorkbook(fis);
	  	    XSSFSheet sheet = wb.getSheet(this.getClass().getSimpleName());
	  	    //XSSFSheet sheet = wb.getSheet("tmv_query1");
	  	    wb.close();
	  	    int lastRowNum = getRowCount(sheet);
	  	    int lastCellNum = sheet.getRow(0).getLastCellNum();
	  	    Object[][] obj = new Object[lastRowNum][1];
	  	    //System.out.println(this.getClass().getSimpleName());

	  	    for (int i = 0,k=0; i < sheet.getLastRowNum(); i++,k++) {
	  	      Map<Object, Object> datamap = new HashMap<Object, Object>();
	  	      datamap.put("RowNo",(i+2));
	  	      for (int j = 0; j < lastCellNum; j++) {
	  	        datamap.put(sheet.getRow(0).getCell(j).toString(), sheet.getRow(i+1).getCell(j).toString());
	  	      }
	  	      if(datamap.containsValue("*")) {
	  	    	  k--;
	  	      }else{
	  	    	  obj[k][0] = datamap;
	  	    	  //System.out.println("Datamap is "+datamap);
	  	      }

	  	    }
	  	    return  obj;
	  	  }
	      
	      public int getRowCount(XSSFSheet sheet) {
	      	int cnt = 0;
	      	for(int i = 0; i <= sheet.getLastRowNum(); i++) {
	      		if(!sheet.getRow(i).getCell(0).toString().equalsIgnoreCase("*")) {
	      			cnt++;
	      		}
	      	}
	        	return cnt-1;
	      }

	      @AfterTest
	    	public void closeBrowser(){
	    	driver.close();
	    	driver = null; 
	    	rep.endTest(test);
	    	rep.flush();
	    	
	    }	
	      
}
