package PAGES;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import testbase.base;
import uiActions.util;

public class SelectAUserWizardsPAGE extends base {
	SoftAssert softAssert = new SoftAssert();
	util u = new util();
    WebDriver driver;
 
    //======================================= SELECT A WIZARD PAGE ELEMENTS ======================================================
    //============================================================================================================================
    
    @FindBy(id="UserLastName") WebElement UserLastNameWizard;
    @FindBy(id="Submit") WebElement ViewUsers;
    @FindBy(xpath="//a[text()='Automation Supervisee']") WebElement AutomationSupervisee;
    @FindBy(xpath="//th[.='Student']/../following-sibling::tr[2]/td[2]") WebElement TestNewstudent;
    @FindBy(xpath="//th[.='Student']/../following-sibling::tr[2]/td[1]") WebElement TestNewstudentChecked;
    @FindBy(id="lbl_CaseManager") WebElement userscaseloadpage;
    @FindBy(xpath="//input[@value='Add More Students to Caseload']") WebElement AddMoreStudentstoCaseload;
    @FindBy(id="StudentLastName") WebElement StudentLastName;
    @FindBy(id="Submit") WebElement ViewStudents;
    @FindBy(id="Submit") WebElement AddStudentstoCaseload;
    @FindBy(xpath="//th[text()='Team Member']") WebElement UserCaseLoadPage;
    
    @FindBy(xpath="//input[@name='SubmitCancel']") WebElement NoDoNotRemoveStudents;
    //==================================== CODE ==================================================================================
    //============================================================================================================================
    public  void SelectAUserWizardsPageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    
    public void SelectAUserWizardsPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    } 
    //============================================================================================================================
    
    
    public void EnterUserLastNameWizard(String strUserLastNameWizard) {
    	this.UserLastNameWizard.sendKeys(strUserLastNameWizard);
	}
    
    public void Verifycaseloadsearchcriteriapage() {
    	this.UserLastNameWizard.isDisplayed();
	}
    
    public void ClickViewUsersSubmit() {
    	this.ViewUsers.click();
	}
    public void ClickAutomationSupervisee() {
    	this.AutomationSupervisee.click();
	}
    
    public void verifyAutomationSupervisee() {
    	u.waitForElementVisibility(driver, AutomationSupervisee, 30);
    	AutomationSupervisee.isDisplayed();
	}
    public void VerifyTestNewstudent(String TestNewstudentExp) {
    	u.waitForElementVisibility(driver, TestNewstudent, 30);
    	String TestNewstudentAct = TestNewstudent.getText().replaceAll("[,^Auto Student1 , Jr ]*", "").toString();
    	softAssert.assertTrue(TestNewstudentExp.equalsIgnoreCase(TestNewstudentAct));
	}
    
    public void VerifyAutomationSupervisee(String AutomationSuperviseeExp) {
    	u.waitForElementVisibility(driver, AutomationSupervisee, 30);
    	String AutomationSuperviseeAct = AutomationSupervisee.getText().replaceAll("[,^Automation ]*", "").toString();
    	softAssert.assertTrue(AutomationSuperviseeExp.equalsIgnoreCase(AutomationSuperviseeAct));
	}
    
    public void VerifyTestNewstudentChecked() {
    	TestNewstudentChecked.isEnabled();
	}
    public void userscaseloadpage() {
    	this.userscaseloadpage.isDisplayed();
    	u.takeScreenShot();
	}
    public void ClickAddMoreStudentstoCaseload() {
    	this.AddMoreStudentstoCaseload.click();
	}
    
    public void UserCaseLoadPage() {
    	UserCaseLoadPage.isDisplayed();
	}
    
    public void ClickDoNotRemoveStudents() {
    	
    }
    
    public void EnterStudentLastName(String strStudentLastName) {
    	this.StudentLastName.sendKeys(strStudentLastName);
	}
   
    
    public void ClickViewStudents() {
    	this.ViewStudents.click();
    }
    public void ClickAddStudentstoCaseload() throws InterruptedException {
    	u.waitToLoad();
    	this.AddStudentstoCaseload.click();
    }
    
}