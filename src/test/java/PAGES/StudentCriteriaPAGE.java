package PAGES;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import uiActions.util;

public class StudentCriteriaPAGE {
	
	util u = new util();
    WebDriver driver;
    
    //======================================= ACTIVE STUDENTS PAGE ELEMENTS ======================================================
    //============================================================================================================================
    @FindBy(xpath="//button[text()='Add Student']") WebElement AddStudentButton;
    @FindBy(xpath="//label[text()='Student ID']/preceding-sibling::input") WebElement StudentIDCriteria;
    @FindBy(xpath="//button[text()='View Students']") WebElement ViewStudents;
    @FindBy(xpath="//label[text()='Site']/preceding-sibling::select") WebElement SiteActiveStudent;
    @FindBy(xpath="//a[text()='Automation Test']") WebElement StudentAutomationTest;
    
    @FindBy(xpath="//label[contains(.,'First Name')]/preceding-sibling::input") WebElement FirstName;
    @FindBy(xpath="//label[contains(.,'Middle Name')]/preceding-sibling::input") WebElement MiddleName;
    @FindBy(xpath="//label[contains(.,'Last Name')]/preceding-sibling::input") WebElement LastName;
    @FindBy(xpath="//label[text()='NYC ID']/preceding-sibling::input") WebElement NYCID;
    @FindBy(xpath="//label[text()='Institution ID']/preceding-sibling::input") WebElement InstitutionID;
    @FindBy(xpath="//label[text()='Student ID']/preceding-sibling::input") WebElement StudentID;
    @FindBy(xpath="//h5[text()='Student Criteria']") WebElement StudentCriteria;
    @FindBy(xpath="//button[text()='Advanced Search']") WebElement AdvancedSearch;
    @FindBy(xpath="//label[text()='Occupational Therapy']") WebElement OccupationalTherapy;
    @FindBy(xpath="//label[text()='Physical Therapy']") WebElement PhysicalTherapy;
    @FindBy(xpath="//label[text()='Speech Therapy']") WebElement SpeechTherapy;
    @FindBy(xpath="//td[contains(.,'Auto Student1 Test, Jr')]/preceding-sibling::td/input") WebElement CheckBoxNewstudent;
    
    @FindBy(id="UserLastName") WebElement UserLastNameWizard;
    @FindBy(id="StudentLastName") WebElement StudentLastName;
    @FindBy(id="Submit") WebElement ViewUsers;
    
    @FindBy(xpath="//input[@id='StudentLastName']") WebElement StudentlastName;  
    @FindBy(xpath="//input[@value='View Inactive Students']") WebElement ViewInactiveStudentsButton;
    //========================================= COMMON FOR ALL PAGES ===========================================================
    //============================================================================================================================
    public  void StudentCriteriaPageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    
    public void StudentCriteriaPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    } 
    //==========================================================================================================================
    public void VerifyStudentCriteria(){
    	u.waitForElementVisibility(driver, StudentCriteria, 25);
    	StudentCriteria.isDisplayed();
    }
    
    public void ClickAddStudentButton(){
    	u.waitForElementClickable(driver, AddStudentButton, 3);
    	AddStudentButton.click();
    }
    public void EnterStudentIDCriteria(String strStudentIDCriteria) {	
		this.StudentIDCriteria.sendKeys(strStudentIDCriteria);	
	}
    public void ClickViewStudentsButton(){
    	u.waitForElementClickable(driver, ViewStudents, 15);
    	this.ViewStudents.click();
    }
    public void ClickStudentAutomationTest(){
    	StudentAutomationTest.click();
    }
    public void EnterFirstName(String strFirstName) {
    	u.waitForElementVisibility(driver, FirstName, 30);
    	this.FirstName.sendKeys(strFirstName);	
	}
    public void EnterMiddleName(String strMiddleName) {
    	u.waitForElementVisibility(driver, MiddleName, 30);
        this.MiddleName.sendKeys(strMiddleName);
	}
    public void EnterLastName(String strLastName) throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementVisibility(driver, LastName, 30);
    	this.LastName.sendKeys(strLastName);
	}
    
    public void EnterCriteriaLastName(String strUserLastNameWizard) {
    	this.UserLastNameWizard.sendKeys(strUserLastNameWizard);
	}
    
    public void EnterStudentLastname(String StudentLastName) {
    	this.StudentlastName.sendKeys(StudentLastName);
    }
    
    public void EnterNYCID(String strNYCID) {
    	u.waitForElementVisibility(driver, NYCID, 30);
    	this.NYCID.sendKeys(strNYCID);
	}
    public void EnterInstitutionID(String strInstitutionID) {
    	u.waitForElementVisibility(driver, InstitutionID, 30);
    	this.InstitutionID.sendKeys(strInstitutionID);
	}
    public void EnterStudentID(String strStudentID) {
    	u.waitForElementVisibility(driver, StudentID, 30);
    	this.StudentID.sendKeys(strStudentID);
	}
    public void ClickAdvancedSearch() {
    	u.waitForElementClickable(driver, AdvancedSearch, 15);
    	AdvancedSearch.click();
	}
    public void ClickOccupationalTherapy() {
    	u.waitForElement(driver, 30, OccupationalTherapy);
    	OccupationalTherapy.click();
	}
    public void ClickPhysicalTherapy() {
    	u.waitForElement(driver, 30, PhysicalTherapy);
    	PhysicalTherapy.click();
	}
    public void ClickSpeechTherapy() {
    	u.waitForElement(driver, 30, SpeechTherapy);
    	SpeechTherapy.click();
	}
    
    public void ClickCheckBoxNewstudent(String CheckBoxNewStudent) throws InterruptedException {
    	u.waitToLoad();
    	driver.findElement(By.xpath("//td[contains(.,'"+CheckBoxNewStudent+"')]/preceding-sibling::td/input")).click();
	}
    
    
    public void ClickViewInactiveStudentsButton() {
    	ViewInactiveStudentsButton.click();
    }
    
    public void ClickViewUsersSubmit() {
    	this.ViewUsers.click();
	}
}