package PAGES;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import testbase.base;
import uiActions.util;

public class ActiveStaffPAGE extends base {
	
	util u = new util();
    WebDriver driver;
    
    //======================================= ACTIVE STUDENTS PAGE ELEMENTS ======================================================
    //============================================================================================================================
    @FindBy(xpath="//span[text()='Clinical Supervisor - Institution']") WebElement ClinicalSupervisorInstitution;
    @FindBy(id="Submit") WebElement ViewUsers;
    @FindBy(xpath="//li[text()=' Automation User']") WebElement verifyAutomationUser;
    //==================================== COMMON CODE ==================================================================================
    //===================================================================================================================================
    public  void ActiveStaffPageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    public void ActiveStaffPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    } 
    //======================================================================================================================================
    
    public void ClickViewUsers() {
    	ViewUsers.click();
    }
    public void VerifyAutomationUser() {
    	u.waitForElementVisibility(driver, verifyAutomationUser, 30);
    	verifyAutomationUser.isDisplayed();
    }
    
}