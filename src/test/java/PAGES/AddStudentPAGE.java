package PAGES;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import uiActions.util;

public class AddStudentPAGE {
	
    
	util u = new util();
    WebDriver driver;
    //=================================== Student Elements ===============================================================
    //====================================================================================================================
    @FindBy(xpath="//input[@id='FirstName']") WebElement FirstName;
    @FindBy(xpath="//input[@id='MiddleName']") WebElement MiddleName;
    @FindBy(xpath="//input[@id='LastName']") WebElement LastName;
    @FindBy(xpath="//input[@id='Suffix']") WebElement Suffix;
    @FindBy(xpath="//input[@id='StudentCode']") WebElement NYCID;
    @FindBy(xpath="//input[@id='StateCode']") WebElement InstitutionID;
    @FindBy(xpath="//input[@id='DateOfBirth']") WebElement DateOfBirth;
    @FindBy(xpath="//input[@id='PlaceOfBirth']") WebElement PlaceOfBirth;
    @FindBy(xpath="//select[@id='GenderID']") WebElement GenderIDdropdown;
    @FindBy(xpath="//option[text()='Male']") WebElement GenderMale;
    @FindBy(xpath="//option[text()='Female']") WebElement GenderFemale;
    @FindBy(xpath="//input[@id='Language']") WebElement PrimaryLanguage;
    @FindBy(xpath="//select[@id='Grade']") WebElement Grade;
    @FindBy(xpath="//select[@id='LanguageOfInstruction']") WebElement LanguageOfInstruction;
    @FindBy(xpath="//select[@id='School']") WebElement Site;
    @FindBy(xpath="//input[@name='lRace']") WebElement Race;
    @FindBy(xpath="//select[@id='EnrollmentType']") WebElement EnrollmentType;
    @FindBy(xpath="//textarea[@id='Notes']") WebElement NotesStudent;
    //===================================Parent Elements===============================================================
    //=================================================================================================================
    @FindBy(xpath="//input[@id='ParentName']") WebElement ParentName;
    @FindBy(xpath="//select[@id='Relationship']") WebElement Relationship;
    @FindBy(xpath="//input[@id='Address']") WebElement Address;
    @FindBy(xpath="//input[@id='City']") WebElement City;
    @FindBy(xpath="//input[@id='State']") WebElement State;
    @FindBy(xpath="//input[@id='ZipCode']") WebElement ZipCode;
    @FindBy(xpath="//input[@id='Email']") WebElement Email;
    @FindBy(xpath="//input[@id='HomePhone']") WebElement HomePhone;
    @FindBy(xpath="//select[@id='NotificationsHomePhone']") WebElement HomePhoneNotifications;
    @FindBy(xpath="//input[@id='WorkPhone']") WebElement WorkPhone;
    @FindBy(xpath="//select[@id='NotificationsWorkPhone']") WebElement WorkPhoneNotifications;
    @FindBy(xpath="//textarea[@id='Comments']") WebElement CommentsParent;
    //====================================SUBMIT ELEMENT==============================================================
    @FindBy(id="Submit") WebElement AddStudenttoDatabase;
    @FindBy(name="ConfirmedAddStudent") WebElement ConfirmedAddStudent;
    @FindBy(id="Submit") WebElement UpdatetheDatabase;
    

    
    //==================================== CODE =====================================================================
    //===============================================================================================================
    public  void AddStudentriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    
    public void AddStudentPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }   
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    } 
        //======================================== STUDENT S =================================================
        //==========================================================================================================
    	public void EnterFirstName(String strFirstName) {	
    		this.FirstName.sendKeys(strFirstName);	
    	}
        public void EnterMiddleName(String strMiddleName) {
	        this.MiddleName.sendKeys(strMiddleName);
    	}
        public void EnterLastName(String strLastName) {
        	this.LastName.sendKeys(strLastName);
    	}
        public void EnterSuffix(String strSuffix) {
        	this.Suffix.sendKeys(strSuffix);
    	}
        public void EnterNYCID(String strNYCID) {
        	this.NYCID.sendKeys(strNYCID);
    	}
        public void EnterInstitutionID(String strInstitutionID) {
        	this.InstitutionID.sendKeys(strInstitutionID);
    	}
        public void EnterDateOfBirth(String strDateOfBirth) {
        	this.DateOfBirth.sendKeys(strDateOfBirth);
    	}
        public void EnterPlaceOfBirth(String strPlaceOfBirth) {
        	this.PlaceOfBirth.sendKeys(strPlaceOfBirth);
    	}
        public void SelectGenderIDdropdown(String strGenderIDdropdown) {
        	this.GenderIDdropdown.click();
        	if (strGenderIDdropdown.equalsIgnoreCase("Male")) {
        		this.GenderMale.click();
			} else if(strGenderIDdropdown.equalsIgnoreCase("Female")){
				this.GenderFemale.click();
			}
    	}
        public void SelectGrade(String strGrade) {
         	//this.Grade.click();	
    	    driver.findElement(By.xpath("//option[text()='" +strGrade+ "']")).click();
        }
        public void SelectPrimaryLanguage(String strPrimaryLanguage) {
         	//this.Grade.click();	
    	    driver.findElement(By.xpath("//option[text()='" +strPrimaryLanguage+ "']")).click();
        }
        public void SelectLanguageOfInstruction(String strLanguageOfInstruction) {
         	this.LanguageOfInstruction.click();	
        	driver.findElement(By.xpath("//select[@id='LanguageOfInstruction']/descendant::option[text()='"+strLanguageOfInstruction+"']")).click();
        }
        public void SelctingSite(String strSite) {
        	driver.findElement(By.xpath("//option[text()='" +strSite+ "']")).click();
        }
        public void SelectEnrollmentType(String strEnrollmentType) {
        	driver.findElement(By.xpath("//option[text()='" +strEnrollmentType+ "']")).click();
        }
        public void ClickingRace() {
        	this.Race.click();
        }
        public void EnterNotesStudent(String strNotesStudent) {	
    		this.NotesStudent.sendKeys(strNotesStudent);	
    	}
        
        //======================================== PARENT S ======================================================
        //==============================================================================================================
        public void EnterParentName(String strParentName) {	
    		this.ParentName.sendKeys(strParentName);	
    	}
        public void EnterRelationship(String strRelationship) {	
    		this.Relationship.click();	
    		driver.findElement(By.xpath("//option[text()='" +strRelationship+ "']")).click();
    	}
        public void EnterAddress(String strAddress) {	
    		this.Address.sendKeys(strAddress);	
    	}
        public void EnterCity(String strCity) {	
    		this.City.sendKeys(strCity);	
    	}
        public void EnterState(String strState) {	
    		this.State.sendKeys(strState);	
    	}
        public void EnterZipCode(String strZipCode) {	
    		this.ZipCode.sendKeys(strZipCode);	
    	}
        public void EnterEmail(String strEmail) {	
    		this.Email.sendKeys(strEmail);	
    	}
        public void EnterHomePhone(String strHomePhone) {	
    		this.HomePhone.sendKeys(strHomePhone);	
    	}
        public void SelectHomePhoneNotifications(String strHomePhoneNotifications) {
        	this.HomePhoneNotifications.click();
    		driver.findElement(By.xpath("//option[text()='" +strHomePhoneNotifications+ "']")).click();
    	}
        public void EnterWorkPhone(String strWorkPhone) {	
    		this.WorkPhone.sendKeys(strWorkPhone);	
    	}
        public void SelectWorkPhoneNotifications(String strWorkPhoneNotifications) {
        	this.WorkPhoneNotifications.click();
    		driver.findElement(By.xpath("//option[text()='" +strWorkPhoneNotifications+ "']")).click();
    	}
        public void EnterCommentsParent(String strCommentsParent) {	
    		this.CommentsParent.sendKeys(strCommentsParent);	
    	}
        //===================================== SUBMIT  ====================================================
        //========================================================================================================
        public void ClickAddStudenttoDatabase() {
        	this.AddStudenttoDatabase.click();
        }
        public void ClickConfirmedAddStudent() {
        	this.ConfirmedAddStudent.click();
        }
        public void ClickUpdatetheDatabase() {
        	this.UpdatetheDatabase.click();
        }
        
        
}