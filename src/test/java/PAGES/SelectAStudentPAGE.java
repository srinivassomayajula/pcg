package PAGES;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import testbase.base;
import uiActions.util;

public class SelectAStudentPAGE extends base {
	SoftAssert softAssert = new SoftAssert();
	util u = new util();
    WebDriver driver;
 
    //======================================= ACTIVE STUDENTS PAGE ELEMENTS ======================================================
    //============================================================================================================================
    @FindBy(xpath="//th[.='NYC ID #']/ancestor::thead/following-sibling::tbody/descendant::td[4]") WebElement NYCIDSearchResults;
    @FindBy(xpath="//th[text()='Name']/ancestor::thead/following-sibling::tbody/descendant::a") WebElement NewStudent;
    @FindBy(xpath="//td[text()='Name']/following-sibling::td[1]") WebElement NameVerifiy;
    @FindBy(xpath="//td[text()='NYC Student ID']/following-sibling::td[1]") WebElement NYCIDVerifiy;
    @FindBy(xpath="//td[text()='Date of Birth']/following-sibling::td[1]") WebElement DateofBirthVerifiy;
    @FindBy(xpath="//a[text()='Automation Test']") WebElement StudentAutomationTest;
    @FindBy(xpath="//a[contains(.,'Auto Student1 Test')]") WebElement AutoStudent1Test;
    @FindBy(xpath="//label[text()='Sort By']/preceding-sibling::select") WebElement SortBy;
    @FindBy(xpath="//button[text()='Export Results']") WebElement ExportResults;
    @FindBy(xpath="//h5[text()='Select a Student']") WebElement SelectaStudentHeading;
    @FindBy(xpath="//button[text()='Print Results']") WebElement PrintResults;
    @FindBy(xpath="//a[@class='lightbox-close']") WebElement PrintResultsClose;
    
    
    @FindBy(xpath="//a[contains(.,'Auto Student1 Test')]/../preceding-sibling::td/descendant::label") WebElement StudentDelete;
    @FindBy(xpath="//button[text()='Delete']") WebElement DeleteButton;
    @FindBy(xpath="//button[text()='Permanently Delete Students']") WebElement PermanentlyDeleteButton;
    @FindBy(xpath="//button[text()='Cancel']") WebElement CancelButton;
    
    @FindBy(xpath="//a[text()='Inactive Students']") WebElement InactiveStudents;
    @FindBy(xpath="//input[@id='StudentLastName']") WebElement LastName;
    @FindBy(xpath="//input[@id='Submit']") WebElement InactiveStudentsSubmit;
    @FindBy(xpath="//a[text()='Name']") WebElement Name;
    //========================================= COMMON FOR ALL PAGES ===========================================================
    //============================================================================================================================
    public  void SelectStudentPageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    
    public void SelectAStudentPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    } 
    
    //=============================================================================================================================
    public void ClickNewStudent() {
    	this.NewStudent.click();
	}
    public void ClickAutomationTest(){
    	u.waitForElementClickable(driver, StudentAutomationTest, 30);
    	//u.scrollTo(driver, StudentAutomationTest);
    	driver.findElement(By.xpath("//a[contains(.,'"+prop.getProperty("Student_Automation")+"')]")).click();
    	//StudentAutomationTest.click();
    }  
    public void ClickAutoStudent1Test(){
    	u.waitForElementClickable(driver, AutoStudent1Test, 40);
    	driver.findElement(By.xpath("//a[contains(.,'"+prop.getProperty("ClickOnStudent")+"')]")).click();
    	//AutoStudent1Test.click();
    }
    public void ClickExportResults(){
    	ExportResults.click();
    }
    public void VerifySelectaStudentHeading(){
    	u.waitForElementVisibility(driver, SelectaStudentHeading, 30);
    	u.takeScreenShot();
    	SelectaStudentHeading.isDisplayed();
    }
    public void ClickPrintResults(){
    	u.waitForElementClickable(driver, PrintResults, 5);
    	PrintResults.click();
    }
    public void ClickPrintResultsClose(){
    	u.waitForElementClickable(driver, PrintResultsClose, 30);
    	PrintResultsClose.click();
    }
    public void ClickStudentDelete(String strStudentDelete ) throws InterruptedException {
    	u.waitToLoad();
    	driver.findElement(By.xpath("//a[contains(.,'"+strStudentDelete+"')]/../preceding-sibling::td/descendant::label")).click();
    }
    public void ClickDeleteButton() {
    	DeleteButton.click();
    }
    public void ClickPermanentlyDeleteButton() {
    	u.waitForElementClickable(driver, PermanentlyDeleteButton, 40);
    	PermanentlyDeleteButton.click();
	}
    public void ClickCancelButton() {
    	u.waitForElementClickable(driver, CancelButton, 30);
    	CancelButton.click();
	}
    
    public void VerifyNYCIDSearhResults(String NYCIDSearhResultsExp) {
    	u.waitForElementVisibility(driver, NYCIDSearchResults, 30);
    	String NYCIDSearchResultsAct = NYCIDSearchResults.getText();
    	softAssert.assertTrue(NYCIDSearhResultsExp.equalsIgnoreCase(NYCIDSearchResultsAct));
    }
    
    public void ClickInactiveStudents() {
    	u.waitForElement(driver, 30, InactiveStudents);
    	InactiveStudents.click();
    }
    public void EnterLastName(String strLastName) {
    	this.LastName.sendKeys(strLastName);
	}
    public void SubmitButtonInactiveStudents() {
    	InactiveStudentsSubmit.click();
    }
    public void VerifyInactiveStudentsList() {
    	u.waitForElement(driver, 30, Name);
    	u.takeScreenShot();
    	Name.isDisplayed();
	}
    
    
}