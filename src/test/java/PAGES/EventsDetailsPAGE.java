package PAGES;

import java.util.HashMap;
import java.util.Map;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import testbase.base;
import uiActions.util;

public class EventsDetailsPAGE extends base {
	
	util u = new util();
    WebDriver driver;
    SoftAssert softAssert = new SoftAssert();
    //======================================= STUDENT SEARCH HISTORY PAGE ELEMENTS ====================================================
    //=================================================================================================================================
    
    @FindBy(xpath="//body[@id='EventDetails.htm']/descendant::h3") WebElement EventTypeEventDetailspage;
    @FindBy(xpath="//td[text()='Date:']/following-sibling::td") WebElement DateEventDetailspage;
    @FindBy(xpath="//td[text()='Consent:']/following-sibling::td") WebElement ConsentEventDetailspage;
    @FindBy(xpath="//td[text()='Site:']/following-sibling::td") WebElement SiteEventDetailspage;
    @FindBy(xpath="//td[text()='Created By:']/following-sibling::td") WebElement CreatedByEventDetailsPage;
    
    
    
  //========================================= COMMON FOR ALL PAGES ==================================================================
    public  void EventsDetailsPageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    public void EventsDetailsPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    }
    //=================================================================================================================================
    
    
    public String VerifyEventType(String EventTypeEventStudentHistorypage) {
    	String EventTypeEventDetails = EventTypeEventDetailspage.getText().replaceFirst("Event Type: ", "" ).toString(),returnEventType = null;
    	softAssert.assertEquals(EventTypeEventDetails, EventTypeEventStudentHistorypage);
    	System.out.println(EventTypeEventDetails);
		return returnEventType;
    }
    
    public String VerifyDateEventDetailspage(String DateEventParentConsentpage) {
    	String DateEventDetails =  DateEventDetailspage.getText().toString(),returnDate = null;
    	softAssert.assertEquals(DateEventDetails, DateEventParentConsentpage);
    	return  returnDate ;
    }
    
    
    public String VerifyConsentEventDetailspage(String ConsentTypeParentConsentpage) {
    	String ConsentEventDetails = ConsentEventDetailspage.getText().replace("Parent Medicaid Consent: " , "").toString(),returnConsent = null;
    	softAssert.assertEquals(ConsentEventDetails, ConsentTypeParentConsentpage);
    	  return returnConsent;
    }
    
    public String VerifySiteEventDetailspage(String SiteStudentCenterpage) {
    	String  SiteEventDetails = SiteEventDetailspage.getText().toString(),returnSite = null;
    	softAssert.assertEquals(SiteEventDetails, SiteStudentCenterpage);
    	return returnSite;
    }
    
    public String VerifyCreatedByEventDetailsPage() {
    	return CreatedByEventDetailsPage.getText().toString();
    }
    
    
}