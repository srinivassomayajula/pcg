package PAGES;

import java.util.HashMap;
import java.util.Map;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import testbase.base;
import uiActions.util;

public class StudentHistoryPAGE extends base {
	
	util u = new util();
    WebDriver driver;
    SoftAssert softAssert = new SoftAssert();
    //======================================= STUDENT SEARCH HISTORY PAGE ELEMENTS ====================================================
    //=================================================================================================================================
    @FindBy(xpath="//a[text()='Automation Supervisee']/../preceding-sibling::td[1]") WebElement LogonBehalf;
    @FindBy(xpath="//a[text()='Event ID']") WebElement EventID;
    @FindBy(xpath="//a[text()='Event Date*']") WebElement EventDate;
    @FindBy(xpath="//a[text()='Event Type']") WebElement EventType;
    @FindBy(xpath="//a[text()='Begin Date']") WebElement BeginDate;
    @FindBy(xpath="//a[text()='End Date']") WebElement EndDate;
    @FindBy(xpath="//a[text()='User']") WebElement User;
    @FindBy(xpath="//a[text()='Document']") WebElement Document;
    @FindBy(xpath="//a[text()='Date Created']") WebElement DateCreated;
    
    
    @FindBy(xpath="//tr[@aria-label='Currently Valid Medicaid Parent Consent']/descendant::input[@value='Details']") WebElement Details;
    @FindBy(xpath="//tr[@aria-label='Currently Valid Medicaid Parent Consent']/descendant::b") WebElement CurrentEventTypeStudentHistorypage;
    @FindBy(xpath="//tr[@aria-label='Currently Valid Medicaid Parent Consent']/descendant::td[2]") WebElement CurrentDateCreatedStudentHistorypage;
    @FindBy(xpath="//tr[@aria-label='Currently Valid Medicaid Parent Consent']/td[3]") WebElement RecentParentConsentColor;
    //========================================= COMMON FOR ALL PAGES ==================================================================
    public  void StudentHistoryPageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    public void StudentHistoryPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    }
    
    //================================================================================================================================
    
      public  String ReadTableData() {
    	  int lastCellTableData = driver.findElements(By.xpath("(//table[@id='EventsTable']/descendant::input[@name='EventIDs']")).size();
			//System.out.println(lastCellTableData);
			Map<Object, Object> TableData = new HashMap<Object, Object>();
			String TableDATA = null;
	  	      for (int j = 1; j <=lastCellTableData; j++) { 
	  	    	TableData.put(driver.findElement(By.xpath("//table[@id='EventsTable']/descendant::a[text()='Event ID']")).getText(), driver.findElement(By.xpath("(//table[@id='EventsTable']/descendant::input[@name='EventIDs']/..)["+j+"]")).getText());
	  	    	TableData.put(driver.findElement(By.xpath("//table[@id='EventsTable']/descendant::a[text()='Event Date*']")).getText(), driver.findElement(By.xpath("(//a[text()='Event Date*']/ancestor::table/tbody/descendant::input[@name='EventIDs']/../following-sibling::td[1])["+j+"]")).getText());
	  	    	TableData.put(driver.findElement(By.xpath("//table[@id='EventsTable']/descendant::a[text()='Event Type']")).getText(), driver.findElement(By.xpath("(//a[text()='Event Type']/ancestor::table/tbody/descendant::input[@name='EventIDs']/../following-sibling::td[2])["+j+"]")).getText());
	  	    	TableData.put(driver.findElement(By.xpath("//table[@id='EventsTable']/descendant::a[text()='Begin Date']")).getText(), driver.findElement(By.xpath("(//a[text()='Begin Date']/ancestor::table/tbody/descendant::input[@name='EventIDs']/../following-sibling::td[3])["+j+"]")).getText());
	  	    	TableData.put(driver.findElement(By.xpath("//table[@id='EventsTable']/descendant::a[text()='End Date']")).getText(), driver.findElement(By.xpath("(//a[text()='End Date']/ancestor::table/tbody/descendant::input[@name='EventIDs']/../following-sibling::td[4])["+j+"]")).getText());
	  	    	TableData.put(driver.findElement(By.xpath("//table[@id='EventsTable']/descendant::a[text()='User']")).getText(), driver.findElement(By.xpath("(//a[text()='User']/ancestor::table/tbody/descendant::input[@name='EventIDs']/../following-sibling::td[5])["+j+"]")).getText());
	  	    	TableData.put(driver.findElement(By.xpath("//table[@id='EventsTable']/descendant::a[text()='Document']")).getText(), driver.findElement(By.xpath("(//a[text()='Document']/ancestor::table/tbody/descendant::input[@name='EventIDs']/../following-sibling::td[6])["+j+"]")).getText());
	  	    	TableData.put(driver.findElement(By.xpath("//table[@id='EventsTable']/descendant::a[text()='Date Created']")).getText(), driver.findElement(By.xpath("(//a[text()='Date Created']/ancestor::table/tbody/descendant::input[@name='EventIDs']/../following-sibling::td[7])["+j+"]")).getText());
	  	    	/*if(TableDATA.contains(null))
	  	    		TableDATA=TableData.toString();
	  	    	else*/
	  	    		TableDATA = TableDATA+System.lineSeparator()+TableData;
	  	      }
	  	    u.takeScreenShot();
		return TableDATA;  
      }
      
      public  String ReadEventIDLatestData() {
    	  int lastRowTableData = driver.findElements(By.xpath("//tr[@aria-label='Currently Valid Medicaid Parent Consent']/td")).size();
			Map<Object, Object> EventIDData = new HashMap<Object, Object>();
			String LatestEventIDDATA = null;
	  	      for (int j = 1; j <=8; j++) { 
	  	    	EventIDData.put(driver.findElement(By.xpath("//table[@id='EventsTable']/descendant::a[text()='Event ID']")).getText(), driver.findElement(By.xpath("(//tr[@aria-label='Currently Valid Medicaid Parent Consent']/td)["+j+"]")).getText());
	  	    	LatestEventIDDATA = LatestEventIDDATA+EventIDData;
	  	      }
	  	    u.takeScreenShot();
		return LatestEventIDDATA;  
      }
      
      public void VeifyStudentHistorypage() {
    	  EventID.isDisplayed();
      }
      
      public void ClickEventID() {
    	  EventID.click();
      }
      
      public void ClickEventDate() {
    	  EventDate.click();
      }
      
      public void ClickEventType() {
    	  EventType.click();
      }
      
      public void ClickBeginDate() {
    	  BeginDate.click();
      }
      
      public void ClickEndDate() {
    	  EndDate.click();
      }
      
      public void ClickUser() {
    	  User.click();
      }
      
      public void ClickDocument() {
    	  Document.click();
      }
      
      public void ClickDateCreated() {
    	  DateCreated.click();
      }
      
      public void ClickDetails() {
    	  Details.click();
      }
      
      public String CurrentEventTypeStudentHistory() {
    	 String CurrentEventTypeStudentHistory =  CurrentEventTypeStudentHistorypage.getText().toString();
    	 System.out.println(CurrentEventTypeStudentHistory);
		return CurrentEventTypeStudentHistory;  
      }
      
	public void VerifyRecentParentConsentColor() {
		String colorString = RecentParentConsentColor.getCssValue("background-color");
    	  //String colorString =  RecentParentConsentColor.getAttribute("background-color").split("#").toString();
    	  softAssert.assertTrue(colorString.equals("926dff"));
    	  u.takeScreenShot();
    	  System.out.println(colorString);
      }
      
	public String CurrentDateCreatedStudentHistorypage() {
   	 String CurrentDateCreatedStudentHistory =  CurrentDateCreatedStudentHistorypage.getText().toString();
   	 System.out.println(CurrentDateCreatedStudentHistory);
		return CurrentDateCreatedStudentHistory;  
     }
}