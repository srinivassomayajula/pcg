package PAGES;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import freemarker.core.ReturnInstruction.Return;
import uiActions.util;

public class StudentCenterPAGE {
	
	util u = new util();
    WebDriver driver;
    SoftAssert softAssert = new SoftAssert();
    
    //======================================= ACTIVE STUDENTS PAGE ELEMENTS ======================================================
    //============================================================================================================================
    @FindBy(xpath="//h5[.='CMR Demographic Snapshot']") WebElement VerifyCMRDemographicSnapshot;
    @FindBy(xpath="//label[text()='Name']/preceding-sibling::p") WebElement NameVerify;
    @FindBy(xpath="//label[text()='Name']/preceding-sibling::p") WebElement NameVerifyCheckbox;
    @FindBy(xpath="//label[text()='NYC Student ID']/preceding-sibling::p") WebElement NYCIDVerify;
    @FindBy(xpath="//label[text()='Date of Birth']/preceding-sibling::p") WebElement DateofBirthVerify;
    @FindBy(xpath="//label[text()='Site Name']/preceding-sibling::p") WebElement SiteVerify;
    @FindBy(xpath="//label[text()='Home Language']/preceding-sibling::p") WebElement HomeLanguageVerify;
    @FindBy(xpath="//label[text()='Language of Instruction']/preceding-sibling::p") WebElement LanguageofInstructionVerify;
    @FindBy(xpath="//label[text()='Gender']/preceding-sibling::p") WebElement GenderVerify;
    
    
    
    
    
    @FindBy(xpath="//h5[contains(.,'Student Center')]") WebElement StudentCenterHeader;
    @FindBy(xpath="//button[text()='Create Document Batch']") WebElement CreateDocumentBatch;
    
    @FindBy(xpath="//p[text()='IEP Mandates']") WebElement IEPMandatesTile;
    @FindBy(xpath="//p[text()='Student History']") WebElement StudentHistoryTile;
    @FindBy(xpath="//p[text()='Parent Consent']") WebElement ParentConsentTile;
    @FindBy(xpath="//p[text()='Order for Service']") WebElement OrderForServiceTile;
    
    @FindBy(xpath="(//p[contains(text(),'Automation Test')])[3]") WebElement studentName;
	 @FindBy(xpath="//p[contains(text(),'AUT123')]") WebElement NYCID;
	 @FindBy(xpath="//p[contains(text(),'06/17/2016')]") WebElement DateOfBirth;
    
    @FindBy(xpath="//p[contains(text(),'Speech E-Referral')]") WebElement SpeechEReferralTile;
    
    @FindBy(xpath="//th[text()='Document']/ancestor::thead/following-sibling::tbody/tr/td/a") WebElement DownloadedDocuments;
    
    //========================================= COMMON FOR ALL PAGES ===========================================================
    public  void StudentCenterDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    
    public void StudentCenterPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    } 
//=============================== For NYC 1 =========================================================================================
    
    public void VerifyCMRDemographicSnapshot() {
    	VerifyCMRDemographicSnapshot.isDisplayed();
    }
    
    public String VerifyCMRName(String strNameVerify) {
    	String NameVerify = this.NameVerify.getText().replaceAll("[,^ ]*", "").toString(),returnName = null;
    	if (strNameVerify.equalsIgnoreCase(NameVerify)) {
			System.out.println("True");
			returnName="["+NameVerify+"] and ["+strNameVerify+"] strings match";
		} else {
			System.out.println("False");
			returnName="["+NameVerify+"] and ["+strNameVerify+"] strings doesn't match";
		}
    	return returnName;
    }
    public String VerifyCMRNYCID(String strNYCIDVerify) {
    	String NYCIDVerify = this.NYCIDVerify.getText().replaceAll("[,^Jr ]*", "").toString(),returnNYC = null;
    	if (strNYCIDVerify.equalsIgnoreCase(NYCIDVerify)) {
			System.out.println("true");
			returnNYC="["+NYCIDVerify+"] and ["+strNYCIDVerify+"] strings match";
		} else {
			System.out.println("false");
			returnNYC="["+NYCIDVerify+"] and ["+strNYCIDVerify+"] strings doesn't match";
		}
    	return returnNYC;
    }
    
    public String ForCheckbox() {
    	String NameCheckbox = this.NameVerifyCheckbox.getText().toString();
		return NameCheckbox;	
    }
    
    public String VerifyDateofBirth(String strDateofBirthVerify) {
    	String DateofBirthVerify = this.DateofBirthVerify.getText().replaceAll("[,^Jr ]*", "").toString(),returnDOB = null;
    	if (strDateofBirthVerify.equalsIgnoreCase(DateofBirthVerify)) {
			System.out.println("true");
			returnDOB="["+DateofBirthVerify+"] and ["+strDateofBirthVerify+"] strings match";
		} else {
			System.out.println("false");
			returnDOB="["+DateofBirthVerify+"] and ["+strDateofBirthVerify+"] strings doesn't match";
		}
    	return returnDOB;
    }
    
    public String VerifySite(String StrSiteVerify) {
    	String SiteVerify = this.SiteVerify.getText().toString(),returnSite = null;
    	softAssert.assertEquals(SiteVerify, StrSiteVerify);
    	returnSite = "["+SiteVerify+"] and ["+StrSiteVerify+"] strings match";
    	return returnSite;
    }
    
    public String VerifyHomeLanguage(String StrPrimarylanguageVerify) {
    	String HomeLanguageVerify = this.HomeLanguageVerify.getText().toString(),returnHomeLanguage = null;
    	if (StrPrimarylanguageVerify.equalsIgnoreCase(HomeLanguageVerify)) {
    		System.out.println("true");
    		returnHomeLanguage="["+HomeLanguageVerify+"] and ["+StrPrimarylanguageVerify+"] strings match";
		} else {
			System.out.println("false");
			returnHomeLanguage="["+HomeLanguageVerify+"] and ["+StrPrimarylanguageVerify+"] strings doesn't match";
		}
    	return returnHomeLanguage;
    }
    
    public String VerifyLanguageofInstruction(String StrLanguageofInstructionVerify) {
    	
    	String LanguageofInstructionVerify = this.LanguageofInstructionVerify.getText().toString(),returnLanguageofInstruction = null;
    	softAssert.assertEquals(LanguageofInstructionVerify, StrLanguageofInstructionVerify);
    	returnLanguageofInstruction="["+LanguageofInstructionVerify+"] and ["+StrLanguageofInstructionVerify+"] strings match";
    	/*
    	if (StrLanguageofInstructionVerify.equalsIgnoreCase(LanguageofInstructionVerify)) {
    		System.out.println("true");
		} else {
			System.out.println("false");
		}*/
    	return returnLanguageofInstruction;
    }
    
    public String VerifyGender(String StrGenderVerify) {
    	String GenderVerify = this.GenderVerify.getText().toString(),returnGender = null;
    	softAssert.assertEquals(GenderVerify, StrGenderVerify);
    	returnGender="["+GenderVerify+"] and ["+StrGenderVerify+"] strings match";
    	/*if (StrGenderVerify.equalsIgnoreCase(GenderVerify)) {
    		System.out.println("true");
		} else {
			System.out.println("false");
		}*/
    	return returnGender;
    }
    
    public String SiteStudentCenterPage() {
		return SiteVerify.getText().toString();
    }
    
    
//===================================== For NYC 2 ================================================================================
    public void VerifyStudentCenterHeader() {
    	StudentCenterHeader.isDisplayed();
    }
    public void ScrollCreateDocumentBatchScroll() {
    	u.scrollTo(driver, CreateDocumentBatch);
    }
    
    
    public void ClickIEPMandatesTile() {
    	IEPMandatesTile.click();
    }
    
    public void ClickStudentHistoryTile() {
    	u.waitForElementVisibility(driver, StudentHistoryTile, 30);
    	StudentHistoryTile.click();
    }
    
    public void ClickParentConsentTile() {
    	u.waitForElementClickable(driver, ParentConsentTile, 30);
    	ParentConsentTile.click();
    }
    
    public void ClickOrderForServiceTile() {
    	u.waitForElementClickable(driver, OrderForServiceTile, 30);
    	OrderForServiceTile.click();
    }
    
    public void VerifyStudentHistoryTile() {
    	StudentHistoryTile.isDisplayed();
    }
    
    public void ClickSpeechEReferralTile1() {
        SpeechEReferralTile.click();
        }
    
    
    public void studentProfileData(HashMap<String, String> map)
    {
    	 map.put("Studentname", studentName.getText());
    	 map.put("StudentDateOfBirth", DateOfBirth.getText());
    	 map.put("StudentNYCID", NYCID.getText()); 
    }
    
    public void VerifyDownloadedDocumentsTable() {
    	u.scrollTo(driver, DownloadedDocuments);
    	DownloadedDocuments.isDisplayed();
    	u.takeScreenShot();
    }
    
}