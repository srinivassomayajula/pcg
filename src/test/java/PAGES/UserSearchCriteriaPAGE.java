package PAGES;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import testbase.base;
import uiActions.util;

public class UserSearchCriteriaPAGE extends base {
	
	util u = new util();
    WebDriver driver;
    //======================================= USER SEARCH CRITERIA PAGE ELEMENTS ======================================================
    //============================================================================================================================
    @FindBy(xpath="//span[text()='Clinical Supervisor - Institution']") WebElement ClinicalSupervisorInstitution;
    @FindBy(id="Submit") WebElement ViewUsers;
    
    
    
    public  void UserSearchCriteriaDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    public void UserSearchCriteriaPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    } 
    
 
    public void ClickClinicalSupervisorInstitution() {
    	ClinicalSupervisorInstitution.click();
    }
    
    public void ClickViewUsers() {
    	ViewUsers.click();
    }
    
}