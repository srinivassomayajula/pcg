package PAGES;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import uiActions.util;

public class SelectAWizardPAGE {
	
	util u = new util();
    WebDriver driver;
 
    //======================================= SELECT A WIZARD PAGE ELEMENTS ======================================================
    //============================================================================================================================

    @FindBy(xpath="//a[contains(.,'Caseload Administration Wizard')]") WebElement CaseloadAdministration;
    @FindBy(xpath="//input[@value='No, Do Not Remove Students']") WebElement DNotRemoveStudents;
    @FindBy(xpath="//h4[text()='Select a Wizard']") WebElement SelectaWizardModal;
    
    
    //==================================== CODE ==================================================================================
    //============================================================================================================================
    public  void SelectAWizardPageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    
    public void SelectAWizardPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    } 
    
    public void ClickCaseloadAdministration() {
    	u.waitForElementClickable(driver, CaseloadAdministration, 15);
    	this.CaseloadAdministration.click();
	}
    
    public void ClickDoNotRemoveStudents() {
    	DNotRemoveStudents.click();
    }
    public void SelectaWizardModal() {
    	u.waitForElementVisibility(driver, SelectaWizardModal, 30);
    	SelectaWizardModal.isDisplayed();
    	u.takeScreenShot();
    }
    
    
    
    
    
    
}