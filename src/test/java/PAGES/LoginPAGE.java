package PAGES;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.LogStatus;

import testbase.base;
import uiActions.util;

public class LoginPAGE extends base {
	util u = new util();
    static WebDriver driver;
    
    //======================================= LOGIN ELEMENTS =================================================================
    //========================================================================================================================
    @FindBy(name="Name") WebElement userName;
    @FindBy(name="Password") WebElement password;
    @FindBy(id="login") WebElement login;
    @FindBy(xpath="//img[@src='/images/edplan_plain_dark.png']") WebElement EdEdPlandefaultpage;
    //======================================== CODE =========================================================================
    //=======================================================================================================================
	public  void LoginDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
	public void VerifyEdPlandefaultpage() {
		u.waitForElementVisibility(driver, EdEdPlandefaultpage, 30);
		EdEdPlandefaultpage.isDisplayed();
    }
    
    public void loginToPCG(String strUserName , String strPasword) {
    	userName.sendKeys(strUserName); 
    	password.sendKeys(strPasword);
    	login.click();
    }
	
    
}
