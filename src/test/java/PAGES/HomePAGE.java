package PAGES;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.LogStatus;

import testbase.base;
import uiActions.util;

public class HomePAGE extends base{
	

	util u = new util();
    WebDriver driver;
    
    //======================================= HOME PAGE ELEMENTS =================================================================
    //============================================================================================================================
    @FindBy(xpath="//a[text()='Students']") WebElement StudentMenu;
    @FindBy(xpath="//a[text()='Active Students']") WebElement ActiveStudentsMenu;
    @FindBy(xpath="//a[text()='Inactive Students']") WebElement InactiveStudentsMenu;
    @FindBy(xpath="//a[text()='Recent Students']") WebElement RecentStudentsMenu;
    @FindBy(xpath="//a[text()='Parents']") WebElement ParentsMenu;
    
    @FindBy(xpath="//a[text()='Wizards']") WebElement WizardsMenu;
    @FindBy(id="linkStaff") WebElement StaffMenu;
    @FindBy(xpath="//a[text()='Active Staff']") WebElement ActiveStaffMenu;
    @FindBy(xpath="//a[text()='Inactive Staff']") WebElement InactiveStaffMenu;
    @FindBy(xpath="//a[text()='Student Center']") WebElement StudentCenterMenu;
    
    @FindBy(xpath="(//a[text()='IEP Mandates'])[1]") WebElement IEPMandatesTab;
    @FindBy(xpath="//li[text()='* Automation Supervisee']") WebElement AutomationSuperviseeHeader;
    @FindBy(xpath="//img[@src='images/edplan_menu_logo_light_small.png']") WebElement EdPlanhomepage;
    @FindBy(xpath="//span[text()='exit_to_app']") WebElement SignOut;
    
    @FindBy(xpath="//a[text()='Inactive Students']") WebElement InactiveStudents;
    //========================================= COMMON FOR ALL PAGES ===========================================================
    //===========================================================================================================================
    public  void HomePageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    
    //==========================================================================================================================
    public void VerifyEdPlanhomepage(){
    	u.waitForElementVisibility(driver, EdPlanhomepage, 30);
    	EdPlanhomepage.isDisplayed();
    	u.takeScreenShot();
    }
    public void ClickStudentMenu(){
    	StudentMenu.click();
    }
    public void ClickActiveStudentsMenu() {
    	u.waitForElement(driver, 30, ActiveStudentsMenu);
    	ActiveStudentsMenu.click();
	}
    public void VerifyActiveAndInactiveStudentsMenu() {
    	u.waitForElementVisibility(driver, ActiveStudentsMenu, 30);
    	ActiveStudentsMenu.isDisplayed();
    	InactiveStudentsMenu.isDisplayed();
	}
    public void VerifyRecentStudentsMenu() {
    	RecentStudentsMenu.isDisplayed();
	}
    public void VerifyParentsMenu() {
    	ParentsMenu.isDisplayed();
	}
    public void ClickInactiveStudents() {
    	u.waitForElement(driver, 30, InactiveStudents);
    	InactiveStudents.click();
    }
    public void ClickWizardsMenu(){
    	WizardsMenu.click();
    }
    public void ClickStaffMenu(){
    	StaffMenu.click();
    }
    public void ClickActiveStaffMenu() throws InterruptedException {
    	u.waitForElementClickable(driver, ActiveStaffMenu, 30);
    	ActiveStaffMenu.click();
    }
    
    public void ClickStudentCenterMenu() throws InterruptedException {
    	u.waitToLoad();
    	StudentCenterMenu.click();
    }
    
    public void VerifyActiveAndInactiveStaffStaffMenu() {
    	u.waitForElementVisibility(driver, ActiveStaffMenu, 30);
    	ActiveStaffMenu.isDisplayed();
    	InactiveStaffMenu.isDisplayed();
    }
    public void VerifyAutomationSuperviseeHeader() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementVisibility(driver, AutomationSuperviseeHeader, 30);
    	AutomationSuperviseeHeader.isDisplayed();
    }
    public void ClickSignOut() {
    	u.takeScreenShot();
    	this.SignOut.click();
    }
    public void ClickIEPMandatesTab() {
    	IEPMandatesTab.click();
    }

	public void ClickViewStudents() {
		// TODO Auto-generated method stub
		
	}
    
    
    
    
    
    
}