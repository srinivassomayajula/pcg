package PAGES;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import testbase.base;
import uiActions.util;

public class SelectUserPAGE extends base {
	
	util u = new util();
    WebDriver driver;
    //======================================= USER SEARCH CRITERIA PAGE ELEMENTS ======================================================
    //=================================================================================================================================
    @FindBy(xpath="//a[text()='Automation Supervisee']/../preceding-sibling::td[1]") WebElement LogonBehalf;
    @FindBy(xpath="//li[text()=' Automation User']") WebElement verifyAutomationUser;
    
    
    //========================================= COMMON FOR ALL PAGES ===========================================================

    public  void SelectUserPageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    public void SelectUserPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    } 
    
    
    //====================================================================================================================================
    
    public void ClickLogonBehalf() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElement(driver, 30, LogonBehalf);
    	//u.waitForElementClickable(driver, LogonBehalf, 25);
    	LogonBehalf.click();
    }
    
    public void VerifyAutomationUser() {
    	u.waitForElementVisibility(driver, verifyAutomationUser, 30);
    	verifyAutomationUser.isDisplayed();
    }
    
}