package PAGES;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import testbase.base;
import uiActions.util;

public class IEPMandatesPAGE extends base {
	

	util u = new util();
    WebDriver driver;
    
    //======================================= HOME PAGE ELEMENTS =================================================================
    //============================================================================================================================
    @FindBy(xpath="//h5[text()='IEP Mandates']") WebElement IEPMandatesHeader;
    @FindBy(xpath="//span[text()='chevron_left']") WebElement chevron_leftArrow;
    @FindBy(xpath="//p[text()='Student Center']") WebElement StudentCenterDropDown;
    @FindBy(xpath="//a[text()='Parent Consent']") WebElement ParentConsent;
    @FindBy(xpath="//h5[text()='Create Parent Consent Document']") WebElement CreateParentConsentDocument;
    @FindBy(xpath="//a[text()='IEP Mandates']") WebElement IEPMandates;
    @FindBy(xpath="//a[text()='Order for Service']") WebElement OrderforService;
    @FindBy(xpath="//h5[text()='Create Order for Service']") WebElement CreateOrderforService;
    @FindBy(xpath="//a[text()='Speech E-Referral']") WebElement SpeechEReferral;
    @FindBy(xpath="//h5[text()='Speech Therapy E-Referral']") WebElement SpeechTherapyEReferral;
    @FindBy(xpath="//a[contains(.,'Order & Referral History')]") WebElement OrderReferralHistory;
    @FindBy(xpath="//h5[text()='Orders/Referral History']") WebElement OrdersReferralHistoryHeader;
    @FindBy(xpath="//a[contains(.,'UDO Acknowledgement Form')]") WebElement UDOAcknowledgementForm;
    @FindBy(xpath="//h5[text()='Acknowledgement of IEP Responsibilities']") WebElement AcknowledgementofIEPResponsibilities;
    
    @FindBy(xpath="//label[contains(.,'entries')]") WebElement Entries;
    
    @FindBy(xpath="(//label[text()='Search:']/input)[1]") WebElement IEPSearchBox;
  
    //==================================== CODE =================================================================================
    //===========================================================================================================================
    
    public  void IEPMandatesPageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    
    
    public void IEPMandatesHeader() {
    	u.waitForElement(driver, 40, IEPMandates);
    	IEPMandates.isDisplayed();
    	u.takeScreenShot();
    }
    public void ClickChevronleftArrow() {
    	u.waitForElement(driver, 40, chevron_leftArrow);
    	chevron_leftArrow.click();
    }
    public void VerifyStudentCenterDropDown() {
    	u.waitForElementVisibility(driver, StudentCenterDropDown, 40);
    	StudentCenterDropDown.isDisplayed();
    	u.takeScreenShot();
    }
    public void ClickParentConsent() {
    	u.waitForElementVisibility(driver, ParentConsent, 40);
    	ParentConsent.click();
    }
    public void CreateParentConsentDocument() {
    	u.waitForElementVisibility(driver, CreateParentConsentDocument, 40);
    	CreateParentConsentDocument.isDisplayed();
    	u.takeScreenShot();
    }
    public void IEPMandates() {
    	u.waitForElementClickable(driver, IEPMandates, 40);
    	IEPMandates.click();
    }
    public void ClickOrderforService() {
    	u.waitForElementClickable(driver, OrderforService, 40);
    	OrderforService.click();
    }
    public void CreateOrderforService() {
    	u.waitForElementVisibility(driver, CreateOrderforService, 40);
    	CreateOrderforService.isDisplayed();
    	u.takeScreenShot();
    }
    public void ClickSpeechEReferral() {
    	u.waitForElementClickable(driver, SpeechEReferral, 40);
    	SpeechEReferral.click();
    }
    public void ClickSpeechTherapyEReferral() {
    	u.waitForElementVisibility(driver, SpeechTherapyEReferral, 40);
    	SpeechTherapyEReferral.isDisplayed();
    	u.takeScreenShot();
    }
    public void ClickOrderReferralHistory() {
    	u.waitForElementClickable(driver, OrderReferralHistory, 40);
    	OrderReferralHistory.click();
    }
    public void OrdersReferralHistoryHeader() {
    	u.waitForElementVisibility(driver, OrdersReferralHistoryHeader, 40);
    	OrdersReferralHistoryHeader.isDisplayed();
    	u.takeScreenShot();
    }
    public void ClickUDOAcknowledgementForm() {
    	u.waitForElementClickable(driver, UDOAcknowledgementForm, 40);
    	UDOAcknowledgementForm.click();
    }
    public void AcknowledgementofIEPResponsibilities() {
    	u.waitForElementVisibility(driver, AcknowledgementofIEPResponsibilities, 40);
    	AcknowledgementofIEPResponsibilities.isDisplayed();
    	u.takeScreenShot();
    }
    public void Entries() {
    	u.waitForElementVisibility(driver, Entries, 40);
    	Entries.isDisplayed();
    	u.takeScreenShot();
    }


	public void EnterIEPSearchBox(String SearchItem) throws InterruptedException {
		u.waitToLoad();
		this.IEPSearchBox.sendKeys(SearchItem);
	}
    
	 public  String IEPMandatesRelativeServicesTabularForm() {
   	  int lastCellTableData = driver.findElements(By.xpath("//th[text()='IEP Mandate']/ancestor::thead/following-sibling::tbody/tr/td[1]")).size();
			//System.out.println(lastCellTableData);
			Map<Object, Object> TableData = new HashMap<Object, Object>();
			String TableDATA = null;
	  	      for (int j = 1; j <=lastCellTableData; j++) { 
	  	    	TableData.put(driver.findElement(By.xpath("//th[text()='IEP Mandate']/ancestor::div[2]/descendant::th[text()='IEP Mandate']")).getText(), driver.findElement(By.xpath("(//th[text()='IEP Mandate']/ancestor::thead/following-sibling::tbody/tr/td[1])["+j+"]")).getText());
	  	    	TableData.put(driver.findElement(By.xpath("//th[text()='IEP Mandate']/ancestor::div[2]/descendant::th[text()='Start Date']")).getText(), driver.findElement(By.xpath("(//th[text()='IEP Mandate']/ancestor::thead/following-sibling::tbody/tr/td[2])["+j+"]")).getText());
	  	    	TableData.put(driver.findElement(By.xpath("//th[text()='IEP Mandate']/ancestor::div[2]/descendant::th[text()='End Date']")).getText(), driver.findElement(By.xpath("(//th[text()='IEP Mandate']/ancestor::thead/following-sibling::tbody/tr/td[3])["+j+"]")).getText());
	  	    	TableData.put(driver.findElement(By.xpath("//th[text()='IEP Mandate']/ancestor::div[2]/descendant::th[text()='Amount of Service']")).getText(), driver.findElement(By.xpath("(//th[text()='IEP Mandate']/ancestor::thead/following-sibling::tbody/tr/td[4])["+j+"]")).getText());
	  	    	TableData.put(driver.findElement(By.xpath("//th[text()='IEP Mandate']/ancestor::div[2]/descendant::th[text()='Group Size']")).getText(), driver.findElement(By.xpath("(//th[text()='IEP Mandate']/ancestor::thead/following-sibling::tbody/tr/td[5])["+j+"]")).getText());
	  	    	/*if(TableDATA.contains(null))
	  	    		TableDATA=TableData.toString();
	  	    	else*/
	  	    		TableDATA = TableDATA+System.lineSeparator()+TableData;
	  	      }
	  	    u.takeScreenShot();
		return TableDATA;  
     }
     
	
	
    
	public  String SearchCriteraData() {
  	  int lastRowTableData = driver.findElements(By.xpath("//th[text()='IEP Mandate']/ancestor::div[2]/descendant::th")).size();
			Map<Object, Object> SearchData = new HashMap<Object, Object>();
			String LatestSearchDATA = null;
	  	      for (int j = 1; j <=1; j++) { 
	  	    	SearchData.put(driver.findElement(By.xpath("(//th[text()='IEP Mandate']/ancestor::div[2]/descendant::th)["+j+"]")).getText(), driver.findElement(By.xpath("(//th[text()='IEP Mandate']/ancestor::div[2]/descendant::td)["+j+"]")).getText());
	  	    	LatestSearchDATA = LatestSearchDATA+SearchData;
	  	      }
	  	    u.takeScreenShot();
		return LatestSearchDATA;  
    }
    
    
    
}