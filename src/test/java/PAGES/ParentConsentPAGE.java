package PAGES;

import java.util.HashMap;
import java.util.Map;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import testbase.base;
import uiActions.util;

public class ParentConsentPAGE extends base {
	
	util u = new util();
    WebDriver driver;
    SoftAssert softAssert = new SoftAssert();
    //======================================= STUDENT SEARCH HISTORY PAGE ELEMENTS ====================================================
    //=================================================================================================================================
 
    @FindBy(xpath="//h5[text()='Create Parent Consent Document']") WebElement ParentConsentPageHeader;
    @FindBy(xpath="//a[text()='Step 2: Enter Consent Data']") WebElement Step2EnterConsentDataTab;
    @FindBy(xpath="//label[text()='Consent Date']/preceding-sibling::input") WebElement ConsentDateParentConsentpage;
    @FindBy(xpath="//label[text()='Consent Type']/preceding-sibling::select") WebElement ConsentTypeParentConsentpage;
    @FindBy(xpath="//button[text()='Add Consent']") WebElement AddConsentButton;
    
    
    
    
    
    
  //========================================= COMMON FOR ALL PAGES ==================================================================
    public  void ParentConsentPageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    public void ParentConsentPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    }
    //=================================================================================================================================
  
    public void VerifyParentConsentPageHeader() {
    	ParentConsentPageHeader.isDisplayed();
    }
    
    
    public void ClickStep2EnterConsentDataTab() {
    	Step2EnterConsentDataTab.click();
    }
    
    public String  ConsentDateParentConsentpage() {
    	return ConsentDateParentConsentpage.getText().toString();
    }
 
    public String  ConsentTypeParentConsentpage() {
    	return ConsentTypeParentConsentpage.getText().toString();
    }
    
    public void ClickAddConsentButton() {
    	AddConsentButton.click();
    }
    
    public void VerifyEnterConsentDataDateTypeDisplayed() {
    	Step2EnterConsentDataTab.isDisplayed();
    	ConsentDateParentConsentpage.isDisplayed();
    	ConsentTypeParentConsentpage.isDisplayed();
    }
    
}