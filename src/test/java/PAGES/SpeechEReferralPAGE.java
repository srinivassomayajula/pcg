package PAGES;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.junit.Assert;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.HashMap;
import org.sikuli.script.Pattern;


import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;
//import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;
import org.sikuli.script.Screen;

import testbase.base;
import uiActions.util;

public class SpeechEReferralPAGE extends base
{
	util u = new util();
    WebDriver driver;
    
    
    
    @FindBy(xpath="//h5[contains(text(),'Speech Therapy E-Referral')]") WebElement SpeechEReferralHeader;
    @FindBy(xpath="//p[contains(text(),'Based on your user profile you are NOT eligible to create a Speech E-Referral. Requirements consist of:')]") WebElement WarningMessage1;
    @FindBy(xpath="//p[contains(text(),'Indication of �Can Provide� for Speech Therapy,')]") WebElement WarningMessage2;
    @FindBy(xpath="//p[contains(text(),'Certification Type: Medicaid Service � Qualified,')]") WebElement WarningMessage3;
    @FindBy(xpath="//p[contains(text(),'NPI number, and')]") WebElement WarningMessage4;
    @FindBy(xpath="//p[contains(text(),'Medicaid ID')]") WebElement WarningMessage5;
    @FindBy(xpath="(//p[contains(text(),'Automation Test')])[3]") WebElement StudentName;
    @FindBy(xpath="//p[contains(text(),'06/17/2016')]") WebElement BirthDate;
    @FindBy(xpath="//p[contains(text(),'AUT123')]") WebElement NYCStudentID;
    @FindBy(xpath="//label[text()='ICD Code']/preceding-sibling::select[@name='srcoctrl.form.SpeechICD9CodeOne']//option[@label='E25.9']") WebElement ICDCode1;
    @FindBy(xpath="//label[text()='ICD Code']/preceding-sibling::select[@name='srcoctrl.form.SpeechICD9CodeOne']//option[@label='D66']") WebElement ICDCode2;
    @FindBy(xpath="//label[text()='ICD Code']/preceding-sibling::select[@name='srcoctrl.form.SpeechICD9CodeOne']//option[@label='E27.0']") WebElement ICDCode3;
    @FindBy(xpath="//label[text()='ICD Code']/preceding-sibling::select[@name='srcoctrl.form.SpeechICD9CodeOne']//option[@label='C71.9']") WebElement ICDCode4;
    @FindBy(xpath="//label[text()='ICD Code']/preceding-sibling::select[@name='srcoctrl.form.SpeechICD9CodeOne']//option[@label='E23.0']") WebElement ICDCode5;
    @FindBy(xpath="//label[text()='Street Address']/preceding-sibling::input[@name='srcoctrl.form.NYCSTAddress']") WebElement streetAddress;
    @FindBy(xpath="//label[contains(text(),'I certify that the information contained herein is')]") WebElement CertificationcriteriaCheckbox;
    @FindBy(xpath="//button[contains(text(),'Create E-Referral')]") WebElement ClickCreateEReferalBTN;
    @FindBy(xpath="//button[contains(@class,'ng-scope')][contains(text(),'Clear All')]") WebElement ClickClearAllBtn;
    @FindBy(xpath="//span[contains(text(),'chevron_left')]") WebElement ClickOnArrowBtn;
    @FindBy(xpath="//a[contains(text(),'Close')]") WebElement ClickCloseBtn;
    @FindBy(xpath="//a[contains(text(),'Parent Consent')]") WebElement ParentConsentMenu;
    @FindBy(xpath="//a[@class='ng-binding'][contains(text(),'Speech E-Referral')]") WebElement SpeechERefarralMenu;
    @FindBy(xpath="//a[contains(text(),'Order for Service')]") WebElement OrderForServiceMenu;
    @FindBy(xpath="//a[contains(text(),'IEP Mandates')]") WebElement IEPMandateMenu;
    @FindBy(xpath="//a[contains(text(),'Order & Referral History')]") WebElement OrderandReferalHistoryMenu;
    @FindBy(xpath="//a[contains(text(),'UDO Acknowledgement Form')]") WebElement UDOAcknowledgementFormMenu;
    @FindBy(xpath="//form[@name='clearallconfirmform']/descendant::button[@type='button']") WebElement CloseButtonForm;
    @FindBy(xpath="//form[@name='clearallconfirmform']/descendant::button[@type='submit']") WebElement ClearAllButtonForm;
    @FindBy(xpath="//label[contains(text(),'Service IS Medically Necessary')]") WebElement ClickRadiobutton;
    
    public  void SpeechEReferralDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    
    public void VerifySpeechEReferralHeader()
    {
    	u.waitForElementVisibility(driver, SpeechEReferralHeader, 40);
    	SpeechEReferralHeader.isDisplayed();
    }
    public void VerifyWarningMessages()
    {
    	
    	String str1=WarningMessage1.getText();
    	test.log(LogStatus.PASS, str1);
    	
    	String str2=WarningMessage2.getText();
    	 test.log(LogStatus.PASS, str2);
    	 
    	 String str3=WarningMessage3.getText();
    	 test.log(LogStatus.PASS, str3);
    	 
    	 String str4=WarningMessage4.getText();
    	 test.log(LogStatus.PASS, str4);
    	  
    	 String str5=WarningMessage5.getText();
    	 test.log(LogStatus.PASS, str5);
    }
    
    
    public void StudentDataPulledFromStudentProfile(HashMap<String, String> map)
    {
    	 String StudentName1=StudentName.getText();
		 Assert.assertEquals(StudentName1,map.get("Studentname"));
		 
		 String StudentDateofBirth=BirthDate.getText();
		 Assert.assertEquals(StudentDateofBirth,map.get("StudentDateOfBirth"));
		 
		 String StudentNYCID1=NYCStudentID.getText();
		 Assert.assertEquals(StudentNYCID1,map.get("StudentNYCID"));
		 
    }
    
    public String StudentName()
    {
    	 String StudentName1=StudentName.getText();
    	 return StudentName1;
    }
    
    public String StudentDateofBirth()
    {
    	 String StudentDateofBirth=BirthDate.getText();
    	 return StudentDateofBirth;
    }
    
    public String StudentNYCID1()
    {
    	String StudentNYCID1=NYCStudentID.getText();
    	return StudentNYCID1;
    }
    
    public void DataDisplayedUnderSpeechTherapy()
    {
    	 String str6=ICDCode1.getText();
    	 test.log(LogStatus.PASS, str6);
    	 
    	 String str7=ICDCode2.getText();
    	 test.log(LogStatus.PASS,str7);
    	 
    	 String str8=ICDCode3.getText();
    	 test.log(LogStatus.PASS, str8);
    	 
    	 String str9=ICDCode4.getText();
    	 test.log(LogStatus.PASS, str9);
    	 
    	 String str10=ICDCode5.getText();
    	 test.log(LogStatus.PASS, str10);
    	 
    	 String str11=streetAddress.getText();
    	 System.out.println(str11);
    }
    
    public void ClickOnServicISMedicallyNecessary_RadioBtn()
    {
    	ClickRadiobutton.click();
    }
    
    public void ClickCertificationcriteriaCheckbox()
    {
    	CertificationcriteriaCheckbox.click();
    }
    
    public void VerifydisplayofCreateEReferralbutton()
    {
    	ClickCreateEReferalBTN.isDisplayed();
    }
    
    public void ClickCreateEReferralbutton()
    {
    	ClickCreateEReferalBTN.click();
    }
    
    public void ClickClearAllButton()
    {
    	u.waitForElementClickable(driver, ClickClearAllBtn, 40);
    	ClickClearAllBtn.click();
    }
    
    public void VerifyCloseAndClearButtonForm() {
    	u.waitForElementVisibility(driver, CloseButtonForm, 30);
    	CloseButtonForm.isDisplayed();
    	u.waitForElementVisibility(driver, ClearAllButtonForm, 30);
    	ClearAllButtonForm.isDisplayed();
    }
    
    public void ClickClearAllButtonForm() {
    	u.waitForElementClickable(driver, ClearAllButtonForm, 40);
    	ClearAllButtonForm.click();
    	
    
    }
    
    public String VerifyStudentName() {
    	String Name = StudentName.getText();
    	return Name;
    }
    
    public String VerifyDateOfBirth() {
    	String DOB = BirthDate.getText();
		return DOB;
    }
    public String VerifyNYCStudentID() {
    	String NYCID = NYCStudentID.getText();
		return NYCID;
    }
    
    
    
    public void ClickOnArrowButton()
    {
    	ClickOnArrowBtn.click();
    }
    
    public void ClickCloseButton()
    {
    	ClickCloseBtn.click();
    }
    
   public void ClickParentConsentMenu()
    {
    	 u.waitForElementClickable(driver, ParentConsentMenu, 60);
    	ParentConsentMenu.click();
    }
    
   public void ClickSpeechEReferralMenu()
   {
	   u.waitForElementClickable(driver, SpeechERefarralMenu, 60);
	   SpeechERefarralMenu.click(); 
   }
   
   public void ClickOrderAndServiceMenu()
   {
	   u.waitForElementClickable(driver,  OrderForServiceMenu, 60);
	   OrderForServiceMenu.click();
   }
   
   public void ClickIEPManadateMenu()
   {
	  
	   IEPMandateMenu.click();
   }
   
   public void ClickOrderandReferralHistory()
   {
	  
	   OrderandReferalHistoryMenu.click(); 
   }
   
   public void ClickUDOAcknowledgementFormMenu()
   {
	   UDOAcknowledgementFormMenu.click();
   }
   
   
   public String  VerifyPdfData(String TestCaseName) throws IOException {
   	String PDF = u.ReadDataFromPDF(TestCaseName);
		return PDF;
   }
		 
   public void DownloadAndSavePDF(String TestCaseName, String ImgPath) throws FindFailed, AWTException, InterruptedException {
   	u.waitToLoad();
   	Robot robot = new Robot();    	
   	Screen screen = new Screen();
   	u.waitToLoad();
   	u.waitToLoad();
   	Pattern image = new Pattern(ImgPath);
   	u.waitToLoad();
   	screen.rightClick(image);
   	
   	robot.keyPress(KeyEvent.VK_DOWN);
   	robot.keyRelease(KeyEvent.VK_DOWN);
   	robot.keyPress(KeyEvent.VK_ENTER);
   	robot.keyRelease(KeyEvent.VK_ENTER);
   	
   	u.waitToLoad();
   	robot.keyPress(KeyEvent.VK_BACK_SPACE);
   	robot.keyRelease(KeyEvent.VK_BACK_SPACE);
   	
   	StringSelection stringSelection = new StringSelection(System.getProperty("user.dir")+"\\PDFs\\"+TestCaseName);
   	Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
   	clipboard.setContents(stringSelection, null);
   	
   	robot.keyPress(KeyEvent.VK_CONTROL);
   	robot.keyPress(KeyEvent.VK_V);
   	robot.keyRelease(KeyEvent.VK_CONTROL);
   	robot.keyRelease(KeyEvent.VK_V);
   	
   	robot.keyPress(KeyEvent.VK_ENTER);
   	robot.keyRelease(KeyEvent.VK_ENTER);
   	u.waitToLoad();
   	robot.keyPress(KeyEvent.VK_TAB);
   	robot.keyRelease(KeyEvent.VK_TAB);
   	robot.keyPress(KeyEvent.VK_ENTER);
   	robot.keyRelease(KeyEvent.VK_ENTER);
   	u.waitToLoad();
   }
   

    
}
