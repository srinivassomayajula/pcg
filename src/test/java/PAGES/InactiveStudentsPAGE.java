package PAGES;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import testbase.base;
import uiActions.util;

public class InactiveStudentsPAGE extends base {
	
	util u = new util();
    WebDriver driver;
    
    //======================================= ACTIVE STUDENTS PAGE ELEMENTS ======================================================
    //============================================================================================================================
    @FindBy(xpath="//a[text()='Inactive Students']") WebElement InactiveStudents;
    @FindBy(xpath="//input[@id='StudentLastName']") WebElement LastName;
    @FindBy(xpath="//input[@id='Submit']") WebElement InactiveStudentsSubmit;
    @FindBy(xpath="//a[text()='Name']") WebElement Name;
    //==================================== CODE ==================================================================================
    //============================================================================================================================
    public  void InactiveStudentsPageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    
    public void InactiveStudentsPageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    } 
    public void ClickInactiveStudents() {
    	u.waitForElement(driver, 30, InactiveStudents);
    	InactiveStudents.click();
    }
    public void EnterLastName(String strLastName) {
    	this.LastName.sendKeys(strLastName);
	}
    public void SubmitButtonInactiveStudents() {
    	InactiveStudentsSubmit.click();
    }
    public void VerifyInactiveStudentsList() {
    	u.waitForElement(driver, 30, Name);
    	u.takeScreenShot();
    	Name.isDisplayed();
	}
}