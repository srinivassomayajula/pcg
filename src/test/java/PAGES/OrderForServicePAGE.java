package PAGES;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.junit.Assert;


import com.relevantcodes.extentreports.LogStatus;

import testbase.base;
import uiActions.util;

public class OrderForServicePAGE extends base {
	
	util u = new util();
    WebDriver driver;
    SoftAssert softAssert = new SoftAssert();
    //======================================= STUDENT SEARCH HISTORY PAGE ELEMENTS ====================================================
    //=================================================================================================================================
    
    @FindBy(xpath="//h5[text()='Create Order for Service']") WebElement CreateOrderforServiceHeader;
    @FindBy(xpath="//h5[text()='Enter Order for Service Data']") WebElement EnterOrderforServiceDataHeader;
    @FindBy(xpath="//h5[text()='Attach External Documents Using EasyTrac']") WebElement AttachExternalDocumentsUsingEasyTracHeader;
    @FindBy(xpath="//h5[text()='Order for School Health Related Support Services (PT/OT/Speech)']") WebElement OrderforSchoolHealthRelatedSupportServicesSpeechHeader;
    @FindBy(xpath="//h5[text()='Order for Skilled Nursing Services']") WebElement OrderForSkilledNursingServicesHeader;
    @FindBy(xpath="//h5[text()='Order for School Health Related Support Services (PT/OT)']") WebElement OrderforSchoolHealthRelatedSupportServicesHeader;
    
    
    @FindBy(xpath="//h5[text()='Homepage Template Document']") WebElement HomepageTemplateDocumentHeader;
    
    
    @FindBy(xpath="//label[text()='Student Name']/preceding-sibling::p") WebElement StudentNameTempleteDocument;
    @FindBy(xpath="//label[text()='Comments']/preceding-sibling::textarea") WebElement CommentsTempleteDocument;
    @FindBy(xpath="//p[text()='OrderForService']/../descendant::div/p[2]") WebElement InstructionsTempleteDocument;
    
    @FindBy(xpath="//a[text()='Step 1: Create Order for Service']") WebElement Step1CreateOrderforService;
    @FindBy(xpath="//a[text()='Step 2: Enter Order for Service Data']") WebElement Step2EnterOrderforServiceData;
    @FindBy(xpath="//a[text()='Step 3: Upload Paper Order Form']") WebElement Step3UploadPaperOrderForm;
    
    @FindBy(xpath="//h5[text()='Last Entered Order for This Student']") WebElement LastEnteredOrderforThisStudentHeader;
    @FindBy(xpath="//option[text()='Speech Therapy Paper Referral']/..") WebElement SelectDocument;
    
    
    @FindBy(xpath="//label[text()='ICD Code']/preceding-sibling::select") WebElement ICDCodes;
    @FindBy(xpath="//label[contains(.,'Ordering Doctor')]/preceding-sibling::input") WebElement OrderingDoctorName;
    @FindBy(xpath="//label[contains(.,'Telephone Number')]/preceding-sibling::input") WebElement TelephoneNumber;
    @FindBy(xpath="//label[contains(.,'Street Address')]/preceding-sibling::input") WebElement StreetAddress;
    @FindBy(xpath="//label[contains(.,'City')]/preceding-sibling::input") WebElement City;
    @FindBy(xpath="//label[contains(.,'State')]/preceding-sibling::input") WebElement State;
    @FindBy(xpath="//label[contains(.,'ZipCode')]/preceding-sibling::input") WebElement ZipCode;
    @FindBy(xpath="//label[contains(.,'License Number')]/preceding-sibling::input") WebElement LicenseNumber;
    @FindBy(xpath="//label[contains(.,'NPI Number')]/preceding-sibling::input") WebElement NPINumber;
    @FindBy(xpath="//label[contains(.,'Medicaid ID')]/preceding-sibling::input") WebElement MedicaidID;
    
    @FindBy(xpath="//label[text()='Start Date']/preceding-sibling::input") WebElement StartDate;
    @FindBy(xpath="//label[text()='Physician']/preceding-sibling::input") WebElement Physician;
    @FindBy(xpath="//label[text()='Phone Number']/preceding-sibling::input") WebElement PhoneNumber;
    @FindBy(xpath="//label[text()='Medicaid ID Number']/preceding-sibling::input") WebElement MedicaidIDNumberEnterOrder;
    @FindBy(xpath="//label[text()='Address']/preceding-sibling::input") WebElement AddressEnterOrder;
    @FindBy(xpath="//h5[text()='Enter Order for Service Data']/ancestor::div[2]/following-sibling::div/descendant::label[text()='License Number']/preceding-sibling::input") WebElement LicenseNumberEnterOrder;
    @FindBy(xpath="//h5[text()='Enter Order for Service Data']/ancestor::div[2]/following-sibling::div/descendant::label[contains(.,'City')]/preceding-sibling::input") WebElement CityEnterOrder;
    @FindBy(xpath="//h5[text()='Enter Order for Service Data']/ancestor::div[2]/following-sibling::div/descendant::label[contains(.,'State')]/preceding-sibling::input") WebElement StateEnterOrder;
    @FindBy(xpath="//label[contains(.,'Zip Code')]/preceding-sibling::input") WebElement ZipcodeEnterOrder;
    @FindBy(xpath="//h5[text()='Enter Order for Service Data']/ancestor::div[2]/following-sibling::div/descendant::label[contains(.,'NPI Number')]/preceding-sibling::input") WebElement NPINumberEnterOrder;
    @FindBy(xpath="//select[@placeholder='Choose Diagnosis Code']") WebElement ICDCodeRelatedServices;
    @FindBy(xpath="//button[text()='Add New Service Order']") WebElement AddNewServiceOrderSubmitButton;
    
    @FindBy(xpath="//label[contains(.,'Name')]/preceding-sibling::input") WebElement OrderingDocName;
    @FindBy(xpath="//label[contains(.,'Address (street, city, state and zip code)')]/preceding-sibling::input") WebElement OrderingDocAddress;
    @FindBy(xpath="//label[contains(.,'License Number')]/preceding-sibling::input") WebElement OrderingDocLicense;
    @FindBy(xpath="//label[contains(.,'NPI Number')]/preceding-sibling::input") WebElement OrderingDocNPINumber;
    @FindBy(xpath="//label[contains(.,'Medicaid Provider ID Number')]/preceding-sibling::input") WebElement OrderingDocMedicaidID;
    @FindBy(xpath="//input[@name='coctrl.form.NYCHSOFOccupational']") WebElement ICDcodesOT;
    @FindBy(xpath="//input[@name='coctrl.form.NYCHSOFPhysical']") WebElement ICDcodesPT;
    @FindBy(xpath="//input[@name='coctrl.form.NYCHSOFSpeech']") WebElement ICDcodesST;

    
    
    @FindBy(xpath="//button[text()='Clear All']") WebElement ClearAllButton;
    @FindBy(xpath="//button[text()='Fill From Last Order']/following-sibling::button[text()='Clear All']") WebElement ClearAllLastOrderButton;
    @FindBy(xpath="//button[text()='Fill From Last Order']") WebElement FillFromLastOrderButton;
    @FindBy(xpath="//button[text()='Create Cover Page']") WebElement CreateCoverPageButton;
    
    @FindBy(xpath="//label[text()='Student name']/preceding-sibling::p") WebElement StudentName;
    @FindBy(xpath="//label[text()='Birth Date']/preceding-sibling::p") WebElement DateOfBirth;
    @FindBy(xpath="//label[text()='NYC Student ID/OSIS#']/preceding-sibling::p") WebElement NYCStudentID;
    
    @FindBy(xpath="//form[@name='clearallconfirmform']/descendant::button[@type='button']") WebElement CloseButtonForm;
    @FindBy(xpath="//form[@name='clearallconfirmform']/descendant::button[@type='submit']") WebElement ClearAllButtonForm;
    
    @FindBy(xpath="//textarea[text()='Comments']") WebElement CommentsOrderForService;
    @FindBy(xpath="//h5[text()='Homepage Template Document']/../following-sibling::div/descendant::button[text()='Create Cover Page']") WebElement CreateCoverPageButtonOnWindow;
    
    
    
    
    
    
    @FindBy(xpath="//h5[text()='IEP Mandates']") WebElement IEPMandatesHeader;
    @FindBy(xpath="//span[text()='chevron_left']") WebElement chevron_leftArrow;
    @FindBy(xpath="//p[text()='Student Center']") WebElement StudentCenterDropDown;
    @FindBy(xpath="//a[text()='Parent Consent']") WebElement ParentConsent;
    @FindBy(xpath="//h5[text()='Create Parent Consent Document']") WebElement CreateParentConsentDocument;
    @FindBy(xpath="//a[text()='IEP Mandates']") WebElement IEPMandates;
    @FindBy(xpath="//a[text()='Order for Service']") WebElement OrderforService;
    @FindBy(xpath="//h5[text()='Create Order for Service']") WebElement CreateOrderforService;
    @FindBy(xpath="//a[text()='Speech E-Referral']") WebElement SpeechEReferral;
    @FindBy(xpath="//h5[text()='Speech Therapy E-Referral']") WebElement SpeechTherapyEReferral;
    @FindBy(xpath="//h5[text()='Speech Therapy Paper Referral']") WebElement SpeechTherapyPaperReferralHeader;
    @FindBy(xpath="//a[contains(.,'Order & Referral History')]") WebElement OrderReferralHistory;
    @FindBy(xpath="//h5[text()='Orders/Referral History']") WebElement OrdersReferralHistoryHeader;
    @FindBy(xpath="//a[contains(.,'UDO Acknowledgement Form')]") WebElement UDOAcknowledgementForm;
    @FindBy(xpath="//h5[text()='Acknowledgement of IEP Responsibilities']") WebElement AcknowledgementofIEPResponsibilities;
    
    @FindBy(xpath="//label[contains(.,'entries')]") WebElement Entries;
    @FindBy(xpath="//a[text()='Close']") WebElement CloseStudentCenterMenu;
    @FindBy(xpath="//label[text()='Service IS Medically Necessary']") WebElement ServiceIsMedicallyNecessaryRadioButton;
    @FindBy(xpath="///label[text()='Service, as written, IS NOT Medically Necessary']") WebElement ServiceIsNotMedicallyNecessaryRadioButton;
    
    @FindBy(xpath="(//h5[text()='Occupational Therapy']/ancestor::div/descendant::label[text()='Service IS Medically Necessary'])[1]") WebElement OTNecessary;
    @FindBy(xpath="(//h5[text()='Physical Therapy']/ancestor::div/descendant::label[text()='Service IS Medically Necessary'])[2]") WebElement PTNecessary;
    @FindBy(xpath="(//h5[text()='Speech Therapy']/ancestor::div/descendant::label[text()='Service IS Medically Necessary'])[3]") WebElement STNecessary;
    
    @FindBy(xpath="(//h5[text()='Occupational Therapy']/ancestor::div/descendant::label[text()='Service, as written, IS NOT Medically Necessary'])[1]") WebElement OTNotNecessary;
    @FindBy(xpath="(//h5[text()='Physical Therapy']/ancestor::div/descendant::label[text()='Service, as written, IS NOT Medically Necessary'])[2]") WebElement PTNotNecessary;
    @FindBy(xpath="(//h5[text()='Speech Therapy']/ancestor::div/descendant::label[text()='Service, as written, IS NOT Medically Necessary'])[3]") WebElement STNotNecessary;
    
    @FindBy(xpath="(//h5[text()='Occupational Therapy']/ancestor::div/descendant::label[text()='N/A'])[1]") WebElement OTNotAvailable;
    @FindBy(xpath="(//h5[text()='Physical Therapy']/ancestor::div/descendant::label[text()='N/A'])[2]") WebElement PTNotAvailable;
    @FindBy(xpath="(//h5[text()='Speech Therapy']/ancestor::div/descendant::label[text()='N/A'])[3]") WebElement STNotAvailable;
    
    @FindBy(xpath="//label[text()='is Medically Necessary']") WebElement ServiceMedicallyNecessary;
    @FindBy(xpath="//label[text()='is NOT Medically Necessary']") WebElement ServiceMedicallyNotNecessary;
    
    @FindBy(xpath="//button[text()='Create Paper Order']") WebElement CreatePaperOrderSubmitButton;
    
    @FindBy(xpath="//th[text()='Authorizing Physician']/ancestor::thead/following-sibling::tbody/descendant::td[2]") WebElement AuthorizingPhysicianLastOrder;
    @FindBy(xpath="//th[text()='Phone Number']/ancestor::thead/following-sibling::tbody/descendant::td[4]") WebElement PhoneNumberLastOrder;
    @FindBy(xpath="//th[text()='Medicaid ID Number']/ancestor::thead/following-sibling::tbody/descendant::td[6]") WebElement MedicaidIdLastOrder;
    @FindBy(xpath="//th[text()='Address']/ancestor::thead/following-sibling::tbody/descendant::td[3]") WebElement AddressLastOrder;
    @FindBy(xpath="//th[text()='License Number']/ancestor::thead/following-sibling::tbody/descendant::td[7]") WebElement LicenseNumberLastOrder;
    @FindBy(xpath="//th[text()='NPI Number']/ancestor::thead/following-sibling::tbody/descendant::td[5]") WebElement NPINumberLastOrder;

    
    @FindBy(xpath="//a[@class='lightbox-close']") WebElement PDFWindowClose;
    
    @FindBy(xpath="//th[text()='File']/ancestor::thead/following-sibling::tbody/descendant::span[text()='file_upload']") WebElement FileUploadIcon;
    @FindBy(xpath="//button[text()='Upload File(s)']") WebElement UploadFileButton;
    
   
  //========================================= COMMON FOR ALL PAGES ==================================================================
    public  void OrderForServicePageDriverRef(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);  
    }
    public void OrderForServicePageFrame(WebDriver driver) throws InterruptedException {
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	u.waitToLoad();
    	driver.switchTo().frame("ezframer");
    }
    public void SwitchFrameToDefault(WebDriver driver) throws InterruptedException { 
    	this.driver = driver;
        PageFactory.initElements(driver, this);
    	driver.switchTo().defaultContent();
    }
    
    //================================================================================================================================
    
    
    public void VerifyCreateOrderforServiceHeader() {
    	u.waitForElementVisibility(driver, CreateOrderforServiceHeader, 30);
    	CreateOrderforServiceHeader.isDisplayed();
    }
    
    
    public String ICDCodes() {
  	  int RowICDCodes = driver.findElements(By.xpath("//label[text()='ICD Code']/preceding-sibling::select/option[@selected='selected']")).size();
			Map<Object, Object> ICDCodesData = new HashMap<Object, Object>();
			String returnIDCCodes = null;
	  	      for (int j = 1; j <=5; j++) { 
	  	    	ICDCodesData.put(driver.findElement(By.xpath("//label[text()='ICD Code']")).getText(), driver.findElement(By.xpath("(//label[text()='ICD Code']/preceding-sibling::select/option[@selected='selected'])["+j+"]")).getText());
	  	    	returnIDCCodes = returnIDCCodes+ICDCodesData;
	  	      }
	  	    u.takeScreenShot();
		return returnIDCCodes;  
    }
    
    /*public String[] SvaedDataForRespectiveTheraphy() {
    	String[] ar = new String[9];
        ar[0] = OrderingDoctorName.getText().toString();
        ar[1] = TelephoneNumber.getText().toString();
        ar[2] = StreetAddress.getText().toString();
        ar[3] = City.getText().toString();
        ar[4] = State.getText().toString();
        ar[5] = ZipCode.getText().toString();
        ar[6] = LicenseNumber.getText().toString();
        ar[7] = NPINumber.getText().toString();
        ar[8] = MedicaidID.getText().toString();
        //System.out.println(ar);
        return ar;
    }*/
    
    public void SvaedDataForRespectiveTheraphy(){
    	OrderingDoctorName.isDisplayed();
    	TelephoneNumber.isDisplayed();
    	StreetAddress.isDisplayed();
    	City.isDisplayed();
    	State.isDisplayed();
    	ZipCode.isDisplayed();
    	LicenseNumber.isDisplayed();
    	NPINumber.isDisplayed();
    	MedicaidID.isDisplayed();
    	u.takeScreenShot();
    }
    
    public void EnabledClearAllButton() {
    	ClearAllButton.isEnabled();
    }
    
    public void VerifyStudentdetailsdisplayed() {
    	StudentName.isDisplayed();
    	DateOfBirth.isDisplayed();
    	NYCStudentID.isDisplayed();
    }
    
    public String VerifyStudentName() {
    	String Name = StudentName.getText();
    	return Name;
    }
    public String VerifyDateOfBirth() {
    	String DOB = DateOfBirth.getText();
		return DOB;
    }
    public String VerifyNYCStudentID() {
    	String NYCID = NYCStudentID.getText();
		return NYCID;
    }

    public void ClickClearAllButton() {
    	u.waitForElementClickable(driver, ClearAllButton, 40);
    	ClearAllButton.click();
    }
    
    public void ClickClearAllLastOrderButton() {
    	WebElement element = driver.findElement(By.xpath("//button[text()='Fill From Last Order']/following-sibling::button[text()='Clear All']"));
    	Actions actions = new Actions(driver);
    	actions.moveToElement(element).click().perform();
    	//ClearAllLastOrderButton.click();
    }
    
    public void VerifyCloseAndClearButtonForm() {
    	CloseButtonForm.isDisplayed();
    	ClearAllButtonForm.isDisplayed();
    }
    
    public void ClickClearAllButtonForm() {
    	ClearAllButtonForm.click();
    }
    
    public void ClickFillFromLastOrderButton() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementClickable(driver, FillFromLastOrderButton, 30);
    	FillFromLastOrderButton.click();
    }
    
    public void ClickCloseButtonFormForm() {
    	CloseButtonForm.click();
    }
    
    public void ClickCreateCoverPageButton() {
    	u.waitForElementClickable(driver, CreateCoverPageButton, 30);
    	CreateCoverPageButton.click();
    }
    
    public void ClickStep2EnterOrderforServiceData() {
    	Step2EnterOrderforServiceData.click();
    }
    public void ClickStep3UploadPaperOrderForm() {
    	Step3UploadPaperOrderForm.click();
    }
    
    public void VerifyStep3UploadPaperOrderFormHeader() {
    	Step3UploadPaperOrderForm.isDisplayed();
    	u.takeScreenShot();
    }
    public void VerifyEnterOrderforServiceDataHeader() {
    	u.waitForElementVisibility(driver, EnterOrderforServiceDataHeader, 30);
    	EnterOrderforServiceDataHeader.isDisplayed();
    	u.takeScreenShot();
    }
    
    public void VerifyOrderforSchoolHealthRelatedSupportServicesSpeechHeader() {
    	u.waitForElementVisibility(driver, OrderforSchoolHealthRelatedSupportServicesSpeechHeader, 30);
    	OrderforSchoolHealthRelatedSupportServicesSpeechHeader.isDisplayed();
    	u.takeScreenShot();
    }
    
    public void VerifyOrderForSkilledNursingServicesHeader() {
    	u.waitForElementVisibility(driver, OrderForSkilledNursingServicesHeader, 30);
    	OrderForSkilledNursingServicesHeader.isDisplayed();
    	u.takeScreenShot();
    }
    
    public void VerifyOrderforSchoolHealthRelatedSupportServicesHeader() {
    	u.waitForElementVisibility(driver, OrderforSchoolHealthRelatedSupportServicesHeader, 30);
    	OrderforSchoolHealthRelatedSupportServicesHeader.isDisplayed();
    	u.takeScreenShot();
    }
    
    public void VerifyAttachExternalDocumentsUsingEasyTracHeader() {
    	u.waitForElementVisibility(driver, AttachExternalDocumentsUsingEasyTracHeader, 30);
    	AttachExternalDocumentsUsingEasyTracHeader.isDisplayed();
    	u.takeScreenShot();
    }
    
    public void VerifyLastEnteredOrderforThisStudentHeader() {
    	u.waitForElementVisibility(driver, LastEnteredOrderforThisStudentHeader, 30);
    	LastEnteredOrderforThisStudentHeader.isDisplayed();
    	u.takeScreenShot();
    }
    
    public void verifyHomepageTemplateDocumentHeader() {
    	u.waitForElementVisibility(driver, HomepageTemplateDocumentHeader, 30);
    	HomepageTemplateDocumentHeader.isDisplayed();
    }
    
    
    public Map<Object, Object> LastEnteredOrderTable() throws InterruptedException {
    	    int RowICDCodes = driver.findElements(By.xpath("//h5[text()='Last Entered Order for This Student']/ancestor::div[@class='panel-heading']/following-sibling::div/descendant::td")).size();
  			Map<Object, Object> LastEnteredOrder = new HashMap<Object, Object>();
  	  	      for (int j = 1; j <=RowICDCodes; j++){ 
  	  	    	  LastEnteredOrder.put(driver.findElement(By.xpath("(//h5[text()='Last Entered Order for This Student']/ancestor::div[@class='panel-heading']/following-sibling::div/descendant::th)["+j+"]")).getText(), driver.findElement(By.xpath("(//h5[text()='Last Entered Order for This Student']/ancestor::div[@class='panel-heading']/following-sibling::div/descendant::td)["+j+"]")).getText());
  	  	    //returnLastEnteredOrder = returnLastEnteredOrder+LastEnteredOrder;
  	  	      }
  	  	    u.takeScreenShot();
  	  	  System.out.println(LastEnteredOrder);
  		return LastEnteredOrder;  
      }
    
    
    public void EnterCommentsOrderForService(String Comments) throws InterruptedException {
    	u.waitForElementClickable(driver, CommentsOrderForService, 30);
    	this.CommentsOrderForService.click();
    	this.CommentsOrderForService.sendKeys(Comments);
    }
    
    public void ClickCreateCoverPageButtonOnWindow() {
    	CreateCoverPageButtonOnWindow.click();
    }
 
   
    
    public void VerifyIEPMandatesHeader() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementVisibility(driver, IEPMandatesHeader, 30);
    	IEPMandatesHeader.isDisplayed();
    }
    public void ClickChevronleftArrow() {
    	u.waitForElement(driver, 40, chevron_leftArrow);
    	chevron_leftArrow.isDisplayed();
    	chevron_leftArrow.click();
    }
    
    public void ClickCloseStudentCenterMenu() {
    	u.waitForElementClickable(driver, CloseStudentCenterMenu, 30);
    	u.takeScreenShot();
    	CloseStudentCenterMenu.click();
    }
    
    public void VerifyStudentCenterDropDown() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementVisibility(driver, StudentCenterDropDown, 30);
    	StudentCenterDropDown.isDisplayed();
    	u.takeScreenShot();
    }
    public void ClickParentConsent() {
    	u.waitForElementVisibility(driver, ParentConsent, 30);
    	ParentConsent.click();
    }
    public void VerifyCreateParentConsentDocumentHeader() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementVisibility(driver, CreateParentConsentDocument, 30);
    	CreateParentConsentDocument.isDisplayed();
    }
    public void ClickIEPMandatesMenu() {
    	u.waitForElementClickable(driver, IEPMandates, 40);
    	IEPMandates.click();
    }
    public void ClickOrderforService() {
    	u.waitForElementClickable(driver, OrderforService, 40);
    	OrderforService.click();
    }

    public void ClickSpeechEReferral() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementClickable(driver, SpeechEReferral, 40);
    	SpeechEReferral.click();
    }
    public void VerifySpeechTherapyEReferral() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementVisibility(driver, SpeechTherapyEReferral, 30);
    	SpeechTherapyEReferral.isDisplayed();
    }
    public void ClickOrderReferralHistory() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementClickable(driver, OrderReferralHistory, 40);
    	OrderReferralHistory.click();
    }
    public void VerifyOrdersReferralHistoryHeader() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementVisibility(driver, OrdersReferralHistoryHeader, 30);
    	OrdersReferralHistoryHeader.isDisplayed();
    }
    public void ClickUDOAcknowledgementForm() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementClickable(driver, UDOAcknowledgementForm, 40);
    	UDOAcknowledgementForm.click();
    }
    public void VerifyAcknowledgementofIEPResponsibilities() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementVisibility(driver, AcknowledgementofIEPResponsibilities, 30);
    	AcknowledgementofIEPResponsibilities.isDisplayed();
    }
    public void VerifyEntries() {
    	u.waitForElementVisibility(driver, Entries, 30);
    	Entries.isDisplayed();
    }
    
    
    public void verifySpeechTherapyPaperReferralHeader() {
    	u.waitForElementVisibility(driver, SpeechTherapyPaperReferralHeader, 30);
    	SpeechTherapyPaperReferralHeader.isDisplayed();
    }
    
    
    public void ClickServiceIsMedicallyNecessaryRadioButton() {
    	ServiceIsMedicallyNecessaryRadioButton.click();
    }
    
    
    public String ServiceIsMedicallyNecessary() {
    	return ServiceIsMedicallyNecessaryRadioButton.getText().toString().replace("Service", "").toLowerCase();
    }
    
    public String ServiceIsNotMedicallyNecessary() {
    	return ServiceIsNotMedicallyNecessaryRadioButton.getText().toString().replace("Service", "").toLowerCase();
    }
    
    public void ClickCreatePaperOrderSubmitButton() {
    	CreatePaperOrderSubmitButton.click();
    }
 
    public void ClickServiceMedicallyNecessary() {
    	ServiceMedicallyNecessary.click();
    }
    
    public void ClickServiceMedicallyNotNecessary() {
    	ServiceMedicallyNotNecessary.click();
    }
    
    public void ClickServiceNecessaryOtPtSt() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementClickable(driver, OTNecessary, 40);
    	OTNecessary.click();
    	PTNecessary.click();
    	try {
    		STNecessary.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
    
    public void ClickServiceNotNecessaryOtPtSt() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementClickable(driver, OTNecessary, 40);
    	OTNotNecessary.click();
    	PTNotNecessary.click();
    	STNotNecessary.click();
    }
    
    public void ClickServiceNotAvailableOtPtSt() throws InterruptedException {
    	u.waitToLoad();
    	u.waitForElementClickable(driver, OTNecessary, 40);
    	OTNotAvailable.click();
    	PTNotAvailable.click();
    	STNotAvailable.click();
    }
    
    public void ClickPDFWindowClose() throws InterruptedException {
    	u.waitForElementClickable(driver, PDFWindowClose, 40);
    	PDFWindowClose.click();
    	u.waitToLoad();
    }
    public void EnterOrderforServiceData() throws InterruptedException {
    	u.waitForElementVisibility(driver, StartDate, 30);
    	u.waitToLoad();
    	StartDate.click();
    	this.StartDate.sendKeys(prop.getProperty("StartDate"));
    	this.Physician.sendKeys(prop.getProperty("PhysicianName"));
    	this.PhoneNumber.sendKeys(prop.getProperty("Phone"));
    	this.MedicaidIDNumberEnterOrder.sendKeys(prop.getProperty("MedicaidNumber"));
    	this.AddressEnterOrder.sendKeys(prop.getProperty("Address"));
    	this.LicenseNumberEnterOrder.sendKeys(prop.getProperty("LicenceNumber"));
    	this.CityEnterOrder.sendKeys(prop.getProperty("City"));
    	this.StateEnterOrder.sendKeys(prop.getProperty("State"));
    	this.ZipcodeEnterOrder.sendKeys(prop.getProperty("ZipCode"));
    	this.NPINumberEnterOrder.sendKeys(prop.getProperty("NPINumber"));
    	this.driver.findElement(By.xpath("//p[text()='Related Services']/../descendant::label[text()='"+prop.getProperty("RelatedServices")+"']")).click();
    	u.SelectUsingVisibleText(ICDCodeRelatedServices, prop.getProperty("ICDCodes"));
    }
    
    public void EnterCreateOrderforServiceData() {
    	this.OrderingDocName.sendKeys(prop.getProperty("PhysicianName"));
    	this.TelephoneNumber.sendKeys(prop.getProperty("Phone"));
    	this.OrderingDocAddress.sendKeys(prop.getProperty("Address"),prop.getProperty("City"),prop.getProperty("State"),prop.getProperty("ZipCode"));
    	this.OrderingDocLicense.sendKeys(prop.getProperty("LicenceNumber"));
    	this.OrderingDocNPINumber.sendKeys(prop.getProperty("NPINumber"));
    	this.OrderingDocMedicaidID.sendKeys(prop.getProperty("MedicaidNumber"));
    	this.ICDcodesOT.sendKeys(prop.getProperty("ICDCodes"));
    	this.ICDcodesPT.sendKeys(prop.getProperty("ICDCodes"));
    	try {
    		this.ICDcodesST.sendKeys(prop.getProperty("ICDCodes"));
		} catch (Exception e) {
			// TODO: handle exception
		}
    	
    }
    
    public void EnterCreateOrderforServiceDataSkilledNursing() {
    	this.OrderingDocName.sendKeys(prop.getProperty("PhysicianName"));
    	this.TelephoneNumber.sendKeys(prop.getProperty("Phone"));
    	this.OrderingDocAddress.sendKeys(prop.getProperty("Address"),prop.getProperty("City"),prop.getProperty("State"),prop.getProperty("ZipCode"));
    	this.OrderingDocLicense.sendKeys(prop.getProperty("LicenceNumber"));
    	this.OrderingDocNPINumber.sendKeys(prop.getProperty("NPINumber"));
    	this.OrderingDocMedicaidID.sendKeys(prop.getProperty("MedicaidNumber"));
    }
    
    public void ClickAddNewServiceOrderSubmitButton() throws InterruptedException {
    	u.waitToLoad();
    	AddNewServiceOrderSubmitButton.click();
    }
   
    
    public void VerifyDataLastOrder() {
    	String AuthorizingPhysician = AuthorizingPhysicianLastOrder.getText().toString();
    	softAssert.assertEquals(AuthorizingPhysician, prop.getProperty("PhysicianName"));
    	
    	String PhoneNumber =PhoneNumberLastOrder.getText().toString();
    	softAssert.assertEquals(PhoneNumber, prop.getProperty("Phone"));
    	
    	String MedicIdNumber = MedicaidIdLastOrder.getText().toString();
    	softAssert.assertEquals(MedicIdNumber, prop.getProperty("MedicaidNumber"));
    	
    	String Address = AddressLastOrder.getText().replaceAll("[,^ ]*", "").toString();
    	softAssert.assertEquals(Address, prop.getProperty("Address")+prop.getProperty("City")+prop.getProperty("State")+prop.getProperty("ZipCode"));
    	
    	String LicienceNumber = LicenseNumberLastOrder.getText().toString();
    	softAssert.assertEquals(LicienceNumber, prop.getProperty("LicenceNumber"));
    	
    	String NIPNumber = NPINumberLastOrder.getText().toString();
    	softAssert.assertEquals(NIPNumber, prop.getProperty("NPINumber"));
    	
    }
    
    
    
    
    
    
    //==============================================PDF Validation UPload and Download======================================================
   //===================================================================================================================
    
    public String StudentNameTempleteDocument() {
    	String StudentNameTemplete = StudentNameTempleteDocument.getText().toString();
		return StudentNameTemplete;
    }
    
    public String InstructionsTempleteDocument() {
    	String InstructionsTemplete = InstructionsTempleteDocument.getText().toString();
    	return InstructionsTemplete;
    }
    
    public String  VerifyPdfData(String TestCaseName) throws IOException {
    	String PDF = u.ReadDataFromPDF(TestCaseName);
		return PDF;
    }
		 
    public void DownloadAndSavePDF(String TestCaseName, String ImgPath) throws FindFailed, AWTException, InterruptedException {
    	u.waitToLoad();
    	u.takeScreenShot();
    	Robot robot = new Robot();    	
    	Screen screen = new Screen();
    	Pattern Image = new Pattern(ImgPath);
    	u.waitToLoad();
    	screen.rightClick(Image);
    	
    	robot.keyPress(KeyEvent.VK_DOWN);
    	robot.keyRelease(KeyEvent.VK_DOWN);
    	robot.keyPress(KeyEvent.VK_ENTER);
    	robot.keyRelease(KeyEvent.VK_ENTER);
    	
    	u.waitToLoad();
    	robot.keyPress(KeyEvent.VK_BACK_SPACE);
    	robot.keyRelease(KeyEvent.VK_BACK_SPACE);
    	
    	StringSelection stringSelection = new StringSelection(System.getProperty("user.dir")+"\\PDFs\\"+TestCaseName);
    	Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    	clipboard.setContents(stringSelection, null);
    	
    	robot.keyPress(KeyEvent.VK_CONTROL);
    	robot.keyPress(KeyEvent.VK_V);
    	robot.keyRelease(KeyEvent.VK_CONTROL);
    	robot.keyRelease(KeyEvent.VK_V);
    	
    	robot.keyPress(KeyEvent.VK_ENTER);
    	robot.keyRelease(KeyEvent.VK_ENTER);
    	u.waitToLoad();
    	robot.keyPress(KeyEvent.VK_TAB);
    	robot.keyRelease(KeyEvent.VK_TAB);
    	robot.keyPress(KeyEvent.VK_ENTER);
    	robot.keyRelease(KeyEvent.VK_ENTER);
    	u.waitToLoad();
    }
    
    public void ClickFileUploadIcon() {
    	u.waitForElementClickable(driver, FileUploadIcon, 40);
    	FileUploadIcon.click();
    }
    
    public void UploadPdfFile(String TestCaseName) throws AWTException, InterruptedException {
    	u.waitToLoad();
    	Robot robot = new Robot();  
    	
    	StringSelection stringSelection = new StringSelection(System.getProperty("user.dir")+"\\PDFs\\"+TestCaseName);
    	Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    	clipboard.setContents(stringSelection, null);
    	
    	robot.keyPress(KeyEvent.VK_CONTROL);
    	robot.keyPress(KeyEvent.VK_V);
    	robot.keyRelease(KeyEvent.VK_CONTROL);
    	robot.keyRelease(KeyEvent.VK_V);
    	
    	u.waitToLoad();
    	robot.keyPress(KeyEvent.VK_TAB);
    	robot.keyRelease(KeyEvent.VK_TAB);
    	robot.keyPress(KeyEvent.VK_TAB);
    	robot.keyRelease(KeyEvent.VK_TAB);
    	robot.keyPress(KeyEvent.VK_ENTER);
    	robot.keyRelease(KeyEvent.VK_ENTER);
    	u.waitToLoad();
    }
    
    public void ClickUploadFileButton() {
    	u.takeScreenShot();
    	UploadFileButton.click();
    }
    
   
}